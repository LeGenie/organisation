import 'package:Organiv/config/variable/global.dart';
import 'package:flutter/material.dart';

class CircularProgress extends StatefulWidget {
  @override
  _CircularProgressState createState() => _CircularProgressState();
}

class _CircularProgressState extends State<CircularProgress> {
  @override
  Widget build(BuildContext context) {
    return CircularProgressIndicator();
  }
}

class LinearProgress extends StatefulWidget {
  @override
  _LinearProgressState createState() => _LinearProgressState();
}

class _LinearProgressState extends State<LinearProgress> {
  @override
  Widget build(BuildContext context) {
    return LinearProgressIndicator(
      backgroundColor: Global.backgroundColor,
    );
  }
}
