import 'package:flutter/material.dart';
import 'package:avatar_glow/avatar_glow.dart';

class DialUserPic extends StatelessWidget {
  const DialUserPic({
    Key key,
    this.size = 192,
    @required this.image,
  }) : super(key: key);

  final double size;
  final String image;

  @override
  Widget build(BuildContext context) {
    return AvatarGlow(
      startDelay: Duration(milliseconds: 1000),
      glowColor: Colors.white,
      endRadius: 160.0,
      duration: Duration(milliseconds: 4000),
      repeat: true,
      showTwoGlows: true,
      repeatPauseDuration: Duration(milliseconds: 100),
      child: MaterialButton(
        onPressed: () {},
        elevation: 100.0,
        shape: CircleBorder(),
        child: Container(
          width: 200.0,
          height: 200.0,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            gradient: RadialGradient(
              colors: [
                Colors.white.withOpacity(0.02),
                Colors.white.withOpacity(0.05)
              ],
              stops: [.5, 1],
            ),
          ),
          child: Container(
            height: double.infinity,
            width: double.infinity,
            child: CircleAvatar(
              backgroundImage: AssetImage(image),
            ),
          ),
        ),
      ),
    );
  }
}
