import 'package:Organiv/app_const.dart';
import 'package:Organiv/views/data/data_local_datasource/local_datasource.dart';
import 'package:Organiv/views/data/database/database_local_datasource.dart';
import 'package:Organiv/views/data/database/database_local_datasource_impl.dart';
import 'package:Organiv/views/data/datasource/firebase_remote_datasource.dart';
import 'package:Organiv/views/data/datasource/firebase_remote_datasource_impl.dart';
import 'package:Organiv/views/data/repositories/database_repository_impl.dart';
import 'package:Organiv/views/data/repositories/firebase_repository_impl.dart';
import 'package:Organiv/views/data/repositories/get_device_number_repository_impl.dart';
import 'package:Organiv/views/domain/repositories/database_repository.dart';
import 'package:Organiv/views/domain/repositories/firebase_repository.dart';
import 'package:Organiv/views/domain/repositories/get_device_number_repository.dart';
import 'package:Organiv/views/domain/usecases/call/add_to_my_single_call_usecase.dart';
import 'package:Organiv/views/domain/usecases/call/create_one_to_one_call_channel_usecase.dart';
import 'package:Organiv/views/domain/usecases/call/end_one_to_one_call.dart';
import 'package:Organiv/views/domain/usecases/call/get_audio_usecase.dart';
import 'package:Organiv/views/domain/usecases/call/get_my_single_call_usecase.dart';
import 'package:Organiv/views/domain/usecases/call/get_one_to_one_call_user_channel_usecase.dart';
import 'package:Organiv/views/domain/usecases/call/get_video_usecase.dart';
import 'package:Organiv/views/domain/usecases/call/send_audio_usecase.dart';
import 'package:Organiv/views/domain/usecases/call/send_video_usecase.dart';
import 'package:Organiv/views/domain/usecases/chat/add_to_my_single_chat_usecase.dart';
import 'package:Organiv/views/domain/usecases/status/add_to_my_single_status_usecase.dart';
import 'package:Organiv/views/domain/usecases/chat/create_one_to_one_chat_channel_usecase.dart';
import 'package:Organiv/views/domain/usecases/contact/database_contact_usecase.dart';
import 'package:Organiv/views/domain/usecases/user/get_all_user_usecase.dart';
import 'package:Organiv/views/domain/usecases/user/update_user_usecase.dart';
import 'package:Organiv/views/domain/usecases/user/get_create_current_user_usecase.dart';
import 'package:Organiv/views/domain/usecases/user/get_current_uid_usecase.dart';
import 'package:Organiv/views/domain/usecases/contact/get_device_numbers_usecase.dart';
import 'package:Organiv/views/domain/usecases/chat/get_my_single_chat_usecase.dart';
import 'package:Organiv/views/domain/usecases/status/get_my_single_status_usecase.dart';
import 'package:Organiv/views/domain/usecases/chat/get_one_to_one_single_user_channel_usecase.dart';
import 'package:Organiv/views/domain/usecases/status/get_status_usecase.dart';
import 'package:Organiv/views/domain/usecases/status/get_status_user_channel_usecase.dart';
import 'package:Organiv/views/domain/usecases/status/create_status_channel_usecase.dart';
import 'package:Organiv/views/domain/usecases/chat/get_text_message_usecase.dart';
import 'package:Organiv/views/domain/usecases/user/is_sign_in_usercase.dart';
import 'package:Organiv/views/domain/usecases/status/send_status_usecase.dart';
import 'package:Organiv/views/domain/usecases/chat/send_text_message_usecase.dart';
import 'package:Organiv/views/domain/usecases/user/sign_in_with_phone_number_usecase.dart';
import 'package:Organiv/views/domain/usecases/user/sign_out_usecase.dart';
import 'package:Organiv/views/domain/usecases/user/verify_phone_number_usecase.dart';
import 'package:Organiv/views/presentation/bloc/call/call_cubit.dart';
import 'package:Organiv/views/presentation/bloc/my_single_call/my_single_call_cubit.dart';
import 'package:Organiv/views/presentation/bloc/user/user_cubit.dart';
import 'package:Organiv/views/presentation/bloc/auth/auth_cubit.dart';
import 'package:Organiv/views/presentation/bloc/communication/communication_cubit.dart';
import 'package:Organiv/views/presentation/bloc/contact/contact_cubit.dart';
import 'package:Organiv/views/presentation/bloc/my_single_chat/my_single_chat_cubit.dart';
import 'package:Organiv/views/presentation/bloc/get_device_number/get_device_numbers_cubit.dart';
import 'package:Organiv/views/presentation/bloc/my_single_status/my_single_status_cubit.dart';
import 'package:Organiv/views/presentation/bloc/phone_auth/phone_auth_cubit.dart';
import 'package:Organiv/views/presentation/bloc/status/status_cubit.dart';
import 'package:Organiv/views/domain/usecases/user/get_my_user_usecase.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart' as FirebaseStorage;
import 'package:firebase_core/firebase_core.dart' as firebase_core;
import 'package:get_it/get_it.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

final sl = GetIt.instance;
Future<void> init() async {
  //External

  final auth = FirebaseAuth.instance;
  final firestore = FirebaseFirestore.instance;
  final firebaseStorage = FirebaseStorage.FirebaseStorage.instance;
  final firebaseCore = firebase_core.FirebaseException;
  final database = await openDatabase(
      join(await getDatabasesPath(), '${AppConst.database}.db'));

  sl.registerLazySingleton(() => auth);
  sl.registerLazySingleton(() => firestore);
  sl.registerLazySingleton(() => firebaseCore);
  sl.registerLazySingleton(() => firebaseStorage);
  sl.registerLazySingleton(() => database);

  //Futures bloc
  sl.registerFactory<AuthCubit>(
    () => AuthCubit(
      signOutUseCase: sl.call(),
      isSignInUseCase: sl.call(),
      getCurrentUidUseCase: sl.call(),
    ),
  );

  sl.registerFactory<PhoneAuthCubit>(() => PhoneAuthCubit(
        getCreateCurrentUserUseCase: sl.call(),
        signInWithPhoneNumberUseCase: sl.call(),
        verfiyPhoneNumberUserCase: sl.call(),
      ));

  sl.registerFactory<GetDeviceNumbersCubit>(() => GetDeviceNumbersCubit(
        getDeviceNumbersUseCase: sl.call(),
      ));

  sl.registerFactory<CommunicationCubit>(() => CommunicationCubit(
        addToMySingleChat: sl.call(),
        getOneToOneUserChannelUseCase: sl.call(),
        getTextMessageUseCase: sl.call(),
        sendTextMessageUseCase: sl.call(),
      ));

  sl.registerFactory<CallCubit>(() => CallCubit(
        addToMySingleCallUseCase: sl.call(),
        getAudioUseCase: sl.call(),
        getOneToOneCallUserChannelUseCase: sl.call(),
        getVideoUseCase: sl.call(),
        sendAudioUseCase: sl.call(),
        sendVideoUseCase: sl.call(),
        endOneToOneCallUseCase: sl.call(),
      ));

  sl.registerFactory<UserCubit>(() => UserCubit(
        createOneToOneChatChannelUseCase: sl.call(),
        createStatusChannelUseCase: sl.call(),
        createOneToOneCallChannelUseCase: sl.call(),
        getAllUserUseCase: sl.call(),
        updateUserUseCase: sl.call(),
        getMyUserUseCase: sl.call(),
      ));

  sl.registerFactory<StatusCubit>(() => StatusCubit(
        addToMySingleStatus: sl.call(),
        getStatusUseCase: sl.call(),
        getStatusUserChannelUseCase: sl.call(),
        sendStatusUseCase: sl.call(),
      ));

  sl.registerFactory<MySingleChatCubit>(() => MySingleChatCubit(
        getMySingleChatUseCase: sl.call(),
      ));

  sl.registerFactory<MySingleCallCubit>(() => MySingleCallCubit(
        getMySingleCallUseCase: sl.call(),
      ));

  sl.registerFactory<MySingleStatusCubit>(() => MySingleStatusCubit(
        getOtherSingleStatusUseCase: sl.call(),
      ));

  sl.registerFactory<ContactCubit>(() => ContactCubit(
        databaseContactUseCase: sl.call(),
      ));

  //usecase

  //auth
  sl.registerLazySingleton<GetCreateCurrentUserUseCase>(
    () => GetCreateCurrentUserUseCase(repository: sl.call()),
  );
  sl.registerLazySingleton<GetCurrentUidUseCase>(
    () => GetCurrentUidUseCase(repository: sl.call()),
  );
  sl.registerLazySingleton<IsSignInUseCase>(
    () => IsSignInUseCase(repository: sl.call()),
  );
  sl.registerLazySingleton<SignInWithPhoneNumberUseCase>(
    () => SignInWithPhoneNumberUseCase(repository: sl.call()),
  );
  //User
  sl.registerLazySingleton<SignOutUseCase>(
      () => SignOutUseCase(repository: sl.call()));
  sl.registerLazySingleton<VerfiyPhoneNumberUserCase>(
      () => VerfiyPhoneNumberUserCase(repository: sl.call()));
  sl.registerLazySingleton<GetAllUserUseCase>(
      () => GetAllUserUseCase(repository: sl.call()));
  sl.registerLazySingleton<UpdateUserUseCase>(
      () => UpdateUserUseCase(repository: sl.call()));
  sl.registerLazySingleton<GetMyUserUseCase>(
      () => GetMyUserUseCase(repository: sl.call()));
  //Message
  sl.registerLazySingleton<GetMySingleChatUseCase>(
      () => GetMySingleChatUseCase(repository: sl.call()));
  sl.registerLazySingleton<GetOneToOneUserChannelUseCase>(
      () => GetOneToOneUserChannelUseCase(repository: sl.call()));
  sl.registerLazySingleton<SendTextMessageUseCase>(
      () => SendTextMessageUseCase(repository: sl.call()));
  sl.registerLazySingleton<AddToMySingleChat>(
      () => AddToMySingleChat(repository: sl.call()));
  sl.registerLazySingleton<GetTextMessageUseCase>(
      () => GetTextMessageUseCase(repository: sl.call()));
  sl.registerLazySingleton<CreateOneToOneChatChannelUseCase>(
      () => CreateOneToOneChatChannelUseCase(repository: sl.call()));
  //Status
  sl.registerLazySingleton<GetMySingleStatusUseCase>(
      () => GetMySingleStatusUseCase(repository: sl.call()));
  sl.registerLazySingleton<GetStatusUserChannelUseCase>(
      () => GetStatusUserChannelUseCase(repository: sl.call()));
  sl.registerLazySingleton<SendStatusUseCase>(
      () => SendStatusUseCase(repository: sl.call()));
  sl.registerLazySingleton<AddToMySingleStatus>(
      () => AddToMySingleStatus(repository: sl.call()));
  sl.registerLazySingleton<GetStatusUseCase>(
      () => GetStatusUseCase(repository: sl.call()));
  sl.registerLazySingleton<CreateStatusChannelUseCase>(
      () => CreateStatusChannelUseCase(repository: sl.call()));
  //Device number
  sl.registerLazySingleton<GetDeviceNumbersUseCase>(
      () => GetDeviceNumbersUseCase(repository: sl.call()));
  sl.registerLazySingleton<DatabaseContactUseCase>(
      () => DatabaseContactUseCase(repository: sl.call()));

  //Call

  sl.registerLazySingleton<GetOneToOneCallUserChannelUseCase>(
      () => GetOneToOneCallUserChannelUseCase(repository: sl.call()));
  sl.registerLazySingleton<GetMySingleCallUseCase>(
      () => GetMySingleCallUseCase(repository: sl.call()));
  sl.registerLazySingleton<AddToMySingleCallUseCase>(
      () => AddToMySingleCallUseCase(repository: sl.call()));
  sl.registerLazySingleton<CreateOneToOneCallChannelUseCase>(
      () => CreateOneToOneCallChannelUseCase(repository: sl.call()));
  sl.registerLazySingleton<EndOneToOneCallUseCase>(
      () => EndOneToOneCallUseCase(repository: sl.call()));
  sl.registerLazySingleton<GetAudioUseCase>(
      () => GetAudioUseCase(repository: sl.call()));
  sl.registerLazySingleton<GetVideoUseCase>(
      () => GetVideoUseCase(repository: sl.call()));
  sl.registerLazySingleton<SendAudioUseCase>(
      () => SendAudioUseCase(repository: sl.call()));
  sl.registerLazySingleton<SendVideoUseCase>(
      () => SendVideoUseCase(repository: sl.call()));

  //repository
  sl.registerLazySingleton<FirebaseRepository>(
      () => FirebaseRepositoryImpl(remoteDataSource: sl.call()));
  sl.registerLazySingleton<GetDeviceNumberRepository>(
      () => GetDeviceNumberRepositoryImpl(localDataSource: sl.call()));
  sl.registerLazySingleton<DatabaseRepository>(
      () => DatabaseRepositoryImpl(localDatabase: sl.call()));

  //remote data
  sl.registerLazySingleton<FirebaseRemoteDataSource>(() =>
      FirebaseRemoteDataSourceImpl(
          auth: sl.call(), firestore: sl.call(), firebaseStorage: sl.call()));
  sl.registerLazySingleton<DatabaseLocalDataSource>(
      () => DatabaseLocalDataSourceImpl(database: sl.call()));
  sl.registerLazySingleton<LocalDataSource>(() => LocalDataSourceImpl());
}
