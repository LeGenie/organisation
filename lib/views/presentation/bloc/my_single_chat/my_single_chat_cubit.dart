import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:Organiv/views/domain/entities/my_single_chat_entity.dart';
import 'package:Organiv/views/domain/usecases/chat/get_my_single_chat_usecase.dart';

part 'my_single_chat_state.dart';

class MySingleChatCubit extends Cubit<MySingleChatState> {
  final GetMySingleChatUseCase getMySingleChatUseCase;

  MySingleChatCubit({this.getMySingleChatUseCase})
      : super(MySingleChatInitial());

  Future<void> getMySingleChat({String uid}) async {
    try {
      final myChatStreamData = getMySingleChatUseCase.call(uid: uid);
      myChatStreamData.listen((myChatData) {
        emit(MySingleChatLoaded(mySingleChat: myChatData));
      });
    } on SocketException catch (_) {
      emit(MySingleChatFailure());
    } catch (_) {
      emit(MySingleChatFailure());
    }
  }
}
