part of 'my_single_chat_cubit.dart';

abstract class MySingleChatState extends Equatable {
  const MySingleChatState();
}

class MySingleChatInitial extends MySingleChatState {
  @override
  List<Object> get props => [];
}

class MySingleChatLoading extends MySingleChatState {
  @override
  List<Object> get props => [];
}

class MySingleChatLoaded extends MySingleChatState {
  final List<MySingleChatEntity> mySingleChat;

  MySingleChatLoaded({this.mySingleChat});

  @override
  List<Object> get props => [mySingleChat];
}

class MySingleChatFailure extends MySingleChatState {
  @override
  List<Object> get props => [];
}
