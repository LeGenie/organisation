part of 'my_single_status_cubit.dart';

abstract class MySingleStatusState extends Equatable {
  const MySingleStatusState();
}

class MySingleStatusInitial extends MySingleStatusState {
  @override
  List<Object> get props => [];
}

class MySingleStatusLoading extends MySingleStatusState {
  @override
  List<Object> get props => [];
}

class MySingleStatusLoaded extends MySingleStatusState {
  final List<MySingleStatusEntity> mySingleStatus;

  MySingleStatusLoaded({this.mySingleStatus}) : assert(mySingleStatus != null);

  @override
  List<Object> get props => [mySingleStatus];
}

class MySingleStatusFailure extends MySingleStatusState {
  @override
  List<Object> get props => [];
}
