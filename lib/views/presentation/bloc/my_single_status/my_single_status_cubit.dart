import 'dart:io';

import 'package:Organiv/views/domain/entities/my_single_status_entity.dart';
import 'package:Organiv/views/domain/usecases/status/get_my_single_status_usecase.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'my_single_status_state.dart';

class MySingleStatusCubit extends Cubit<MySingleStatusState> {
  final GetMySingleStatusUseCase getOtherSingleStatusUseCase;

  MySingleStatusCubit({this.getOtherSingleStatusUseCase})
      : super(MySingleStatusInitial());

  Future<void> getMySingleStatus({String uid}) async {
    try {
      final myStatusStreamData = getOtherSingleStatusUseCase.call(uid: uid);
      myStatusStreamData.listen((myStatusData) {
        emit(MySingleStatusLoaded(mySingleStatus: myStatusData));
      });
    } on SocketException catch (_) {
      emit(MySingleStatusFailure());
    } catch (_) {
      emit(MySingleStatusFailure());
    }
  }
}
