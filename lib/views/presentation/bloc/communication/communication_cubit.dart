import 'dart:io';
import 'dart:async';

import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/views/domain/entities/my_single_chat_entity.dart';
import 'package:Organiv/views/domain/entities/text_message_entity.dart';
import 'package:Organiv/views/domain/usecases/chat/add_to_my_single_chat_usecase.dart';
import 'package:Organiv/views/domain/usecases/chat/send_text_message_usecase.dart';
import 'package:Organiv/views/domain/usecases/chat/get_text_message_usecase.dart';
import 'package:Organiv/views/domain/usecases/chat/get_one_to_one_single_user_channel_usecase.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'communication_state.dart';

class CommunicationCubit extends Cubit<CommunicationState> {
  final SendTextMessageUseCase sendTextMessageUseCase;
  final GetOneToOneUserChannelUseCase getOneToOneUserChannelUseCase;
  final GetTextMessageUseCase getTextMessageUseCase;
  final AddToMySingleChat addToMySingleChat;

  CommunicationCubit({
    this.getOneToOneUserChannelUseCase,
    this.getTextMessageUseCase,
    this.sendTextMessageUseCase,
    this.addToMySingleChat,
  }) : super(CommunicationInitial());

  Future<void> sendTextMessage({
    String profilURL,
    String senderName,
    String senderId,
    String recipientId,
    String recipientName,
    String message,
    String recipientPhoneNumber,
    String senderPhoneNumber,
    String messageType,
    bool isDeleted = false,
    bool isRead,
    bool isReceived,
  }) async {
    try {
      final _channelId = await getOneToOneUserChannelUseCase.call(
          uid: senderId, otherUid: recipientId);
      if (_channelId == null) {
        //En cas d'inexistance de la conversation
        await addToMySingleChat.call(MySingleChatEntity(
          senderName: senderName,
          senderUID: senderId,
          recipientName: recipientName,
          recipientUID: recipientId,
          channelId: _channelId,
          profilURL: profilURL,
          count: 0,
          silence: false,
          online: true,
          recipientPhoneNumber: recipientPhoneNumber,
          senderPhoneNumber: senderPhoneNumber,
          recentTextMessage: message,
          isRead: true,
          isArchived: false,
          time: Timestamp.now(),
          nullable: true,
        ));
        return await sendTextMessage(
          senderName: senderName,
          senderId: senderId,
          senderPhoneNumber: senderPhoneNumber,
          message: message,
          profilURL: profilURL,
          messageType: messageType,
          recipientId: recipientId,
          recipientName: recipientName,
          recipientPhoneNumber: recipientPhoneNumber,
          isDeleted: isDeleted,
          isRead: isRead,
          isReceived: isReceived,
        );
      }
      await sendTextMessageUseCase.call(
          new TextMessageEntity(
            senderName: senderName,
            senderUID: senderId,
            recipientName: recipientName,
            recipientUID: recipientId,
            messageType: messageType,
            message: message,
            isDeleted: isDeleted,
            isRead: isRead,
            isReceived: isReceived,
            messageId: "",
            time: Timestamp.now(),
          ),
          _channelId);

      await addToMySingleChat.call(MySingleChatEntity(
          senderName: senderName,
          senderUID: senderId,
          recipientName: recipientName,
          recipientUID: recipientId,
          channelId: _channelId,
          recipientPhoneNumber: recipientPhoneNumber,
          senderPhoneNumber: senderPhoneNumber,
          recentTextMessage: message,
          profilURL: profilURL,
          count: 0,
          isRead: true,
          isArchived: false,
          time: Timestamp.now(),
          nullable: false));
      await _getMessage(senderId: senderId, recipientId: recipientId);
    } on SocketException catch (_) {
      emit(CommunicationFailure());
    } catch (_) {
      emit(CommunicationFailure());
    }
  }

  Future<void> getMessages({String senderId, String recipientId}) async {
    emit(CommunicationLoading());
    await _getMessage(senderId: senderId, recipientId: recipientId);
  }

  Future<void> _getMessage(
      {String senderId, String recipientId, int nb = 0}) async {
    try {
      final _channelId = await getOneToOneUserChannelUseCase.call(
          uid: senderId, otherUid: recipientId);
      if (nb == 10) return;
      if (_channelId == null)
        Timer(
            Duration(milliseconds: 10),
            () => _getMessage(
                senderId: senderId, recipientId: recipientId, nb: nb + 1));
      final messageStreamData = getTextMessageUseCase.call(_channelId);
      await addToMySingleChat.call(MySingleChatEntity(
          senderUID: senderId,
          recipientUID: recipientId,
          channelId: _channelId,
          count: 0,
          lastView: Timestamp.now(),
          online: true,
          isRead: true,
          nullable: false));

      messageStreamData.listen((messages) {
        if (Global.connected)
          messages.forEach((message) async {
            if (message.senderUID == Global.user.uid) {
              if (!message.isSend)
                await sendTextMessageUseCase.call(
                    new TextMessageEntity(
                      isSend: true,
                      messageId: message.messageId,
                    ),
                    _channelId,
                    update: true);
            } else {
              if (!message.isRead)
                await sendTextMessageUseCase.call(
                    new TextMessageEntity(
                      isReceived: true,
                      isRead: true,
                      messageId: message.messageId,
                    ),
                    _channelId,
                    update: true);
            }
          });
        emit(CommunicationLoaded(messages: messages));
      });
    } on SocketException catch (_) {
      emit(CommunicationFailure());
    } catch (_) {
      emit(CommunicationFailure());
    }
  }
}
