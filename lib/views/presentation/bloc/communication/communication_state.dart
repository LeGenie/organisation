part of 'communication_cubit.dart';

abstract class CommunicationState extends Equatable {
  const CommunicationState();
}

class CommunicationInitial extends CommunicationState {
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

class CommunicationLoading extends CommunicationState {
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

class CommunicationLoaded extends CommunicationState {
  final List<TextMessageEntity> messages;

  CommunicationLoaded({this.messages});
  @override
  // TODO: implement props
  List<Object> get props => [messages];
}

class CommunicationFailure extends CommunicationState {
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}
