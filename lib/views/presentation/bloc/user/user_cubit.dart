import 'dart:io';
import 'dart:async';

import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/views/domain/entities/user_entity.dart';
import 'package:Organiv/views/domain/usecases/call/create_one_to_one_call_channel_usecase.dart';
import 'package:Organiv/views/domain/usecases/chat/create_one_to_one_chat_channel_usecase.dart';
import 'package:Organiv/views/domain/usecases/status/create_status_channel_usecase.dart';
import 'package:Organiv/views/domain/usecases/user/get_all_user_usecase.dart';
import 'package:Organiv/views/domain/usecases/user/update_user_usecase.dart';
import 'package:Organiv/views/domain/usecases/user/get_my_user_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'user_state.dart';

class UserCubit extends Cubit<UserState> {
  final GetAllUserUseCase getAllUserUseCase;
  final CreateOneToOneChatChannelUseCase createOneToOneChatChannelUseCase;
  final CreateStatusChannelUseCase createStatusChannelUseCase;
  final CreateOneToOneCallChannelUseCase createOneToOneCallChannelUseCase;
  final UpdateUserUseCase updateUserUseCase;
  final GetMyUserUseCase getMyUserUseCase;

  UserCubit({
    this.getAllUserUseCase,
    this.createOneToOneChatChannelUseCase,
    this.createStatusChannelUseCase,
    this.createOneToOneCallChannelUseCase,
    this.updateUserUseCase,
    this.getMyUserUseCase,
  }) : super(UserInitial());

  //Update User
  Future<void> updateUser({UserEntity user}) async {
    try {
      await updateUserUseCase.call(user: user);
    } on SocketException catch (_) {
      emit(UserFailure());
    } catch (_) {
      emit(UserFailure());
    }
  }

  Future<void> getMyUser() async {
    try {
      final _userStreamData = await getMyUserUseCase.call();
      _userStreamData.listen((user) => Global.user = user);
    } on SocketException catch (_) {
      emit(UserFailure());
    } catch (_) {
      emit(UserFailure());
    }
  }

  Future<void> getAllUsers() async {
    try {
      final _userStreamData = getAllUserUseCase.call();
      _userStreamData.listen((users) {
        emit(UserLoaded(users: users));
        Timer(Duration(milliseconds: 10), () => getCall());
        ;
      });
    } on SocketException catch (_) {
      emit(UserFailure());
    } catch (_) {
      emit(UserFailure());
    }
  }

  Future<void> getCall() async {
    try {
      if (Global.user == null) return;
      final _user = Global.user;
      if (_user != null &&
          _user.callId != null &&
          _user.callId != "" &&
          !_user.hasDialled &&
          _user.hasCall &&
          !_user.stream) emit(UserCall(user: _user));
      if (_user != null &&
          _user.callId != null &&
          _user.callId != "" &&
          _user.hasCall &&
          _user.stream) {
        emit(UserCallStream(user: _user));
      }
    } on SocketException catch (_) {
      emit(UserFailure());
    } catch (_) {
      emit(UserFailure());
    }
  }

  //create chat channel
  Future<void> createSingleChatChannel({String uid, String otherUid}) async {
    try {
      await createOneToOneChatChannelUseCase.call(uid: uid, otherUid: otherUid);
    } on SocketException catch (_) {
      emit(UserFailure());
    } catch (_) {
      emit(UserFailure());
    }
  }

  //create status channel
  Future<void> createStatusChannel({String uid}) async {
    try {
      await createStatusChannelUseCase.call(uid: uid);
    } on SocketException catch (_) {
      emit(UserFailure());
    } catch (_) {
      emit(UserFailure());
    }
  }

  // create call channel
  Future<void> createSingleCallChannel({String uid, String otherUid}) async {
    try {
      await createOneToOneCallChannelUseCase.call(uid: uid, otherUid: otherUid);
    } on SocketException catch (_) {
      emit(UserFailure());
    } catch (_) {
      emit(UserFailure());
    }
  }
}
