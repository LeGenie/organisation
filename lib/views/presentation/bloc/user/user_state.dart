part of 'user_cubit.dart';

abstract class UserState extends Equatable {
  const UserState();
}

class UserInitial extends UserState {
  @override
  List<Object> get props => throw UnimplementedError();
}

class UserLoading extends UserState {
  @override
  List<Object> get props => throw UnimplementedError();
}

class UserLoaded extends UserState {
  final List<UserEntity> users;

  UserLoaded({this.users});

  @override
  List<Object> get props => [users];
}

class UserCall extends UserState {
  final UserEntity user;

  UserCall({this.user});

  @override
  List<Object> get props => [user];
}

class UserCallStream extends UserState {
  final UserEntity user;

  UserCallStream({this.user});

  @override
  List<Object> get props => [user];
}

class UserNoCall extends UserState {
  @override
  List<Object> get props => [];
}

class UserFailure extends UserState {
  @override
  List<Object> get props => [];
}
