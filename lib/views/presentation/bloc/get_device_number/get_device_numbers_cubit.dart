import 'package:Organiv/views/domain/entities/contact_entity.dart';
import 'package:Organiv/views/domain/usecases/contact/get_device_numbers_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'get_device_numbers_state.dart';

class GetDeviceNumbersCubit extends Cubit<GetDeviceNumbersState> {
  final GetDeviceNumbersUseCase getDeviceNumbersUseCase;
  GetDeviceNumbersCubit({this.getDeviceNumbersUseCase})
      : super(GetDeviceNumbersInitial());

  Future<void> getDeviceNumbers() async {
    try {
      final _contacNumbers = await getDeviceNumbersUseCase.call();
      emit(GetDeviceNumbersLoaded(contacts: _contacNumbers));
    } catch (_) {
      emit(GetDeviceNumbersFailure());
    }
  }
}
