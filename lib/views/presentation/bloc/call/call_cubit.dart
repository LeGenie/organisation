import 'dart:async';

import 'package:Organiv/app_const.dart';
import 'package:Organiv/views/domain/entities/my_single_call_entity.dart';
import 'package:Organiv/views/domain/entities/call_entity.dart';
import 'package:Organiv/views/domain/entities/audio_entity.dart';
import 'package:Organiv/views/domain/entities/video_entity.dart';
import 'package:Organiv/views/domain/entities/user_entity.dart';
import 'package:Organiv/views/domain/usecases/call/add_to_my_single_call_usecase.dart';
import 'package:Organiv/views/domain/usecases/call/end_one_to_one_call.dart';
import 'package:Organiv/views/domain/usecases/call/get_audio_usecase.dart';
import 'package:Organiv/views/domain/usecases/call/get_one_to_one_call_user_channel_usecase.dart';
import 'package:Organiv/views/domain/usecases/call/get_video_usecase.dart';
import 'package:Organiv/views/domain/usecases/call/send_audio_usecase.dart';
import 'package:Organiv/views/domain/usecases/call/send_video_usecase.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'call_state.dart';

class CallCubit extends Cubit<CallState> {
  final SendAudioUseCase sendAudioUseCase;
  final SendVideoUseCase sendVideoUseCase;
  final GetOneToOneCallUserChannelUseCase getOneToOneCallUserChannelUseCase;
  final GetAudioUseCase getAudioUseCase;
  final GetVideoUseCase getVideoUseCase;
  final AddToMySingleCallUseCase addToMySingleCallUseCase;
  final EndOneToOneCallUseCase endOneToOneCallUseCase;

  CallCubit({
    this.sendAudioUseCase,
    this.sendVideoUseCase,
    this.getOneToOneCallUserChannelUseCase,
    this.getAudioUseCase,
    this.getVideoUseCase,
    this.addToMySingleCallUseCase,
    this.endOneToOneCallUseCase,
  }) : super(CallInitial());

  endCall({CallEntity call, String callId}) async {
    emit(CallLoading());
    try {
      await endOneToOneCallUseCase.call(call: call, callId: callId);
      emit(CallEnd());
    } catch (_) {
      print(_.toString());
      emit(CallFailure());
    }
  }

  Future<void> dial(
      {UserEntity from, UserEntity to, String callType, int nb = 0}) async {
    if (callType == null) callType = AppConst.call_audio;
    emit(CallLoading());
    try {
      final String channelId = await getOneToOneCallUserChannelUseCase.call(
          uid: from.uid, otherUid: to.uid);
      if (nb == 10) return;
      if (channelId == null)
        Timer(Duration(milliseconds: 10),
            () => dial(from: from, to: to, callType: callType, nb: nb + 1));
      if (callType == AppConst.call_audio) {
        AudioEntity call = AudioEntity(
          callerId: from.uid,
          callerName: from.name,
          callerPic: from.profilUrl,
          receiverId: to.uid,
          receiverName: to.name,
          receiverPic: to.profilUrl,
          channelId: channelId,
          hasDialled: true,
        );
        final String _callId = await sendAudioUseCase.call(call, channelId);
        emit(CallInProgress(call: call, callId: _callId));
      } else {
        VideoEntity call = VideoEntity(
          callerId: from.uid,
          callerName: from.name,
          callerPic: from.profilUrl,
          receiverId: to.uid,
          receiverName: to.name,
          receiverPic: to.profilUrl,
          channelId: channelId,
          hasDialled: true,
        );
        final String _callId = await sendVideoUseCase.call(call, channelId);
        emit(CallInProgress(call: call, callId: _callId));
      }
    } catch (_) {
      print(_.toString());
      emit(CallFailure());
    }
  }

  Future<void> reject({String uid, String callId, int nb = 0}) async {
    emit(CallLoading());
    try {
      final String channelId = await getOneToOneCallUserChannelUseCase.call(
          uid: uid, otherUid: callId);
      final String pipeId = await getOneToOneCallUserChannelUseCase.call(
          uid: uid, otherUid: callId, id: 'pipeId');
      if (nb == 10) return;
      if (channelId == null)
        return Timer(Duration(milliseconds: 10),
            () => reject(uid: uid, callId: callId, nb: nb + 1));
      CallEntity call = new CallEntity(
        callerId: uid,
        channelId: channelId,
        receiverId: callId,
        nullable: false,
      );
      await endCall(call: call, callId: pipeId);
    } catch (_) {
      print(_.toString());
      emit(CallFailure());
    }
  }

  Future<String> acceptCall({String uid, String callId, int nb = 0}) async {
    if (nb == 10) return null;
    try {
      final String pipeId = await getOneToOneCallUserChannelUseCase.call(
          uid: uid, otherUid: callId, id: 'pipeId');
      return pipeId;
    } catch (_) {
      return null;
    }
  }

  Future<void> callStream(
    String channelId,
    String callId,
    String callType,
  ) async {
    emit(CallLoading());
    try {
      if (callType == AppConst.call_audio) {
        final streamCall = await getAudioUseCase.call(channelId, callId);
        emit(CallStream(stream: streamCall));
      } else {
        final streamCall = await getVideoUseCase.call(channelId, callId);
        emit(CallStream(stream: streamCall));
      }
    } catch (_) {
      emit(CallFailure());
    }
  }
}
