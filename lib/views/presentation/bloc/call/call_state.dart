part of 'call_cubit.dart';

abstract class CallState extends Equatable {
  const CallState();
}

class CallInitial extends CallState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class CallLoading extends CallState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class CallLoaded extends CallState {
  final List<CallEntity> calls;

  CallLoaded({this.calls});
  @override
  // TODO: implement props
  List<Object> get props => [calls];
}

class CallEnd extends CallState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class CallInProgress extends CallState {
  final CallEntity call;
  final String callId;

  CallInProgress({this.call, this.callId});
  @override
  // TODO: implement props
  List<Object> get props => [call];
}

class CallStream extends CallState {
  final Stream<DocumentSnapshot> stream;

  CallStream({this.stream});

  @override
  // TODO: implement props
  List<Object> get props => [stream];
}

class CallCurrent extends CallState {
  final MySingleCallEntity mySingleCallEntity;

  CallCurrent({this.mySingleCallEntity});

  @override
  // TODO: implement props
  List<Object> get props => [mySingleCallEntity];
}

class CallFailure extends CallState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}
