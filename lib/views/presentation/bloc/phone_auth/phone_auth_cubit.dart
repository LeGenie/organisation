import 'dart:io';
import 'dart:async';

import 'package:Organiv/views/domain/entities/user_entity.dart';
import 'package:Organiv/views/domain/usecases/user/get_create_current_user_usecase.dart';
import 'package:Organiv/views/domain/usecases/user/sign_in_with_phone_number_usecase.dart';
import 'package:Organiv/views/domain/usecases/user/verify_phone_number_usecase.dart';
import 'package:Organiv/config/variable/language/fr.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'phone_auth_state.dart';

class PhoneAuthCubit extends Cubit<PhoneAuthState> {
  final SignInWithPhoneNumberUseCase signInWithPhoneNumberUseCase;
  final VerfiyPhoneNumberUserCase verfiyPhoneNumberUserCase;
  final GetCreateCurrentUserUseCase getCreateCurrentUserUseCase;
  PhoneAuthCubit({
    this.signInWithPhoneNumberUseCase,
    this.verfiyPhoneNumberUserCase,
    this.getCreateCurrentUserUseCase,
  }) : super(PhoneAuthInitial());

  Future<void> submitVerifyPhoneNumber({String phoneNumber}) async {
    emit(PhoneAuthLoading());
    try {
      await verfiyPhoneNumberUserCase.call(phoneNumber);
      emit(PhoneAuthSmsCodeReceived());
    } on FirebaseAuthException catch (_) {
      emit(PhoneAuthFailure());
    } on SocketException catch (_) {
      emit(PhoneAuthFailure());
    } catch (_) {
      emit(PhoneAuthFailure());
    }
  }

  Future<void> submitSmsCode({String smsCode}) async {
    emit(PhoneAuthLoading());
    try {
      await signInWithPhoneNumberUseCase.call(smsCode);
      emit(PhoneAuthProfilInfo());
    } on FirebaseAuthException catch (_) {
      emit(PhoneAuthFailure());
    } on SocketException catch (_) {
      emit(PhoneAuthFailure());
    } catch (_) {
      emit(PhoneAuthFailure());
    }
  }

  Future<void> submitProfilInfo({
    String name,
    String profilUrl,
    String phoneNumber,
    String uid,
    String macAdress,
    String markAdress,
    String ipAdress,
    String internationalizedPhoneNumber,
    String isoCode,
  }) async {
    emit(PhoneAuthLoading());
    try {
      await getCreateCurrentUserUseCase.call(
        UserEntity(
            name: name,
            phoneNumber: phoneNumber,
            profilUrl: profilUrl,
            macAdress: macAdress,
            markAdress: markAdress,
            internationalizedPhoneNumber: internationalizedPhoneNumber,
            isoCode: isoCode,
            ipAdress: ipAdress,
            callId: null,
            hasCall: false,
            hasDialled: false,
            online: true,
            silence: false,
            status: Variable.statusLabel,
            lastView: new Timestamp.now(),
            created_at: new Timestamp.now(),
            uid: ""),
      );
      emit(PhoneAuthSuccess());
    } on FirebaseAuthException catch (_) {
      emit(PhoneAuthFailure());
    } on SocketException catch (_) {
      emit(PhoneAuthFailure());
    } catch (_) {
      emit(PhoneAuthFailure());
    }
  }
}
