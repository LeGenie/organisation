part of 'contact_cubit.dart';

abstract class ContactState extends Equatable {
  const ContactState();
}

class ContactInitial extends ContactState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ContactLoading extends ContactState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ContactLoaded extends ContactState {
  final List<ContactUserModel> contacts;

  ContactLoaded({this.contacts});
  @override
  // TODO: implement props
  List<Object> get props => [contacts];
}

class ContactFailure extends ContactState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}
