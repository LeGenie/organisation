import 'package:Organiv/views/data/model/contact_user_model.dart';
import 'package:Organiv/views/domain/usecases/contact/database_contact_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'contact_state.dart';

class ContactCubit extends Cubit<ContactState> {
  final DatabaseContactUseCase databaseContactUseCase;
  ContactCubit({this.databaseContactUseCase}) : super(ContactInitial());

  Future<void> create() async {
    try {
      await databaseContactUseCase.create();
    } catch (e) {
      emit(ContactFailure());
    }
  }

  Future<void> addContact({ContactUserModel contact}) async {
    try {
      await databaseContactUseCase.addContact(contact: contact);
    } catch (e) {
      emit(ContactFailure());
    }
  }

  Future<void> addContacts(List<ContactUserModel> contacts) async {
    try {
      if (contacts.length > 1) await truncContact();
      contacts.forEach((contact) async => await addContact(contact: contact));
    } catch (e) {
      emit(ContactFailure());
    }
  }

  Future<void> getContact() async {
    try {
      List<ContactUserModel> contacts =
          await databaseContactUseCase.getContact();
      emit(ContactLoaded(contacts: contacts));
    } catch (e) {
      emit(ContactFailure());
    }
  }

  Future<void> deleteContact({String uid}) async {
    try {
      await databaseContactUseCase.deleteContact(uid: uid);
    } catch (e) {
      emit(ContactFailure());
    }
  }

  Future<void> updateContact({ContactUserModel contact}) async {
    try {
      await databaseContactUseCase.updateContact(contact: contact);
    } catch (e) {
      emit(ContactFailure());
    }
  }

  Future<void> truncContact() async {
    try {
      await databaseContactUseCase.truncContact();
    } catch (e) {
      emit(ContactFailure());
    }
  }
}
