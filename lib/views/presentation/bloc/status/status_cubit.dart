import 'dart:io';

import 'package:Organiv/views/data/model/my_single_status_model.dart';
import 'package:Organiv/views/domain/entities/status_entity.dart';
import 'package:Organiv/views/domain/usecases/status/add_to_my_single_status_usecase.dart';
import 'package:Organiv/views/domain/usecases/status/get_status_usecase.dart';
import 'package:Organiv/views/domain/usecases/status/get_status_user_channel_usecase.dart';
import 'package:Organiv/views/domain/usecases/status/send_status_usecase.dart';
import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

part 'status_state.dart';

class StatusCubit extends Cubit<StatusState> {
  final SendStatusUseCase sendStatusUseCase;
  final GetStatusUseCase getStatusUseCase;
  final GetStatusUserChannelUseCase getStatusUserChannelUseCase;
  final AddToMySingleStatus addToMySingleStatus;

  static int _nbr = 0;

  StatusCubit({
    this.getStatusUseCase,
    this.getStatusUserChannelUseCase,
    this.sendStatusUseCase,
    this.addToMySingleStatus,
  }) : super(StatusInitial());

  Future<void> sendStatus({
    String senderName,
    String uid,
    String color,
    int size,
    String legende,
    String link,
    String message,
    String statusType,
    Timestamp time,
    List<String> uids,
  }) async {
    emit(StatusLoading());
    try {
      await sendStatusUseCase.call(
          new StatusEntity(
            color: color,
            size: size,
            legende: legende,
            link: link,
            message: message,
            nullable: true,
            statusType: statusType,
            time: time,
            uids: uids,
          ),
          uid,
          update: false);
      await addToMySingleStatus.call(MySingleStatusModel(
          channelId: uid,
          profilURL: "",
          count: _nbr + 1,
          time: Timestamp.now(),
          online: true,
          nullable: false));
      getMyStatus(uid: uid);
    } on SocketException catch (_) {
      emit(StatusFailure());
    } catch (_) {
      emit(StatusFailure());
    }
  }

  Future<void> getStatus({
    String uid,
    String otherUid,
  }) async {
    emit(StatusLoading());
    try {
      await getMyStatus(uid: otherUid, readUid: uid);
    } on SocketException catch (_) {
      emit(StatusFailure());
    } catch (_) {
      emit(StatusFailure());
    }
  }

  Future<void> getMyStatus({String uid, String readUid}) async {
    emit(StatusLoading());
    try {
      final statusStreamData =
          getStatusUseCase.call(uid: uid, readUid: readUid);
      statusStreamData.listen((status) {
        _nbr = status.length;
        emit(StatusLoaded(status: status));
      });
    } on SocketException catch (_) {
      emit(StatusFailure());
    } catch (_) {
      emit(StatusFailure());
    }
  }
}
