part of 'status_cubit.dart';

abstract class StatusState extends Equatable {
  const StatusState();
}

class StatusInitial extends StatusState {
  @override
  List<Object> get props => [];
}

class StatusLoading extends StatusState {
  @override
  List<Object> get props => [];
}

class StatusLoaded extends StatusState {
  final List<StatusEntity> status;

  StatusLoaded({this.status});

  @override
  List<Object> get props => [status];
}

class StatusSucceed extends StatusState {
  @override
  List<Object> get props => [];
}

class StatusFailure extends StatusState {
  @override
  List<Object> get props => [];
}
