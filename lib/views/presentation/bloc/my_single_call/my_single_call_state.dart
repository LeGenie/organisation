part of 'my_single_call_cubit.dart';

abstract class MySingleCallState extends Equatable {
  const MySingleCallState();
}

class MySingleCallInitial extends MySingleCallState {
  @override
  List<Object> get props => [];
}

class MySingleCallLoading extends MySingleCallState {
  @override
  List<Object> get props => [];
}

class MySingleCallLoaded extends MySingleCallState {
  final List<MySingleCallEntity> mySingleCall;

  MySingleCallLoaded({this.mySingleCall});

  @override
  List<Object> get props => [mySingleCall];
}

class MySingleCallFailure extends MySingleCallState {
  @override
  List<Object> get props => [];
}
