import 'dart:io';

import 'package:Organiv/views/domain/entities/my_single_call_entity.dart';
import 'package:Organiv/views/domain/usecases/call/get_my_single_call_usecase.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'my_single_call_state.dart';

class MySingleCallCubit extends Cubit<MySingleCallState> {
  final GetMySingleCallUseCase getMySingleCallUseCase;

  MySingleCallCubit({this.getMySingleCallUseCase})
      : super(MySingleCallInitial());

  Future<void> getMySingleCall({String uid}) async {
    emit(MySingleCallLoading());
    try {
      final myCallStreamData = await getMySingleCallUseCase.call(uid: uid);
      myCallStreamData.listen((myCallData) {
        emit(MySingleCallLoaded(mySingleCall: myCallData));
      });
    } on SocketException catch (_) {
      emit(MySingleCallFailure());
    } catch (_) {
      emit(MySingleCallFailure());
    }
  }
}
