import 'dart:async';

import 'package:Organiv/config/parameter/widget.dart';
import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/config/variable/language/fr.dart';
import 'package:Organiv/views/methods/services/share_preference.dart';
import 'package:Organiv/views/presentation/bloc/contact/contact_cubit.dart';
import 'package:Organiv/views/presentation/bloc/user/user_cubit.dart';
import 'package:Organiv/views/presentation/pages/call_page.dart';
import 'package:Organiv/views/presentation/pages/camera_page.dart';
import 'package:Organiv/views/presentation/pages/chat_page.dart';
import 'package:Organiv/views/presentation/pages/status_page.dart';
import 'package:Organiv/views/presentation/screens/call/dialScreen/dial_screen.dart';
import 'package:Organiv/views/presentation/screens/call/stream/index.dart';
import 'package:Organiv/views/presentation/screens/pickup/ringing.dart';
import 'package:Organiv/views/domain/entities/user_entity.dart';
import 'package:Organiv/views/presentation/widgets/style.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:Organiv/views/presentation/bloc/call/call_cubit.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver {
  get _userEntity => Global.user;
  bool _isSearch = false;
  TextEditingController _searController = new TextEditingController();
  PageController _pageViewController =
      new PageController(initialPage: Global.indexPage ?? 1);
  List<Widget> get _pages => [
        CameraPage(),
        ChatPage(),
        StatusPage(),
        CallPage(),
        IndexPage(),
      ];

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    UserEntity user;
    if (state != AppLifecycleState.resumed) {
      user = UserEntity(
        lastView: Timestamp.now(),
        uid: Global.user.uid,
        online: false,
      );
    } else {
      user = UserEntity(
        uid: Global.user.uid,
        lastView: Timestamp.now(),
        online: true,
      );
    }
    BlocProvider.of<UserCubit>(context).updateUser(user: user);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    HelperFunction.saveIndexPageSharePreference(Global.indexPage);
    HelperFunction.saveUIDSharePreference(Global.user.uid);
    super.dispose();
  }

  @override
  void initState() {
    if (_userEntity == null) {
      Timer(Duration(milliseconds: 10), () => initState());
      return;
    }
    init();
    super.initState();
    _pageViewController.addListener(() {});
    WidgetsBinding.instance.addObserver(this);
  }

  init() async {
    BlocProvider.of<UserCubit>(context).getCall();
    BlocProvider.of<UserCubit>(context).getMyUser();
    BlocProvider.of<UserCubit>(context)
        .createStatusChannel(uid: _userEntity.uid);
    BlocProvider.of<ContactCubit>(context).getContact();
    await HelperFunction.getIndexPageSharePreference().then((index) {
      Global.indexPage = index ?? 1;
      setState(() {
        _jumpPage(Global.indexPage);
      });
    });
  }

  _buildSearch() {
    return Container(
      height: 2 * Global.interParagraphSpace,
      margin: EdgeInsets.only(top: Global.interParagraphSpace),
      child: TextField(
        controller: _searController,
        style: TextStyle(color: Colors.white),
        onChanged: (value) {
          setState(() {});
        },
        decoration: textInputDecoration(context, Variable.searchWord,
            color: Colors.white,
            prefixIcon: InkWell(
              onTap: () {
                setState(() {
                  _isSearch = false;
                });
              },
              child: Icon(Icons.arrow_back),
            )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ContactCubit, ContactState>(
      builder: (context, contactState) {
        if (contactState is ContactLoaded) {
          Global.contacts = contactState.contacts;
          return BlocBuilder<UserCubit, UserState>(
            builder: (context, userState) {
              if (userState is UserCall) {
                return Ringing();
              }
              if (Global.user != null &&
                  Global.user.hasCall != null &&
                  Global.user.hasCall &&
                  Global.user.hasDialled) {
                return BlocBuilder<CallCubit, CallState>(
                  builder: (_, callState) {
                    if (callState is CallInProgress) {
                      return DialScreen(
                          call: callState.call, callId: callState.callId);
                    }
                    return _body();
                  },
                );
              }
              return _body();
            },
          );
        }
        return _body();
      },
    );
  }

  Widget _body() {
    return Scaffold(
      appBar: Global.indexPage != 0
          ? AppBar(
              elevation: 0.0,
              automaticallyImplyLeading: false,
              title: !_isSearch
                  ? Text(
                      Global.title,
                      style: TextStyle(color: Colors.white),
                    )
                  : Container(height: 0.0, width: 0.0),
              flexibleSpace: !_isSearch
                  ? Container(height: 0.0, width: 0.0)
                  : _buildSearch(),
              actions: [
                InkWell(
                    onTap: () {
                      setState(() {
                        _isSearch = true;
                      });
                    },
                    child: !_isSearch
                        ? Icon(
                            Icons.search,
                            color: Global.backgroundColor,
                          )
                        : _searController.text == ''
                            ? Container()
                            : GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _searController.text = "";
                                  });
                                },
                                child: Icon(
                                  Icons.close,
                                  color: Global.backgroundColor,
                                ),
                              )),
                SizedBox(
                  width: Global.interParagraphSpace / 5,
                ),
                Icon(
                  Icons.more_vert,
                  color: Global.backgroundColor,
                ),
                SizedBox(
                  width: Global.interParagraphSpace / 5,
                ),
              ],
            )
          : null,
      body: Column(
        children: <Widget>[
          !_isSearch
              ? Global.indexPage != 0
                  ? customerTabBar()
                  : Container(height: 0.0, width: 0.0)
              : Container(height: 0.0, width: 0.0),
          Expanded(
              child: PageView.builder(
            itemCount: _pages.length,
            controller: _pageViewController,
            onPageChanged: (index) {
              setState(() {
                Global.indexPage = index;
              });
            },
            itemBuilder: (_, index) {
              return _pages[index];
            },
          ))
        ],
      ),
    );
  }

  Widget customerTabBar() {
    return Container(
      height: 1.5 * Global.interParagraphSpace,
      decoration: BoxDecoration(color: Global.backgroundHeartColor),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(
                left: Global.interParagraphSpace / 3,
                right: Global.interParagraphSpace),
            width: Global.interParagraphSpace / 2,
            child: GestureDetector(
              onTap: () {
                setState(() {
                  _jumpPage(0);
                });
              },
              child: Icon(
                Icons.camera_alt,
                color: Colors.white,
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {
                setState(() {
                  _jumpPage(1);
                });
              },
              child: customerTabBarButton(
                text: Variable.headerHome[0],
                textColor:
                    Global.indexPage == 1 ? textIconColor : textIconColorGray,
                borderColor:
                    Global.indexPage == 1 ? textIconColor : Colors.transparent,
                count: Global.user.countMessage ?? 0,
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {
                setState(() {
                  _jumpPage(2);
                });
              },
              child: customerTabBarButton(
                text: Variable.headerHome[1],
                textColor:
                    Global.indexPage == 2 ? textIconColor : textIconColorGray,
                borderColor:
                    Global.indexPage == 2 ? textIconColor : Colors.transparent,
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {
                setState(() {
                  _jumpPage(3);
                });
              },
              child: customerTabBarButton(
                text: Variable.headerHome[2],
                textColor:
                    Global.indexPage == 3 ? textIconColor : textIconColorGray,
                borderColor:
                    Global.indexPage == 3 ? textIconColor : Colors.transparent,
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {
                setState(() {
                  _jumpPage(4);
                });
              },
              child: customerTabBarButton(
                text: Variable.headerHome[3],
                textColor:
                    Global.indexPage == 4 ? textIconColor : textIconColorGray,
                borderColor:
                    Global.indexPage == 4 ? textIconColor : Colors.transparent,
              ),
            ),
          ),
        ],
      ),
    );
  }

  customerTabBarButton(
      {String text, Color textColor, Color borderColor, int count = 0}) {
    final double boderWidth = 3.0;
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: borderColor, width: boderWidth),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            text,
            style: TextStyle(
              fontSize: Global.interParagraphSpace / 2,
              fontWeight: FontWeight.w500,
              color: textColor,
            ),
          ),
          count > 0
              ? Row(
                  children: [
                    SizedBox(width: 8.0),
                    Container(
                      width: .6 * Global.interParagraphSpace +
                          3 * (count.toString().length - 1),
                      height: .6 * Global.interParagraphSpace +
                          3 * (count.toString().length - 1),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(50))),
                      child: Center(
                        child: Text(
                          "$count",
                          style: TextStyle(
                              fontSize: 10,
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.italic),
                        ),
                      ),
                    ),
                  ],
                )
              : Container(),
        ],
      ),
    );
  }

  void _jumpPage(int i) {
    Global.indexPage = i;
    _pageViewController.jumpToPage(Global.indexPage);
  }
}
