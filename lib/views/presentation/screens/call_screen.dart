import 'package:Organiv/views/domain/entities/call_entity.dart';
import 'package:Organiv/views/presentation/bloc/call/call_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CallScreen extends StatefulWidget {
  final CallEntity call;
  final String callId;

  const CallScreen({Key key, @required this.call, @required this.callId})
      : super(key: key);

  @override
  _CallScreenState createState() => _CallScreenState();
}

class _CallScreenState extends State<CallScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            /*Text(
              "${widget.call.type == AppConst.call_audio ? "Audio " : "Video "}${Variable.callHasMade}${widget.call.receiverName} ...",
            ),
            SizedBox(height: 15),*/
            MaterialButton(
              color: Colors.red,
              child: Icon(
                Icons.call_end,
                color: Colors.white,
              ),
              onPressed: () async => await BlocProvider.of<CallCubit>(context)
                  .endCall(call: widget.call, callId: widget.callId),
            ),
          ],
        ),
      ),
    );
  }
}
