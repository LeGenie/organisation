import 'package:Organiv/views/presentation/widgets/style.dart';
import 'package:avatar_glow/avatar_glow.dart';
import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/config/variable/language/fr.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:flutter_vibrate/flutter_vibrate.dart';
import 'package:Organiv/views/presentation/bloc/call/call_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Ringing extends StatefulWidget {
  const Ringing({Key key}) : super(key: key);

  @override
  _RingingState createState() => _RingingState();
}

class _RingingState extends State<Ringing> with WidgetsBindingObserver {
  double son = 0.7;
  final List<Duration> duration = [];
  int count = 3;
  bool _canVibrate = true;
  Iterable<Duration> pauses;
  //final ClientRole _role = ClientRole.Broadcaster;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state != AppLifecycleState.resumed) {
      FlutterRingtonePlayer.stop();
    }
  }

  @override
  void initState() {
    //FlutterRingtonePlayer.playRingtone();
    _vibrate();
    _sound();
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  _sound() {
    FlutterRingtonePlayer.playRingtone(
      looping: true, // Android only - API >= 28
      volume: son, // Android only - API >= 28
    );
  }

  _vibrate() async {
    bool canVibrate = await Vibrate.canVibrate;
    for (int i = 0; i <= count; i++)
      duration.add(Duration(milliseconds: 500 * (i % 2 + 1)));
    pauses = duration;
    setState(() {
      _canVibrate = canVibrate;
      if (_canVibrate) Vibrate.vibrateWithPauses(pauses);
    });
  }

  @override
  void dispose() {
    FlutterRingtonePlayer.stop();
    Vibrate.vibrateWithPauses([Duration(milliseconds: 1)]);
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    double size = MediaQuery.of(context).size.aspectRatio *
        MediaQuery.of(context).size.width /
        3;
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              Colors.black.withOpacity(.35),
              Colors.black.withOpacity(.8),
              Colors.black.withOpacity(.35),
              Colors.black.withOpacity(.8),
              Colors.black.withOpacity(.35),
              Colors.black.withOpacity(.8),
              Colors.black.withOpacity(.35),
              Colors.black.withOpacity(.8),
              Colors.black.withOpacity(.35),
            ]),
            borderRadius: BorderRadius.circular(10)),
        height: double.infinity,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AvatarGlow(
              startDelay: Duration(milliseconds: 1000),
              glowColor: Colors.white,
              endRadius: 160.0,
              duration: Duration(milliseconds: 2000),
              repeat: true,
              showTwoGlows: true,
              repeatPauseDuration: Duration(milliseconds: 100),
              child: MaterialButton(
                onPressed: () {
                  print("GO");
                },
                elevation: 100.0,
                shape: CircleBorder(),
                child: Container(
                  width: 200.0,
                  height: 200.0,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                          blurRadius: 50,
                          offset: Offset.zero,
                          color: Global.backgroundHeartColor.withOpacity(.5),
                          spreadRadius: 50.0)
                    ],
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(160.0),
                  ),
                  child: Container(
                    height: double.infinity,
                    width: double.infinity,
                    child: CircleAvatar(
                      backgroundImage: AssetImage(Global.linkProfil),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 2 * size),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Container(
                      child: MaterialButton(
                        onPressed: () {},
                        child: Center(
                          child: Icon(
                            Icons.alarm,
                            size: size / 3,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Text(
                      Variable.remind,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                          fontWeight: FontWeight.w500),
                    )
                  ],
                ),
                Column(
                  children: [
                    Container(
                      child: MaterialButton(
                        onPressed: () {},
                        child: Center(
                          child: Icon(
                            Icons.message,
                            size: size / 3,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    Text(
                      Variable.message,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                          fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ],
            ),
            SizedBox(height: size / 2),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    RawMaterialButton(
                      onPressed: () => _rejectCall(),
                      child: Icon(
                        Icons.call_end,
                        color: Colors.white,
                        size: 35.0,
                      ),
                      shape: CircleBorder(),
                      elevation: 5.0,
                      fillColor: Colors.redAccent,
                      padding: const EdgeInsets.all(15.0),
                    ),
                    SizedBox(height: 8),
                    Text(
                      Variable.declinecall,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w500),
                    )
                  ],
                ),
                Column(
                  children: [
                    RawMaterialButton(
                      //onPressed: () => onJoin(),
                      onPressed: () {},
                      child: Icon(
                        Icons.call,
                        color: Colors.white,
                        size: size / 2,
                      ),
                      shape: CircleBorder(),
                      elevation: 5.0,
                      fillColor: greenColor,
                      padding: const EdgeInsets.all(15.0),
                    ),
                    SizedBox(height: 8),
                    Text(
                      Variable.acceptcall,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    )
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  _rejectCall() async {
    if (Global.user != null &&
        Global.user.callId != null &&
        Global.user.callId != "") {
      await BlocProvider.of<CallCubit>(context)
          .reject(uid: Global.user.uid, callId: Global.user.callId);
    }
  }

  /*Future<void> onJoin({int nb = 0}) async {
    final channel = await BlocProvider.of<CallCubit>(context)
        .acceptCall(uid: Global.user.uid, callId: Global.user.callId);
    if (nb == 10) return;
    if (channel.isNotEmpty && channel != null) {
      // await for camera and mic permissions before pushing video page
      await _handleCameraAndMic(Permission.camera);
      await _handleCameraAndMic(Permission.microphone);
      // push video page with given channel name
      final UserEntity user = UserEntity(
        uid: Global.user.uid,
        lastView: Timestamp.now(),
        stream: true,
      );
      final UserEntity otherUser = UserEntity(
        uid: Global.user.callId,
        lastView: Timestamp.now(),
        stream: true,
      );
      BlocProvider.of<UserCubit>(context).updateUser(user: otherUser);
      BlocProvider.of<UserCubit>(context)
          .updateUser(user: user)
          .whenComplete(() => Navigator.pop(context));
      await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) =>
              CallLayoutPage(channelName: channel, role: _role),
        ),
      );
    } else
      return await onJoin(nb: nb + 1);
  }

  Future<void> _handleCameraAndMic(Permission permission) async {
    await permission.request();
  }*/
}
