import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/config/variable/language/fr.dart';
import 'package:Organiv/views/domain/entities/call_entity.dart';
import 'package:Organiv/views/presentation/bloc/call/call_cubit.dart';
import 'package:Organiv/views/presentation/screens/call_screen.dart';
import 'package:Organiv/views/presentation/widgets/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PickupScreen extends StatefulWidget {
  final CallEntity call;
  final String callId;

  const PickupScreen({Key key, @required this.call, @required this.callId})
      : super(key: key);

  @override
  _PickupScreenState createState() => _PickupScreenState();
}

class _PickupScreenState extends State<PickupScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(vertical: 100),
        child: Column(
          children: [
            Text(
              Variable.incoming,
              style: TextStyle(fontSize: Global.interParagraphSpace),
            ),
            SizedBox(height: 50),
            Image.network(
              widget.call.callerPic,
              height: 5 * Global.interParagraphSpace,
              width: 5 * Global.interParagraphSpace,
            ),
            SizedBox(height: 50),
            Text(
              widget.call.callerName,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
            SizedBox(height: 75),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(
                  onPressed: () async {
                    await BlocProvider.of<CallCubit>(context)
                        .endCall(call: widget.call);
                  },
                  icon: Icon(Icons.call_end),
                  color: Colors.redAccent,
                ),
                SizedBox(width: 25),
                IconButton(
                  onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CallScreen(
                            call: widget.call, callId: widget.callId)),
                  ),
                  icon: Icon(Icons.call),
                  color: greenColor,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
