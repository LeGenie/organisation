import 'package:Organiv/views/data/model/call_model.dart';
import 'package:Organiv/views/domain/entities/call_entity.dart';
import 'package:Organiv/views/presentation/bloc/call/call_cubit.dart';
import 'package:Organiv/views/presentation/screens/pickup/pickup_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PickupLayout extends StatefulWidget {
  final String channelId;
  final CallEntity call;

  const PickupLayout({
    Key key,
    @required this.channelId,
    @required this.call,
  }) : super(key: key);

  @override
  _PickupLayoutState createState() => _PickupLayoutState();
}

class _PickupLayoutState extends State<PickupLayout> {
  Stream<DocumentSnapshot> stream;

  @override
  void initState() {
    BlocProvider.of<CallCubit>(context)
        .callStream(widget.channelId, widget.call.channelId, widget.call.type);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<CallCubit, CallState>(
        builder: (_, callState) {
          if (callState is CallStream) {
            stream = callState.stream;
            _body();
          }
          return Container(child: Center(child: CircularProgressIndicator()));
        },
      ),
    );
  }

  Widget _body() {
    return StreamBuilder<DocumentSnapshot>(
      stream: stream,
      builder: (context, snapshot) {
        if (snapshot.hasData && snapshot.data.data() != null) {
          CallModel callModel = CallModel.fromSnapShot(snapshot.data);
          return PickupScreen(call: callModel, callId: callModel.channelId);
        }
        return _ringing();
      },
    );
  }

  Widget _ringing() {
    return Container();
  }
}
