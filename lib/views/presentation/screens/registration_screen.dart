import 'package:Organiv/config/parameter/widget.dart';
import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/config/variable/language/fr.dart';
import 'package:Organiv/views/methods/util/validate_phone_number.dart';
import 'package:Organiv/views/data/model/user_model.dart';
import 'package:Organiv/views/presentation/bloc/auth/auth_cubit.dart';
import 'package:Organiv/views/presentation/bloc/phone_auth/phone_auth_cubit.dart';
import 'package:Organiv/views/presentation/bloc/user/user_cubit.dart';
import 'package:Organiv/views/presentation/pages/phone_verification_page.dart';
import 'package:Organiv/views/presentation/pages/set_initial_profil.dart';
import 'package:Organiv/views/presentation/screens/home_screen.dart';
import 'package:Organiv/views/presentation/screens/pickup/ringing.dart';
import 'package:Organiv/views/presentation/widgets/style.dart';
import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RegistrationScreen extends StatefulWidget {
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  static Country _selectedFilterdDialogCountry =
      CountryPickerUtils.getCountryByIsoCode("CM");
  String _countryCode = _selectedFilterdDialogCountry.phoneCode;
  String _internationalizedPhoneNumber = "";
  TextEditingController _phoneAuthController = new TextEditingController();
  bool _isValidate;

  @override
  void dispose() {
    _phoneAuthController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _isValidate = false;
    _phoneAuthController.addListener(() {
      ValidatePhoneNumber.validatePhoneNumber(onPhoneNumberChange,
          _phoneAuthController.text, _selectedFilterdDialogCountry.isoCode);
    });
  }

  @override
  Widget build(BuildContext context) {
    Global _global = new Global(context);
    return Scaffold(
      body: BlocConsumer<PhoneAuthCubit, PhoneAuthState>(
        listener: (context, phoneAuthState) {
          if (phoneAuthState is PhoneAuthSuccess) {
            BlocProvider.of<AuthCubit>(context).loggedIn();
          }

          if (phoneAuthState is PhoneAuthFailure) {
            Scaffold.of(context).showSnackBar(
              SnackBar(
                backgroundColor: Global.backgroundRefus,
                content: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(Variable.logginError),
                      Icon(Icons.error),
                    ],
                  ),
                ),
              ),
            );
          }
        },
        builder: (context, phoneAuthState) {
          if (phoneAuthState is PhoneAuthSmsCodeReceived) {
            return PhoneVerificationPage(
                phoneNumber: _internationalizedPhoneNumber);
          }

          if (phoneAuthState is PhoneAuthProfilInfo) {
            return SetInitialProfil(
              internationalizedPhoneNumber: _internationalizedPhoneNumber,
              isoCode: _selectedFilterdDialogCountry.isoCode,
              phoneNumber: _phoneAuthController.text,
            );
          }

          if (phoneAuthState is PhoneAuthLoading) {
            return Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }

          if (phoneAuthState is PhoneAuthSuccess) {
            return BlocBuilder<AuthCubit, AuthState>(
              builder: (context, authState) {
                if (authState is Authenticated) {
                  return BlocBuilder<UserCubit, UserState>(
                    builder: (context, userState) {
                      if (userState is UserLoaded) {
                        final _currentUserInfo = userState.users.firstWhere(
                            (user) => user.uid == authState.uid,
                            orElse: () => UserModel());
                        Global.user = _currentUserInfo;
                        return HomeScreen();
                      }
                      if (userState is UserCall) {
                        return Ringing();
                      }
                      return Container(
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      );
                    },
                  );
                }
                return Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              },
            );
          }

          return _bodyWidget(_global);
        },
      ),
    );
  }

  Widget _bodyWidget(Global _global) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
        child: Column(
          children: [
            SizedBox(height: Global.interParagraphSpace / 2),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(Icons.arrow_back),
                ),
                Expanded(
                  child: Center(
                    child: Text(
                      Variable.signInPhone,
                      style: TextStyle(
                          fontSize: Global.titleSize,
                          color: Global.backgroundHeartColor,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: Global.interParagraphSpace),
            Text(
              Variable.commentDescSignPhone,
              textAlign: TextAlign.justify,
              style: TextStyle(fontSize: Global.commentSize),
            ),
            ListTile(
              onTap: _openFilteredCountryPickerDialog,
              title:
                  _buildDialogItem(_selectedFilterdDialogCountry, wrap: false),
            ),
            SizedBox(height: Global.interParagraphSpace),
            Row(
              children: [
                Container(
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide.none,
                    ),
                  ),
                  width: 2 * Global.interParagraphSpace,
                  height: Global.interParagraphSpace,
                  alignment: Alignment.center,
                  child: Text(
                    "+ $_countryCode",
                    style: _global.bodyStyle,
                  ),
                ),
                SizedBox(height: Global.interParagraphSpace),
                Expanded(
                  child: Container(
                    height: Global.interParagraphSpace,
                    child: TextField(
                      controller: _phoneAuthController,
                      maxLines: 1,
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                      decoration:
                          textInputDecoration(context, Variable.phoneWord),
                    ),
                  ),
                ),
                SizedBox(height: Global.interParagraphSpace),
              ],
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: MaterialButton(
                  color: _isValidate
                      ? Global.backgroundHeartColor
                      : Colors.grey[600],
                  disabledColor: Colors.grey[600],
                  onPressed: _submitVerifyPhoneNumber,
                  child: Text(
                    Variable.next,
                    style: _global.bodyStyle.copyWith(color: Colors.white),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _openFilteredCountryPickerDialog() {
    Global _global = new Global(context);
    showDialog(
      context: context,
      builder: (_) => Theme(
        data: Theme.of(context).copyWith(primaryColor: primaryColor),
        child: CountryPickerDialog(
          isSearchable: true,
          titlePadding: EdgeInsets.all(8.0),
          searchCursorColor: Colors.black,
          searchInputDecoration: InputDecoration(hintText: Variable.searchWord),
          title: Text(Variable.selectPhoneCountry,
              style: _global.bodyStyle.copyWith(
                  fontSize: Global.titleSize,
                  color: Global.backgroundHeartColor,
                  fontWeight: FontWeight.w500)),
          onValuePicked: (Country country) {
            setState(() {
              _selectedFilterdDialogCountry = country;
              _countryCode = _selectedFilterdDialogCountry.phoneCode;
            });
          },
          itemBuilder: _buildDialogItem,
        ),
      ),
    );
  }

  void onPhoneNumberChange(
      String number, String internationalizedPhoneNumber, String isoCode) {
    setState(() {
      if (internationalizedPhoneNumber == null ||
          internationalizedPhoneNumber == "")
        _isValidate = false;
      else {
        _isValidate = true;
      }
    });
  }

  void _submitVerifyPhoneNumber() {
    _internationalizedPhoneNumber =
        "+$_countryCode${_phoneAuthController.text}";
    if (_phoneAuthController.text.isNotEmpty) {
      BlocProvider.of<PhoneAuthCubit>(context)
          .submitVerifyPhoneNumber(phoneNumber: _internationalizedPhoneNumber);
    }
  }
}

Widget _buildDialogItem(Country country, {bool wrap = true}) {
  return Container(
    height: Global.interParagraphSpace,
    alignment: Alignment.center,
    decoration: BoxDecoration(
      border: Border(
        bottom: BorderSide(color: greenColor, width: 1),
      ),
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        CountryPickerUtils.getDefaultFlagImage(country),
        Text(" + ${country.phoneCode} "),
        wrap ? Text(Global.truncString(country.name)) : Text(country.name),
        Spacer(),
        Icon(Icons.arrow_drop_down)
      ],
    ),
  );
}
