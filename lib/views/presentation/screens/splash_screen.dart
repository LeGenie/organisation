import 'dart:async';

import 'package:Organiv/config/variable/global.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  final Widget home;
  final int time;

  const SplashScreen({Key key, this.home, this.time}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Timer(Duration(seconds: widget.time), () {
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (_) => widget.home),
        (route) => false,
      );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Global.backgroundHeartColor,
      body: Center(
          child: Container(
        height: Global.circleSize,
        width: Global.circleSize,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(Global.circleSize)),
        padding: EdgeInsets.symmetric(horizontal: 5),
        child: CircleAvatar(
          radius: MediaQuery.of(context).size.height / 4,
          backgroundImage: new AssetImage(
            Global.linkLogo,
          ),
        ),
      )),
    );
  }
}
