import 'package:Organiv/app_const.dart';
import 'package:Organiv/config/parameter/size_config.dart';
import 'package:Organiv/component/dial_user_pic.dart';
import 'package:Organiv/component/rounded_button.dart';
import 'package:flutter/material.dart';
import 'package:Organiv/views/domain/entities/call_entity.dart';
import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/config/variable/language/fr.dart';
import 'package:camera/camera.dart';
import 'package:Organiv/views/presentation/bloc/call/call_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'dial_button.dart';

class Body extends StatefulWidget {
  final CallEntity call;
  final String callId;

  const Body({Key key, this.call, this.callId}) : super(key: key);
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  List<CameraDescription> cameras;
  CameraController _cameraController;
  final List<ResolutionPreset> _resolution = [
    ResolutionPreset.low,
    ResolutionPreset.medium,
    ResolutionPreset.high,
    ResolutionPreset.veryHigh,
    ResolutionPreset.ultraHigh
  ];
  int _indexResolution = 2;
  int _camera = 0;
  bool _enable = false;

  @override
  void initState() {
    print(widget.call.type);
    if (widget.call.type == AppConst.call_video) initializeCamera();
    super.initState();
  }

  @override
  void dispose() {
    if (widget.call.type == AppConst.call_video) _cameraController.dispose();
    super.dispose();
  }

  //Camera show
  Future<void> initializeCamera() async {
    cameras = await availableCameras();
    if (cameras.length == 0) return;
    if (cameras.length == 2) _camera = 1;
    _cameraController =
        CameraController(cameras[_camera], _resolution[_indexResolution]);
    _mounted();
  }

  Future<void> _mounted() async {
    _cameraController.initialize().then((value) {
      if (!mounted) return;
      setState(() {
        _enable = true;
      });
    });
  }

  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: double.infinity,
          width: double.infinity,
          child: !_enable ? Container() : CameraPreview(_cameraController),
        ),
        _body()
      ],
    );
  }

  Widget _body() {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            Text(
              widget.call.receiverName,
              style: Theme.of(context)
                  .textTheme
                  .headline4
                  .copyWith(color: Colors.white),
            ),
            Text(
              "${Variable.callHasMade(widget.call.type == AppConst.call_audio ? "Audio " : "Video ")}",
              style: TextStyle(color: Colors.white60),
            ),
            VerticalSpacing(),
            DialUserPic(image: widget.call.receiverPic ?? Global.linkProfil),
            Spacer(),
            Wrap(
              alignment: WrapAlignment.spaceBetween,
              children: [
                DialButton(
                  iconSrc: "assets/icons/Icon Mic.svg",
                  text: "Audio",
                  press: () {},
                ),
                DialButton(
                  iconSrc: "assets/icons/Icon Volume.svg",
                  text: "Microphone",
                  press: () {},
                ),
                DialButton(
                  iconSrc: "assets/icons/Icon Video.svg",
                  text: "Video",
                  press: () {},
                ),
                DialButton(
                  iconSrc: "assets/icons/Icon Message.svg",
                  text: "Message",
                  press: () {},
                ),
                DialButton(
                  iconSrc: "assets/icons/Icon User.svg",
                  text: "Add contact",
                  press: () {},
                ),
                DialButton(
                  iconSrc: "assets/icons/Icon Voicemail.svg",
                  text: "Voice mail",
                  press: () {},
                ),
              ],
            ),
            VerticalSpacing(),
            RoundedButton(
              iconSrc: "assets/icons/call_end.svg",
              press: () async => await BlocProvider.of<CallCubit>(context)
                  .endCall(call: widget.call, callId: widget.callId),
              color: kRedColor,
              iconColor: Colors.white,
            )
          ],
        ),
      ),
    );
  }
}
