import 'package:Organiv/config/parameter/size_config.dart';
import 'package:flutter/material.dart';
import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/views/domain/entities/call_entity.dart';

import 'components/body.dart';

class DialScreen extends StatelessWidget {
  final CallEntity call;
  final String callId;

  const DialScreen({Key key, this.call, this.callId}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Global.backgroundHeartColor.withOpacity(.3),
      body: Body(call: call, callId: callId),
    );
  }
}
