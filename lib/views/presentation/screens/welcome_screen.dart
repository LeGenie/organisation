import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/config/variable/language/fr.dart';
import 'package:Organiv/views/presentation/screens/registration_screen.dart';
import 'package:flutter/material.dart';

class WelcomScreen extends StatefulWidget {
  @override
  _WelcomScreenState createState() => _WelcomScreenState();
}

class _WelcomScreenState extends State<WelcomScreen> {
  @override
  Widget build(BuildContext context) {
    Global _global = new Global(context);
    return Scaffold(
      body: Container(
        margin: EdgeInsets.symmetric(
          horizontal: Global.interParagraphSpace,
          vertical: Global.interParagraphSpace,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Center(
              child: Column(
                children: [
                  SizedBox(height: Global.interParagraphSpace / 2),
                  Text(
                    Variable.welcome,
                    style: TextStyle(
                        fontSize: Global.titleSize,
                        color: Global.backgroundHeartColor,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height / 2,
              width: 4 * MediaQuery.of(context).size.width / 5,
              child: Image.asset(Global.linkWelcome),
            ),
            Column(
              children: [
                Container(
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    Variable.commentDescWelcome,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: Global.commentSize),
                  ),
                )
              ],
            ),
            MaterialButton(
              color: Global.backgroundHeartColor,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => RegistrationScreen(),
                  ),
                );
              },
              child: Text(
                Variable.agree,
                style: _global.bodyStyle
                    .copyWith(color: Colors.white, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
