import 'package:Organiv/config/parameter/widget.dart';
import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/config/variable/language/fr.dart';
import 'package:Organiv/views/presentation/bloc/auth/auth_cubit.dart';
import 'package:Organiv/views/presentation/bloc/contact/contact_cubit.dart';
import 'package:Organiv/views/presentation/bloc/phone_auth/phone_auth_cubit.dart';
import 'package:Organiv/views/presentation/bloc/user/user_cubit.dart';
import 'package:Organiv/views/presentation/widgets/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SetInitialProfil extends StatefulWidget {
  final String phoneNumber;
  final String internationalizedPhoneNumber;
  final String isoCode;

  const SetInitialProfil({
    Key key,
    this.phoneNumber,
    this.internationalizedPhoneNumber,
    this.isoCode,
  }) : super(key: key);

  @override
  _SetInitialProfilState createState() => _SetInitialProfilState();
}

class _SetInitialProfilState extends State<SetInitialProfil> {
  String get _phoneNumber => widget.phoneNumber;
  String get _internationalizedPhoneNumber =>
      widget.internationalizedPhoneNumber;
  String get _isoCode => widget.isoCode;
  TextEditingController _nameController = new TextEditingController();

  @override
  void initState() {
    BlocProvider.of<UserCubit>(context).getAllUsers();
    BlocProvider.of<AuthCubit>(context).appStarted();
    BlocProvider.of<ContactCubit>(context)
        .create()
        .then((value) => BlocProvider.of<ContactCubit>(context).getContact());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ContactCubit, ContactState>(
      builder: (context, contactState) {
        if (contactState is ContactLoaded) {
          Global.contacts = contactState.contacts;
          return _body();
        }
        return _body();
      },
    );
  }

  Widget _body() {
    Global _global = new Global(context);
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
        child: Column(
          children: [
            SizedBox(height: Global.interParagraphSpace / 2),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  Variable.titleProfil,
                  style: TextStyle(
                    fontSize: Global.titleSize,
                    color: Global.backgroundHeartColor,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
            SizedBox(height: Global.interParagraphSpace),
            Text(
              Variable.commentDescProfil,
              textAlign: TextAlign.justify,
              style: TextStyle(fontSize: Global.commentSize),
            ),
            SizedBox(height: Global.interParagraphSpace),
            _rowWidget(),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: MaterialButton(
                  color: Global.backgroundHeartColor,
                  onPressed: _submitProfileInfo,
                  child: Text(
                    Variable.next,
                    style: _global.bodyStyle.copyWith(color: Colors.white),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _rowWidget() {
    return Container(
      child: Row(
        children: [
          Container(
            height: 1.5 * Global.interParagraphSpace,
            width: 1.5 * Global.interParagraphSpace,
            decoration: BoxDecoration(
                color: textIconColor,
                borderRadius: BorderRadius.all(Radius.circular(25))),
            child: Icon(Icons.camera_alt),
          ),
          SizedBox(width: Global.interParagraphSpace / 2),
          Expanded(
            child: TextField(
              controller: _nameController,
              maxLines: 1,
              maxLength: Global.maxLenghtName,
              decoration: textInputDecoration(context, Variable.nameWord),
            ),
          ),
          SizedBox(width: Global.interParagraphSpace / 2),
          Container(
            height: Global.interParagraphSpace,
            width: Global.interParagraphSpace,
            decoration: BoxDecoration(
              color: Colors.transparent,
              borderRadius: BorderRadius.all(
                Radius.circular(25),
              ),
            ),
            child: Icon(Icons.insert_emoticon),
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  void _submitProfileInfo() async {
    if (_nameController.text.isNotEmpty) {
      String _macAdress = await Global.macAdressPhone;
      String _markAdress = await Global.markPhone;
      String _ipAdress = "";
      await BlocProvider.of<PhoneAuthCubit>(context).submitProfilInfo(
        name: _nameController.text,
        phoneNumber: _phoneNumber,
        profilUrl: "",
        internationalizedPhoneNumber: _internationalizedPhoneNumber,
        isoCode: _isoCode,
        macAdress: _macAdress,
        markAdress: _markAdress,
        ipAdress: _ipAdress,
      );
    }
  }
}
