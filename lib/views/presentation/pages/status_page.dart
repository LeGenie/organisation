import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/config/variable/language/fr.dart';
import 'package:Organiv/views/domain/entities/contact_user_entity.dart';
import 'package:Organiv/views/domain/entities/user_entity.dart';
import 'package:Organiv/views/domain/entities/my_single_status_entity.dart';
import 'package:Organiv/views/domain/entities/status_entity.dart';
import 'package:Organiv/views/presentation/bloc/my_single_status/my_single_status_cubit.dart';
import 'package:Organiv/views/presentation/bloc/status/status_cubit.dart';
import 'package:Organiv/views/presentation/bloc/user/user_cubit.dart';
import 'package:Organiv/views/presentation/pages/sub_pages/single_item_story_page.dart';
import 'package:Organiv/views/presentation/pages/sub_pages/status_text_page.dart';
import 'package:Organiv/views/presentation/pages/sub_pages/story_page_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class StatusPage extends StatefulWidget {
  const StatusPage({Key key}) : super(key: key);

  @override
  _StatusPageState createState() => _StatusPageState();
}

class _StatusPageState extends State<StatusPage> {
  get _userEntity => Global.user;
  List<StatusEntity> myStatus = [];
  List<UserEntity> users = [];
  List<MySingleStatusEntity> mySingleStatusEntity = [];

  @override
  void initState() {
    BlocProvider.of<StatusCubit>(context)
        .getMyStatus(uid: _userEntity.uid, readUid: null);
    BlocProvider.of<MySingleStatusCubit>(context)
        .getMySingleStatus(uid: _userEntity.uid);
    BlocProvider.of<UserCubit>(context).getAllUsers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<UserCubit, UserState>(
        builder: (_, userState) {
          if (userState is UserLoaded) {
            users = userState.users;
            return BlocBuilder<StatusCubit, StatusState>(
              builder: (_, statusState) {
                if (statusState is StatusLoaded) {
                  myStatus = statusState.status;
                  return BlocBuilder<MySingleStatusCubit, MySingleStatusState>(
                    builder: (_, mySingleStatusState) {
                      if (mySingleStatusState is MySingleStatusLoaded) {
                        mySingleStatusState.mySingleStatus.removeWhere(
                            (element) =>
                                element.channelId == _userEntity.uid ||
                                element.count == 0);
                        mySingleStatusEntity =
                            mySingleStatusState.mySingleStatus;
                        return _body();
                      }
                      return _body();
                    },
                  );
                }
                return _loadingWidget();
              },
            );
          }
          return _loadingWidget();
        },
      ),
    );
  }

  Widget _customerFloatingActionButton() {
    return Positioned(
      bottom: 30,
      right: 10,
      child: Align(
        alignment: Alignment.bottomRight,
        child: Column(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (_) => StatusTextPage()));
              },
              child: Container(
                height: 1.5 * Global.interParagraphSpace,
                width: 1.5 * Global.interParagraphSpace,
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.all(Radius.circular(25)),
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(0, 4.0),
                        blurRadius: 0.50,
                        color: Colors.black.withOpacity(.2),
                        spreadRadius: 0.10)
                  ],
                ),
                child: Icon(
                  Icons.edit,
                  size: Global.interParagraphSpace,
                  color: Colors.blueGrey,
                ),
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            Container(
              height: 2 * Global.interParagraphSpace - 5,
              width: 2 * Global.interParagraphSpace - 5,
              decoration: BoxDecoration(
                color: Global.backgroundHeartColor,
                borderRadius: BorderRadius.all(Radius.circular(50)),
                boxShadow: [
                  BoxShadow(
                      offset: Offset(0, 4.0),
                      blurRadius: 0.50,
                      color: Colors.black.withOpacity(.2),
                      spreadRadius: 0.10)
                ],
              ),
              child: Icon(
                Icons.camera_alt,
                size: Global.interParagraphSpace,
                color: Global.backgroundTypeMessage,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _storyWidget() {
    return Container(
      margin: EdgeInsets.only(left: 10, right: 10, top: 4),
      child: GestureDetector(
        onTap: () {
          if (myStatus.isNotEmpty) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => StoryPageView(
                  status: myStatus,
                  user: _userEntity,
                ),
              ),
            );
          }
        },
        child: Container(
          child: Row(
            children: [
              Container(
                height: 2 * Global.interParagraphSpace + 15,
                width: 2 * Global.interParagraphSpace + 15,
                child: Stack(
                  children: [
                    Container(
                      height: 2 * Global.interParagraphSpace + 10,
                      width: 2 * Global.interParagraphSpace + 10,
                      child: Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: CircleAvatar(
                          backgroundImage: AssetImage(Global.linkProfil),
                        ),
                      ),
                    ),
                    Positioned(
                      right: 5,
                      bottom: 5,
                      child: Container(
                        alignment: Alignment.center,
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                          color: Global.backgroundHeartColor,
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                        child: Icon(Icons.add, color: Colors.white, size: 15),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 12),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    Variable.myStatus,
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                  ),
                  SizedBox(height: 2),
                  Text(
                    Variable.myStatusSubtitle,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontStyle: FontStyle.italic),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _recentTextWidget(String text) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
      decoration: BoxDecoration(color: Colors.grey[200]),
      child: Text(
        text,
        style: TextStyle(fontStyle: FontStyle.italic),
      ),
    );
  }

  Widget _listStories(List<MySingleStatusEntity> mySingleStatusEntity) {
    mySingleStatusEntity.removeWhere((element) => _getContact(element) == null);
    return ListView.builder(
      itemCount: mySingleStatusEntity.length,
      shrinkWrap: true,
      physics: ScrollPhysics(),
      itemBuilder: (BuildContext context, int index) {
        return SingleItemStoryPage(
          mySingleStatusEntity: mySingleStatusEntity[index],
          userEntity: _userEntity,
          last: index == mySingleStatusEntity.length - 1,
        );
      },
    );
  }

  UserEntity _getContact(MySingleStatusEntity statusEntity) {
    UserEntity _user;
    ContactUserEntity _contact;
    users.forEach((element) {
      if (element.uid == statusEntity.channelId) return _user = element;
    });
    if (_user != null) {
      Global.contacts.forEach((contact) {
        if (contact.uid == _user.uid) return _contact = contact;
      });
    }
    if (_contact == null)
      return null;
    else
      return _user;
  }

  _body() {
    return Stack(
      alignment: Alignment.center,
      children: [
        SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height -
                4 * Global.interParagraphSpace,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                _storyWidget(),
                SizedBox(height: 8.0),
                _recentTextWidget(Variable.recentStatus),
                mySingleStatusEntity.isEmpty
                    ? Container(height: 0.0)
                    : Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          SizedBox(height: 8.0),
                          _listStories(mySingleStatusEntity),
                        ],
                      ),
                /*_recentTextWidget(Variable.viewStatus),
                mySingleStatusEntity.isEmpty
                    ? _emptyListDisplayStatusWidget()
                    : _listStories(mySingleStatusEntity),
                mySingleStatusEntity.isEmpty
                    ? Container(height: 0.0)
                    : Column(
                        children: [
                          SizedBox(height: 8.0),
                          _recentTextWidget(Variable.ortherStatus),
                          SizedBox(height: 8.0),
                          _listStories(mySingleStatusEntity),
                        ],
                      ),
                mySingleStatusEntity.isEmpty
                    ? Container(height: 0.0)
                    : Column(
                        children: [
                          SizedBox(height: 8.0),
                          _recentTextWidget(Variable.silenceStatus),
                          SizedBox(height: 8.0),
                          _listStories(mySingleStatusEntity),
                        ],
                      ),*/
              ],
            ),
          ),
        ),
        _customerFloatingActionButton(),
      ],
    );
  }

  Widget _emptyListDisplayStatusWidget() {
    return Container(
      height:
          MediaQuery.of(context).size.height - 8 * Global.interParagraphSpace,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 5 * Global.interParagraphSpace,
            width: 5 * Global.interParagraphSpace,
            decoration: BoxDecoration(
              color: Global.backgroundHeartColor.withOpacity(.5),
              borderRadius: BorderRadius.all(Radius.circular(100)),
            ),
            child: Icon(Icons.access_alarm,
                color: Colors.white.withOpacity(.6), size: 50),
          ),
          Align(
            child: Padding(
              padding: const EdgeInsets.all(15),
              child: Text(
                Variable.welcomeChat,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 14, color: Colors.black.withOpacity(.4)),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _loadingWidget() {
    return Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
