import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/config/variable/language/fr.dart';
import 'package:Organiv/views/presentation/widgets/style.dart';
import 'package:flutter/material.dart';

class SingleItemCallPage extends StatefulWidget {
  @override
  _SingleItemCallPageState createState() => _SingleItemCallPageState();
}

class _SingleItemCallPageState extends State<SingleItemCallPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 1, right: 10, left: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Container(
                    height: 2 * Global.interParagraphSpace - 5,
                    width: 2 * Global.interParagraphSpace - 5,
                    child: CircleAvatar(
                      backgroundImage: AssetImage(Global.linkProfil),
                    ),
                  ),
                  SizedBox(width: 10),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        Variable.nameWord,
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w500),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.call_received,
                            color: greenColor,
                          ),
                          Text(
                            "Yesterday",
                            style: TextStyle(fontStyle: FontStyle.italic),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      )
                    ],
                  )
                ],
              ),
              Container(
                height: 2 * Global.interParagraphSpace - 5,
                width: 2 * Global.interParagraphSpace - 5,
                color: Colors.black,
                child: Icon(
                  Icons.call,
                  color: greenColor,
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 60, right: 10),
            child: Divider(
              thickness: 1.50,
            ),
          )
        ],
      ),
    );
  }
}
