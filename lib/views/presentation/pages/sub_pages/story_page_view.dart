import 'package:Organiv/config/variable/global.dart';
import 'package:flutter/material.dart';
import 'package:Organiv/views/domain/entities/status_entity.dart';
import 'package:Organiv/views/domain/entities/user_entity.dart';
import 'package:story_view/controller/story_controller.dart';
import 'package:story_view/story_view.dart';
import 'package:Organiv/app_const.dart';
import 'package:flutter/foundation.dart';

class StoryPageView extends StatefulWidget {
  final List<StatusEntity> status;
  final UserEntity user;

  const StoryPageView({Key key, this.status, this.user}) : super(key: key);

  @override
  _StoryPageViewState createState() => _StoryPageViewState();
}

class _StoryPageViewState extends State<StoryPageView> {
  final StoryController _controlerStory = StoryController();
  List<StoryItem> _storyItems = [];

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          _header(),
          Expanded(
            child: StoryView(
              controller: _controlerStory,
              storyItems: _storyItems,
              inline: false,
              repeat: false,
              progressPosition: ProgressPosition.top,
              onComplete: () => Navigator.pop(context),
              onStoryShow: (s) => print("Showing a story"),
            ),
          ),
        ],
      ),
    );
  }

  _init() {
    widget.status.forEach((item) {
      if (item.statusType == AppConst.text) {
        _storyItems.add(
          StoryItem.text(
            title: item.message,
            roundedBottom: false,
            roundedTop: false,
            textStyle: TextStyle(
                decoration: TextDecoration.none,
                fontSize: item.size.ceilToDouble()),
            duration: const Duration(seconds: 5),
            backgroundColor: new Color(
              int.parse(item.color.split('(0x')[1].split(')')[0], radix: 16),
            ),
          ),
        );
      } else if (item.statusType == AppConst.image) {
        StoryItem.pageImage(
          url: item.link,
          caption: item.legende,
          duration: const Duration(seconds: 5),
          controller: _controlerStory,
        );
      } else if (item.statusType == AppConst.video) {
        StoryItem.pageVideo(
          item.link,
          duration: Duration(seconds: Global.durationVideo),
          caption: item.legende,
          controller: _controlerStory,
        );
      }
    });
  }

  Widget _header() {
    return Container(
      child: Container(
        child: Row(
          children: [
            Container(
              color: Colors.black,
            )
          ],
        ),
      ),
    );
  }
}
