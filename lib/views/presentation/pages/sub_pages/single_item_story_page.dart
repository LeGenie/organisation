import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/config/variable/language/fr.dart';
import 'package:Organiv/views/domain/entities/my_single_status_entity.dart';
import 'package:Organiv/views/domain/entities/contact_user_entity.dart';
import 'package:Organiv/views/domain/entities/status_entity.dart';
import 'package:Organiv/views/domain/entities/user_entity.dart';
import 'package:Organiv/views/presentation/pages/sub_pages/story_page_view.dart';
import 'package:Organiv/views/presentation/bloc/status/status_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SingleItemStoryPage extends StatefulWidget {
  final MySingleStatusEntity mySingleStatusEntity;
  final UserEntity userEntity;
  final bool last;

  const SingleItemStoryPage(
      {Key key, this.mySingleStatusEntity, this.last = false, this.userEntity})
      : super(key: key);

  @override
  _SingleItemStoryPageState createState() => _SingleItemStoryPageState();
}

class _SingleItemStoryPageState extends State<SingleItemStoryPage> {
  List<StatusEntity> myStatus = [];
  ContactUserEntity contactUserEntity;
  @override
  void initState() {
    Global.contacts.forEach((contact) {
      if (contact.uid == widget.mySingleStatusEntity.channelId)
        return contactUserEntity = contact;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocBuilder<StatusCubit, StatusState>(
        builder: (_, statusState) {
          if (statusState is StatusLoaded) {
            myStatus = statusState.status;
            return _body();
          }
          return _body();
        },
      ),
    );
  }

  Widget _body() {
    return Container(
      margin: EdgeInsets.only(top: 1, right: 10, left: 10),
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              if (myStatus.isNotEmpty) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => StoryPageView(
                      status: myStatus,
                      user: widget.userEntity,
                    ),
                  ),
                );
              }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                      height: 2 * Global.interParagraphSpace - 5,
                      width: 2 * Global.interParagraphSpace - 5,
                      child: CircleAvatar(
                        backgroundImage: AssetImage(Global.linkProfil),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          contactUserEntity != null
                              ? contactUserEntity.label
                              : widget.userEntity.internationalizedPhoneNumber,
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w500),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "${Global.date(timestamp: widget.mySingleStatusEntity.time, format: Global.weekFormat)} ${Variable.conjonctionA} ${Global.date(timestamp: widget.mySingleStatusEntity.time, format: Global.hourFormat)}",
                          style: TextStyle(fontStyle: FontStyle.italic),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        )
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 60, right: 10),
            child: !widget.last
                ? Divider(
                    thickness: 1.50,
                  )
                : Container(height: 0.0),
          )
        ],
      ),
    );
  }
}
