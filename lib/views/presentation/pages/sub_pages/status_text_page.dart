import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/config/variable/language/fr.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Organiv/views/presentation/bloc/status/status_cubit.dart';
import 'package:Organiv/app_const.dart';

class StatusTextPage extends StatefulWidget {
  const StatusTextPage({Key key}) : super(key: key);
  @override
  _StatusTextPageState createState() => _StatusTextPageState();
}

class _StatusTextPageState extends State<StatusTextPage> {
  get _userEntity => Global.user;
  int index = 0;
  double height = 40;
  final TextEditingController _controller = TextEditingController();
  final List<Color> colors = [
    Colors.red,
    Colors.black,
    Colors.green[300],
    Colors.blue[900],
    Colors.grey[500],
    Colors.cyan[100],
    Colors.yellow,
    Colors.pink[300],
    Colors.purple[200],
    Colors.orange[300],
    Colors.grey[800],
    Colors.white,
  ];

  @override
  void initState() {
    index = new Random().nextInt(colors.length - 1);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _sendStatus(),
        child: Icon(
          Icons.send,
          color: Colors.white,
        ),
      ),
    );
  }

  Widget _body() {
    Size size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.symmetric(vertical: 60, horizontal: 10),
      color: colors[index],
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              flex: 5,
              child: new ConstrainedBox(
                constraints: new BoxConstraints(
                  minWidth: size.width,
                  maxWidth: size.width,
                  minHeight: 25.0,
                  maxHeight: size.height - 10,
                ),
                child: new TextField(
                  cursorColor: Global.backgroundHeartColor,
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  autofocus: true,
                  onChanged: (value) => setState(() {}),
                  controller: _controller,
                  textAlign: _controller.text.isEmpty
                      ? TextAlign.left
                      : TextAlign.center,
                  style: TextStyle(
                      fontSize: height,
                      color: colors[index] == Colors.white
                          ? Colors.black
                          : Colors.white),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(
                        top: 2.0, left: 13.0, right: 13.0, bottom: 2.0),
                    hintText: Variable.labelStatus,
                    hintStyle: TextStyle(
                      color: Colors.white70,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _sendStatus() async {
    bool hasEmpty =
        _controller.text.replaceAll("\n", "").replaceAll(" ", "").isNotEmpty;
    if (_controller.text.isNotEmpty && hasEmpty) {
      List<String> uids = [];
      Global.contacts.forEach((contact) => uids.add(contact.uid));
      await BlocProvider.of<StatusCubit>(context)
          .sendStatus(
            color: colors[index].toString(),
            legende: null,
            link: null,
            message: _controller.text,
            senderName: _userEntity.name,
            statusType: AppConst.text,
            time: Timestamp.now(),
            uid: _userEntity.uid,
            size: height.floor(),
            uids: uids,
          )
          .then((value) => Navigator.pop(context));
    }
  }
}
