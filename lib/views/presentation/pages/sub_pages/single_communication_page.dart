import 'dart:async';
import 'package:Organiv/app_const.dart';
import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/config/variable/language/fr.dart';
import 'package:Organiv/views/domain/entities/text_message_entity.dart';
import 'package:Organiv/views/domain/entities/user_entity.dart';
import 'package:Organiv/views/presentation/bloc/communication/communication_cubit.dart';
import 'package:Organiv/views/presentation/widgets/style.dart';
import 'package:Organiv/views/presentation/bloc/call/call_cubit.dart';
import 'package:Organiv/views/presentation/bloc/user/user_cubit.dart';
import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:readmore/readmore.dart';
import 'package:intl/intl.dart';

class SingleCommunicationPage extends StatefulWidget {
  final String senderUID;
  final String recipientUID;
  final String senderName;
  final String recipientName;
  final String recipientPhoneNumber;
  final String senderPhoneNumber;
  final String senderProfil;
  final String recipientProfil;
  final String lastView;
  final bool online;

  const SingleCommunicationPage({
    Key key,
    this.senderUID,
    this.recipientUID,
    this.senderName,
    this.recipientName,
    this.recipientPhoneNumber,
    this.senderPhoneNumber,
    this.lastView,
    this.online,
    this.senderProfil,
    this.recipientProfil,
  }) : super(key: key);

  @override
  _SingleCommunicationPageState createState() =>
      _SingleCommunicationPageState();
}

class _SingleCommunicationPageState extends State<SingleCommunicationPage> {
  TextEditingController _textMessageController = new TextEditingController();
  ScrollController _scrollController = new ScrollController();
  bool canSend = false;

  @override
  void initState() {
    BlocProvider.of<CommunicationCubit>(context).getMessages(
        senderId: widget.senderUID, recipientId: widget.recipientUID);
    _textMessageController.addListener(() {
      setState(() {
        if (_textMessageController.text.isEmpty ||
            _textMessageController.text == '')
          canSend = false;
        else
          canSend = true;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _textMessageController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Row(
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(50)),
              ),
              child: RawMaterialButton(
                onPressed: () => Navigator.pop(context),
                child: Container(
                  padding: EdgeInsets.only(top: 2, bottom: 2),
                  child: Row(
                    children: [
                      Icon(
                        Icons.arrow_back,
                        color: Colors.white,
                        size: 22,
                      ),
                      SizedBox(width: 5),
                      Container(
                        height: 1.5 * Global.interParagraphSpace,
                        width: 1.5 * Global.interParagraphSpace,
                        child: Image.asset(
                            widget.recipientProfil ?? Global.linkProfil),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.recipientName,
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 18,
                    color: Colors.white,
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  widget.online
                      ? "${Variable.online}"
                      : "${Variable.lastView} ${widget.lastView}",
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(color: Colors.white, fontSize: 12),
                )
              ],
            )
          ],
        ),
        actions: [
          GestureDetector(
            onTap: () => _call(type: AppConst.call_video),
            child: Icon(Icons.videocam, color: Colors.white),
          ),
          SizedBox(width: Global.interParagraphSpace / 2),
          GestureDetector(
            onTap: () => _call(type: AppConst.call_audio),
            child: Icon(Icons.call, color: Colors.white),
          ),
          //Icon(Icons.call, color: Colors.white),
          SizedBox(width: Global.interParagraphSpace / 3),
          Icon(Icons.more_vert, color: Colors.white),
          SizedBox(width: Global.interParagraphSpace / 2),
        ],
      ),
      body: BlocBuilder<CommunicationCubit, CommunicationState>(
        builder: (_, communicationState) {
          if (communicationState is CommunicationLoaded) {
            return _bodyWidget(communicationState);
          }

          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        },
      ),
    );
  }

  Widget _bodyWidget(CommunicationLoaded communicationLoadedState) {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          child: Image.asset(
            Global.linkBackgroundMessage,
            fit: BoxFit.cover,
          ),
        ),
        Column(
          children: [
            _messagesListWidget(communicationLoadedState.messages),
            _sendMessageTextField(),
          ],
        )
      ],
    );
  }

  Widget _messagesListWidget(List<TextMessageEntity> messages) {
    Timer(Duration(milliseconds: 100), () {
      _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        duration: Duration(milliseconds: 300),
        curve: Curves.easeInQuad,
      );
    });

    return Expanded(
      child: ListView.builder(
        controller: _scrollController,
        itemCount: messages.length,
        itemBuilder: (_, index) {
          return _messageItem(
              message: messages[index],
              preview: index == 0 ? null : messages[index - 1]);
        },
      ),
    );
  }

  Widget _sendMessageTextField() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Row(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(25)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(.2),
                    offset: Offset(0.0, 0.5),
                    spreadRadius: 1,
                    blurRadius: 1,
                  )
                ],
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  SizedBox(width: 10),
                  Container(
                    margin: EdgeInsets.only(bottom: 10),
                    child: Icon(
                      Icons.insert_emoticon,
                      color: Colors.grey[500],
                    ),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: ConstrainedBox(
                      constraints: BoxConstraints(maxHeight: 150),
                      child: Scrollbar(
                        child: TextField(
                          controller: _textMessageController,
                          maxLines: Global.maxLineConversation,
                          minLines: Global.minLineConversation,
                          style: TextStyle(fontSize: 18),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: Variable.typeMessage,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 10),
                        child: Icon(Icons.attach_file),
                      ),
                      SizedBox(width: 10),
                      _textMessageController.text.isEmpty ||
                              _textMessageController.text == ""
                          ? Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(bottom: 10),
                                  child: Icon(Icons.camera_alt),
                                ),
                                SizedBox(width: 15),
                              ],
                            )
                          : Container(),
                    ],
                  )
                ],
              ),
            ),
          ),
          SizedBox(width: 5),
          InkWell(
            onTap: () {
              if (canSend) _sendTextMessage();
            },
            child: Container(
              height: 45,
              width: 45,
              decoration: BoxDecoration(
                color: Global.backgroundHeartColor,
                borderRadius: BorderRadius.all(Radius.circular(50)),
              ),
              child: canSend
                  ? Container(child: Icon(Icons.send, color: textIconColor))
                  : Icon(Icons.mic, color: textIconColor),
            ),
          )
        ],
      ),
    );
  }

  Widget _messageItem({TextMessageEntity message, TextMessageEntity preview}) {
    final String text = message.message;
    final String time =
        Global.date(timestamp: message.time, format: Global.hourFormat);
    final Color color = message.senderUID == widget.senderUID
        ? Global.backgroundSendMessage
        : Global.backgroundReceiveMessage;
    final TextAlign align = TextAlign.left;
    final CrossAxisAlignment boxAlign = message.senderUID == widget.senderUID
        ? CrossAxisAlignment.end
        : CrossAxisAlignment.start;
    final BubbleNip nip = message.senderUID == widget.senderUID
        ? BubbleNip.rightTop
        : BubbleNip.leftTop;
    final CrossAxisAlignment crossAlign = CrossAxisAlignment.start;
    final double right = message.senderUID == widget.senderUID ? 16 : 0;
    final double left = message.senderUID == widget.senderUID ? 0 : 16;
    Duration compare;
    bool hourView = false;
    if (preview != null) {
      compare = message.time.toDate().difference(preview.time.toDate());
      if (int.parse(DateFormat('d').format(message.time.toDate()).toString()) !=
          int.parse(DateFormat('d').format(preview.time.toDate()).toString()))
        hourView = true;
      else if (compare.inDays >= 1)
        hourView = true;
      else
        hourView = false;
    } else
      hourView = true;
    return Column(
      crossAxisAlignment: boxAlign,
      children: [
        hourView
            ? Center(
                child: Container(
                  margin: EdgeInsets.all(10),
                  height: Global.interParagraphSpace,
                  width: MediaQuery.of(context).size.width / 4,
                  decoration: BoxDecoration(
                    color: Colors.black.withOpacity(.15),
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                  ),
                  child: Center(
                    child: Text(
                      Global.date(
                          timestamp: message.time, format: Global.dateFormat),
                      style:
                          TextStyle(fontSize: 12, fontStyle: FontStyle.italic),
                    ),
                  ),
                ),
              )
            : Container(),
        ConstrainedBox(
          constraints: BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width * 0.9,
          ),
          child: Container(
            width: _calculLength(text.split("\n")) <= 10
                ? 6 * _calculLength(text.split("\n")) +
                    MediaQuery.of(context).textScaleFactor *
                        (MediaQuery.of(context).size.width / 3)
                : _calculLength(text.split("\n")) >= 50
                    ? MediaQuery.of(context).size.width
                    : MediaQuery.of(context).textScaleFactor *
                            10.0 *
                            (_calculLength(text.split("\n"))) +
                        (MediaQuery.of(context).textScaleFactor *
                            MediaQuery.of(context).size.width /
                            7),
            margin: EdgeInsets.only(right: right, left: left, top: 3),
            child: Bubble(
              margin: BubbleEdges.only(right: 3 * left, left: 3 * right),
              color: color,
              nip: preview != null
                  ? preview.senderUID != message.senderUID
                      ? nip
                      : hourView
                          ? nip
                          : BubbleNip.no
                  : nip,
              child: Column(
                crossAxisAlignment: crossAlign,
                children: [
                  ReadMoreText(
                    text,
                    trimLines: 15,
                    colorClickableText: Colors.pink,
                    trimMode: TrimMode.Line,
                    trimCollapsedText: '...\tVoir plus',
                    delimiter: "",
                    lessStyle:
                        TextStyle(fontSize: 0.1, color: Colors.transparent),
                    moreStyle: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: Colors.black87,
                        fontStyle: FontStyle.italic),
                    textAlign: align,
                    style: TextStyle(fontSize: 17),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        time,
                        style: TextStyle(
                          fontSize: 11,
                          color: Colors.black.withOpacity(.4),
                        ),
                      ),
                      message.senderUID == widget.senderUID
                          ? Icon(
                              !message.isSend
                                  ? Icons.access_time
                                  : !message.isReceived
                                      ? Icons.check
                                      : Icons.done_all,
                              color: message.isRead
                                  ? Colors.red
                                  : Colors.black.withOpacity(.4),
                              size: 14,
                            )
                          : Container()
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  void _sendTextMessage() {
    bool hasEmpty = _textMessageController.text
        .replaceAll("\n", "")
        .replaceAll(" ", "")
        .isNotEmpty;
    if (_textMessageController.text.isNotEmpty && hasEmpty) {
      BlocProvider.of<CommunicationCubit>(context).sendTextMessage(
        profilURL: "",
        isRead: false,
        isReceived: false,
        message: _textMessageController.text,
        senderId: widget.senderUID,
        senderName: widget.senderName,
        senderPhoneNumber: widget.senderPhoneNumber,
        recipientId: widget.recipientUID,
        recipientName: widget.recipientName,
        recipientPhoneNumber: widget.recipientPhoneNumber,
        messageType: AppConst.text,
      );
      _textMessageController.clear();
    }
  }

  int _calculLength(List<String> split) {
    if (split.length == 1) return split[0].length;
    int _length = 0;
    split.forEach((element) {
      if (element.length >= _length) _length = element.length;
    });
    return _length;
  }

  void _call({String type}) async {
    UserEntity from = UserEntity(
      internationalizedPhoneNumber: widget.recipientPhoneNumber,
      uid: widget.senderUID,
      name: widget.senderName,
      profilUrl: widget.senderProfil,
      phoneNumber: widget.senderPhoneNumber,
      nullable: false,
    );
    UserEntity to = UserEntity(
      internationalizedPhoneNumber: widget.recipientPhoneNumber,
      uid: widget.recipientUID,
      name: widget.recipientName,
      profilUrl: widget.recipientProfil,
      phoneNumber: widget.recipientPhoneNumber,
      nullable: false,
    );
    await BlocProvider.of<UserCubit>(context).createSingleCallChannel(
        uid: widget.senderUID, otherUid: widget.recipientUID);
    await BlocProvider.of<CallCubit>(context)
        .dial(from: from, to: to, callType: type)
        .then((value) => Navigator.pop(context));
  }
}
