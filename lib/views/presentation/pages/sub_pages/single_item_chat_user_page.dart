import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/views/domain/entities/my_single_chat_entity.dart';
import 'package:Organiv/views/data/model/contact_user_model.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class SingleItemChatUserPage extends StatefulWidget {
  final MySingleChatEntity mySingleChatEntity;
  final ContactUserModel contact;

  const SingleItemChatUserPage({Key key, this.mySingleChatEntity, this.contact})
      : super(key: key);

  @override
  _SingleItemChatUserPageState createState() => _SingleItemChatUserPageState();
}

class _SingleItemChatUserPageState extends State<SingleItemChatUserPage> {
  get _mySingleChatEntity => widget.mySingleChatEntity;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(top: 10, right: 20, left: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Container(
                    height: 2 * Global.interParagraphSpace - 5,
                    width: 2 * Global.interParagraphSpace - 5,
                    child: CircleAvatar(
                      backgroundImage: AssetImage(Global.linkProfil),
                    ),
                  ),
                  SizedBox(width: 10),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: new ConstrainedBox(
                          constraints: new BoxConstraints(
                            maxWidth: 2 * size.width / 3,
                          ),
                          child: Text(
                            widget.contact != null
                                ? widget.contact.label
                                : widget
                                    .mySingleChatEntity.recipientPhoneNumber,
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w500),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                      SizedBox(height: 5),
                      new ConstrainedBox(
                        constraints: new BoxConstraints(
                          minWidth: size.width / 4,
                          maxWidth: 2 * size.width / 3 - 25,
                        ),
                        child: Text(
                          _mySingleChatEntity.recentTextMessage,
                          style: TextStyle(fontStyle: FontStyle.italic),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      )
                    ],
                  )
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    DateFormat(Global.hourFormat)
                        .format(_mySingleChatEntity.time.toDate()),
                  ),
                  _mySingleChatEntity.count > 0
                      ? Column(
                          children: [
                            SizedBox(height: .2 * Global.interParagraphSpace),
                            Container(
                              width: .85 * Global.interParagraphSpace +
                                  4 *
                                      (_mySingleChatEntity.count
                                              .toString()
                                              .length -
                                          1),
                              height: .85 * Global.interParagraphSpace +
                                  4 *
                                      (_mySingleChatEntity.count
                                              .toString()
                                              .length -
                                          1),
                              decoration: BoxDecoration(
                                  color: Global.notificationColor,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(50))),
                              child: Center(
                                child: Text("${_mySingleChatEntity.count}",
                                    style: TextStyle(
                                        fontSize: 12,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w500)),
                              ),
                            ),
                          ],
                        )
                      : Container(),
                ],
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 60, right: 10),
            child: Divider(thickness: 1.5),
          )
        ],
      ),
    );
  }
}
