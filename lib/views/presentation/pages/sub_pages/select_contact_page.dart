import 'package:Organiv/app_const.dart';
import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/config/variable/language/fr.dart';
import 'package:Organiv/views/data/model/contact_user_model.dart';
import 'package:Organiv/views/presentation/bloc/call/call_cubit.dart';
import 'package:Organiv/views/presentation/bloc/contact/contact_cubit.dart';
import 'package:Organiv/views/domain/entities/user_entity.dart';
import 'package:Organiv/views/presentation/bloc/get_device_number/get_device_numbers_cubit.dart';
import 'package:Organiv/views/presentation/bloc/user/user_cubit.dart';
import 'package:Organiv/views/presentation/pages/sub_pages/single_communication_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

class SelectContactPage extends StatefulWidget {
  final String communication;

  const SelectContactPage({Key key, this.communication = "messages"})
      : super(key: key);

  @override
  _SelectContactPageState createState() => _SelectContactPageState();
}

class _SelectContactPageState extends State<SelectContactPage> {
  get _userEntity => Global.user;
  List<ContactUserModel> contacts = Global.contacts;
  @override
  void initState() {
    BlocProvider.of<GetDeviceNumbersCubit>(context).getDeviceNumbers();
    BlocProvider.of<UserCubit>(context).getAllUsers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GetDeviceNumbersCubit, GetDeviceNumbersState>(
      builder: (context, contactNumberState) {
        if (contactNumberState is GetDeviceNumbersLoaded) {
          return BlocBuilder<UserCubit, UserState>(
            builder: (context, userState) {
              if (userState is UserLoaded) {
                final dbUsers = userState.users
                    .where((user) => user.uid != _userEntity.uid);
                contactNumberState.contacts.forEach((deviceNumber) {
                  dbUsers.forEach(
                    (dbUser) {
                      if ((dbUser.phoneNumber ==
                                  deviceNumber.phoneNumber
                                      .replaceAll(" ", '') &&
                              dbUser.isoCode == _userEntity.isoCode) ||
                          dbUser.internationalizedPhoneNumber ==
                              deviceNumber.phoneNumber.replaceAll(" ", '')) {
                        for (int i = 0; i < Global.contacts.length; i++) {
                          if (Global.contacts[i].uid == dbUser.uid) {
                            Global.contacts[i].status = dbUser.status;
                            Global.contacts[i].online = dbUser.online;
                            Global.contacts[i].lastView = dbUser.lastView;
                            return;
                          }
                        }
                        //On ajoute de nouveaux contacts
                        Global.contacts.add(
                          ContactUserModel(
                            count: 0,
                            internationalizedPhoneNumber:
                                dbUser.internationalizedPhoneNumber,
                            nameUser: dbUser.name,
                            online: dbUser.online,
                            silence: dbUser.silence,
                            uid: dbUser.uid,
                            lastView: dbUser.lastView,
                            //Prendre le nom du contact
                            label: deviceNumber.label,
                            phoneNumber: dbUser.phoneNumber,
                            status: dbUser.status,
                            created_at: Timestamp.now(),
                            updated_at: Timestamp.now(),
                            nullable: true,
                          ),
                        );
                      }
                    },
                  );
                });
                //On retire de contact qui n'ont plus de compte
                Global.contacts.forEach((contact) {
                  if (dbUsers.any((user) => user.uid == contact.uid)) return;
                  Global.contacts
                      .removeWhere((element) => element.uid == contact.uid);
                });
                Global.contacts.sort((a, b) =>
                    a.label.substring(0, 1).codeUnitAt(0) -
                    b.label.substring(0, 1).codeUnitAt(0));
                //On y ajoute le lastView
                contacts = Global.contacts;
                //On met à jour la base de données
                _updateDatabase();
                return _body();
              }
              return Scaffold(
                body: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            },
          );
        }
        return _body();
      },
    );
  }

  Widget _body() {
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          onTap: () => Navigator.pop(context),
          child: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
        automaticallyImplyLeading: false,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              Variable.selectContact,
              style: TextStyle(color: Colors.white),
            ),
            Text(
              contacts.length < 2
                  ? "${contacts.length} contact"
                  : "${contacts.length} contacts",
              style: TextStyle(
                  fontSize: 14,
                  color: Colors.white,
                  fontStyle: FontStyle.italic),
            ),
          ],
        ),
        actions: [
          Icon(Icons.search),
          Icon(Icons.more_vert),
        ],
      ),
      body: _listNewConversation(),
    );
  }

  Widget _listNewConversation() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Column(
        children: [
          widget.communication == AppConst.call
              ? _newGroupButtonWidget()
              : _newCallGroupButtonWidget(),
          SizedBox(height: Global.interParagraphSpace / 2),
          _newContactButtonWidget(),
          SizedBox(height: Global.interParagraphSpace / 4),
          Divider(),
          SizedBox(height: Global.interParagraphSpace / 4),
          contacts != null ? _listContact(contacts) : Container(),
        ],
      ),
    );
  }

  Widget _listContact(List<ContactUserModel> contacts) {
    return Expanded(
      child: ListView.builder(
        itemCount: contacts.length,
        itemBuilder: (BuildContext context, index) {
          return _itemContact(contacts[index]);
        },
      ),
    );
  }

  Widget _itemContact(ContactUserModel contact) {
    return widget.communication != AppConst.call
        ? InkWell(
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => SingleCommunicationPage(
                    senderName: _userEntity.name,
                    senderUID: _userEntity.uid,
                    recipientName: contact.label,
                    recipientUID: contact.uid,
                    senderPhoneNumber: _userEntity.internationalizedPhoneNumber,
                    recipientPhoneNumber: contact.internationalizedPhoneNumber,
                    senderProfil: _userEntity.profilUrl,
                    recipientProfil: contact.profilURL,
                    online: contact.online,
                    lastView: contact.lastView != null
                        ? DateFormat(Global.hourFormat)
                            .format(contact.lastView.toDate())
                        : null,
                  ),
                ),
              );
              BlocProvider.of<UserCubit>(context).createSingleChatChannel(
                  uid: _userEntity.uid, otherUid: contact.uid);
            },
            child: _line(contact),
          )
        : _line(contact);
  }

  Widget _line(ContactUserModel contact) {
    double size = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Container(
                    height: 1.5 * Global.interParagraphSpace,
                    width: 1.5 * Global.interParagraphSpace,
                    child: CircleAvatar(
                      backgroundImage: AssetImage(Global.linkProfil),
                    ),
                  ),
                  SizedBox(width: 10),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${contact.label}",
                        style: TextStyle(fontSize: 18),
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        width: widget.communication == AppConst.call
                            ? 3 * size / 5
                            : 3 * size / 4,
                        child: Text(
                          "${contact.status}",
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontStyle: FontStyle.italic,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              widget.communication == AppConst.call
                  ? Row(
                      children: [
                        GestureDetector(
                          onTap: () async {
                            UserEntity to = UserEntity(
                              internationalizedPhoneNumber:
                                  contact.internationalizedPhoneNumber,
                              uid: contact.uid,
                              name: contact.label,
                              profilUrl: contact.profilURL,
                              phoneNumber: contact.phoneNumber,
                              nullable: false,
                              status: contact.status,
                            );

                            await BlocProvider.of<UserCubit>(context)
                                .createSingleCallChannel(
                                    uid: _userEntity.uid,
                                    otherUid: contact.uid);
                            await BlocProvider.of<CallCubit>(context)
                                .dial(
                                    from: _userEntity,
                                    to: to,
                                    callType: AppConst.call_audio)
                                .then((value) => Navigator.pop(context));
                          },
                          child: Icon(
                            Icons.call,
                            color: Global.backgroundHeartColor,
                            size: Global.interParagraphSpace,
                          ),
                        ),
                        SizedBox(width: 15),
                        GestureDetector(
                          onTap: () async {
                            UserEntity to = UserEntity(
                              internationalizedPhoneNumber:
                                  contact.internationalizedPhoneNumber,
                              uid: contact.uid,
                              name: contact.label,
                              profilUrl: contact.profilURL,
                              phoneNumber: contact.phoneNumber,
                              nullable: false,
                              status: contact.status,
                            );
                            await BlocProvider.of<UserCubit>(context)
                                .createSingleCallChannel(
                                    uid: _userEntity.uid,
                                    otherUid: contact.uid);
                            await BlocProvider.of<CallCubit>(context)
                                .dial(
                                    from: _userEntity,
                                    to: to,
                                    callType: AppConst.call_video)
                                .then((value) => Navigator.pop(context));
                          },
                          child: Icon(
                            Icons.videocam_sharp,
                            color: Global.backgroundHeartColor,
                            size: Global.interParagraphSpace,
                          ),
                        ),
                      ],
                    )
                  : Container(),
              Divider(),
            ],
          ),
        ],
      ),
    );
  }

  _updateDatabase() =>
      BlocProvider.of<ContactCubit>(context).addContacts(Global.contacts);

  Widget _newGroupButtonWidget() {
    return Container(
      child: Row(
        children: [
          Container(
            height: 1.5 * Global.interParagraphSpace,
            width: 1.5 * Global.interParagraphSpace,
            decoration: BoxDecoration(
              color: Global.backgroundHeartColor,
              borderRadius: BorderRadius.all(Radius.circular(40)),
            ),
            child: Icon(
              Icons.people,
              color: Colors.white,
              size: Global.interParagraphSpace,
            ),
          ),
          SizedBox(width: Global.interParagraphSpace / 2),
          Text(
            Variable.newGroup,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              fontStyle: FontStyle.italic,
            ),
          )
        ],
      ),
    );
  }

  Widget _newContactButtonWidget() {
    return Container(
      margin: EdgeInsets.only(right: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Container(
                height: 1.5 * Global.interParagraphSpace,
                width: 1.5 * Global.interParagraphSpace,
                decoration: BoxDecoration(
                  color: Global.backgroundHeartColor,
                  borderRadius: BorderRadius.all(Radius.circular(40)),
                ),
                child: Icon(
                  Icons.person_add,
                  color: Colors.white,
                  size: Global.interParagraphSpace,
                ),
              ),
              SizedBox(width: Global.interParagraphSpace / 2),
              Text(
                Variable.newContact,
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic,
                ),
              )
            ],
          ),
          Icon(FontAwesomeIcons.qrcode, color: Colors.grey),
        ],
      ),
    );
  }

  _newCallGroupButtonWidget() {
    return Container(
      child: Row(
        children: [
          Container(
            height: 1.5 * Global.interParagraphSpace,
            width: 1.5 * Global.interParagraphSpace,
            decoration: BoxDecoration(
              color: Global.backgroundHeartColor,
              borderRadius: BorderRadius.all(Radius.circular(40)),
            ),
            child: Icon(
              Icons.people,
              color: Colors.white,
              size: Global.interParagraphSpace,
            ),
          ),
          SizedBox(width: Global.interParagraphSpace / 2),
          Text(
            Variable.newCallGroup,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              fontStyle: FontStyle.italic,
            ),
          )
        ],
      ),
    );
  }
}
