import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/config/variable/language/fr.dart';
import 'package:Organiv/views/presentation/bloc/phone_auth/phone_auth_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class PhoneVerificationPage extends StatefulWidget {
  final String phoneNumber;

  PhoneVerificationPage({this.phoneNumber});

  @override
  _PhoneVerificationPageState createState() => _PhoneVerificationPageState();
}

class _PhoneVerificationPageState extends State<PhoneVerificationPage> {
  TextEditingController _pinCodeController = new TextEditingController();
  bool _isValidate = false;
  final int _length = 6;

  @override
  Widget build(BuildContext context) {
    Global _global = new Global(context);
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
        child: Column(
          children: [
            SizedBox(height: Global.interParagraphSpace / 2),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(Icons.arrow_back),
                ),
                Expanded(
                  child: Center(
                    child: Text(
                      Variable.signInPhone,
                      style: TextStyle(
                          fontSize: Global.titleSize,
                          color: Global.backgroundHeartColor,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: Global.interParagraphSpace),
            Text(
              Variable.commentDescSmsCode,
              textAlign: TextAlign.justify,
              style: TextStyle(fontSize: Global.commentSize),
            ),
            SizedBox(height: Global.interParagraphSpace),
            _pinCodeWidget(),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: MaterialButton(
                  color: _isValidate
                      ? Global.backgroundHeartColor
                      : Colors.grey[600],
                  onPressed: _submitSmsCode,
                  child: Text(
                    Variable.next,
                    style: _global.bodyStyle.copyWith(color: Colors.white),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _pinCodeWidget() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: Global.interParagraphSpace),
      child: Column(
        children: [
          PinCodeTextField(
            controller: _pinCodeController,
            backgroundColor: Colors.transparent,
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
            length: _length,
            obscureText: true,
            onChanged: _onChanged,
            appContext: context,
          ),
          Text(Variable.digits)
        ],
      ),
    );
  }

  void _submitSmsCode() {
    if (_pinCodeController.text.isNotEmpty) {
      BlocProvider.of<PhoneAuthCubit>(context)
          .submitSmsCode(smsCode: _pinCodeController.text);
    }
  }

  _onChanged(String pinCode) {
    bool state;
    if (pinCode.length == _length)
      state = true;
    else
      state = false;
    setState(() => _isValidate = state);
  }
}
