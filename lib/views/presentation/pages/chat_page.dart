import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/config/variable/language/fr.dart';
import 'package:Organiv/views/domain/entities/my_single_chat_entity.dart';
import 'package:Organiv/views/data/model/contact_user_model.dart';
import 'package:Organiv/views/presentation/bloc/contact/contact_cubit.dart';
import 'package:Organiv/views/presentation/bloc/my_single_chat/my_single_chat_cubit.dart';
import 'package:Organiv/views/presentation/pages/sub_pages/select_contact_page.dart';
import 'package:Organiv/views/presentation/pages/sub_pages/single_item_chat_user_page.dart';
import 'package:Organiv/views/presentation/pages/sub_pages/single_communication_page.dart';
import 'package:Organiv/views/presentation/bloc/get_device_number/get_device_numbers_cubit.dart';
import 'package:Organiv/views/presentation/bloc/user/user_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ChatPage extends StatefulWidget {
  const ChatPage({Key key}) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  get _userEntity => Global.user;

  @override
  void initState() {
    BlocProvider.of<GetDeviceNumbersCubit>(context).getDeviceNumbers();
    BlocProvider.of<UserCubit>(context).getAllUsers();
    BlocProvider.of<ContactCubit>(context).getContact();
    BlocProvider.of<MySingleChatCubit>(context)
        .getMySingleChat(uid: _userEntity.uid);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<MySingleChatCubit, MySingleChatState>(
        builder: (_, mySingleChatState) {
          if (mySingleChatState is MySingleChatLoaded) {
            if (Global.contacts.isEmpty)
              return _reloadContact(myChatData: mySingleChatState);
            else
              return _myChatList(myChatData: mySingleChatState);
          }
          return _loadingWidget();
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Global.backgroundHeartColor,
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => SelectContactPage(),
            ),
          );
        },
        child: Icon(
          Icons.chat,
          color: Global.backgroundTypeMessage,
        ),
      ),
    );
  }

  Widget _emptyListDisplayMessageWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          height: 5 * Global.interParagraphSpace,
          width: 5 * Global.interParagraphSpace,
          decoration: BoxDecoration(
            color: Global.backgroundHeartColor.withOpacity(.5),
            borderRadius: BorderRadius.all(Radius.circular(100)),
          ),
          child: Icon(Icons.message,
              color: Colors.white.withOpacity(.6), size: 50),
        ),
        Align(
          child: Padding(
            padding: const EdgeInsets.all(15),
            child: Text(
              Variable.welcomeChat,
              textAlign: TextAlign.center,
              style:
                  TextStyle(fontSize: 14, color: Colors.black.withOpacity(.4)),
            ),
          ),
        )
      ],
    );
  }

  Widget _myChatList({MySingleChatLoaded myChatData}) {
    return Column(
      children: [
        Expanded(
          child: myChatData.mySingleChat.isEmpty
              ? _emptyListDisplayMessageWidget()
              : ListView.builder(
                  itemCount: myChatData.mySingleChat.length,
                  itemBuilder: (_, index) {
                    ContactUserModel userOther;
                    MySingleChatEntity itemChat =
                        myChatData.mySingleChat[index];
                    //IF I am sender
                    Global.contacts.forEach((contact) {
                      if (contact.uid == itemChat.recipientUID)
                        userOther = contact;
                    });

                    return InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => SingleCommunicationPage(
                              senderName: itemChat.senderName,
                              senderUID: itemChat.senderUID,
                              recipientName: userOther != null
                                  ? userOther.label
                                  : itemChat.recipientPhoneNumber,
                              recipientUID: itemChat.recipientUID,
                              senderPhoneNumber: itemChat.senderPhoneNumber,
                              recipientPhoneNumber:
                                  itemChat.recipientPhoneNumber,
                              senderProfil: _userEntity.profilUrl,
                              recipientProfil: userOther.profilURL,
                              online: userOther.online,
                              lastView: DateFormat(Global.hourFormat).format(
                                  userOther != null &&
                                          userOther.milliseconds != null
                                      ? Timestamp.fromMillisecondsSinceEpoch(
                                              userOther.milliseconds)
                                          .toDate()
                                      : Timestamp.now().toDate()),
                            ),
                          ),
                        );
                      },
                      child: SingleItemChatUserPage(
                        mySingleChatEntity: itemChat,
                        contact: userOther,
                      ),
                    );
                  },
                ),
        ),
      ],
    );
  }

  Widget _loadingWidget() {
    return Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget _reloadContact({MySingleChatLoaded myChatData}) {
    return BlocBuilder<GetDeviceNumbersCubit, GetDeviceNumbersState>(
      builder: (context, contactNumberState) {
        if (contactNumberState is GetDeviceNumbersLoaded) {
          return BlocBuilder<UserCubit, UserState>(
            builder: (context, userState) {
              if (userState is UserLoaded) {
                final dbUsers = userState.users
                    .where((user) => user.uid != _userEntity.uid);
                contactNumberState.contacts.forEach((deviceNumber) {
                  dbUsers.forEach(
                    (dbUser) {
                      if (dbUser.phoneNumber ==
                              deviceNumber.phoneNumber.replaceAll(" ", '') ||
                          dbUser.internationalizedPhoneNumber ==
                              deviceNumber.phoneNumber.replaceAll(" ", '')) {
                        //On ajoute de nouveaux contacts
                        if (Global.contacts
                            .any((element) => element.uid == dbUser.uid))
                          return;
                        Global.contacts.add(
                          ContactUserModel(
                            count: 0,
                            internationalizedPhoneNumber:
                                dbUser.internationalizedPhoneNumber,
                            nameUser: dbUser.name,
                            online: dbUser.online,
                            silence: dbUser.silence,
                            uid: dbUser.uid,
                            profilURL: dbUser.profilUrl,
                            lastView: dbUser.lastView,
                            //Prendre le nom du contact
                            label: deviceNumber.label,
                            phoneNumber: dbUser.phoneNumber,
                            status: dbUser.status,
                            nullable: true,
                          ),
                        );
                      }
                    },
                  );
                });
                Global.contacts.sort((a, b) =>
                    a.label.substring(0, 1).codeUnitAt(0) -
                    b.label.substring(0, 1).codeUnitAt(0));
                //On met à jour la base de données
                _updateDatabase();
                return _myChatList(myChatData: myChatData);
              }
              return Scaffold(
                body: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            },
          );
        }
        return _myChatList(myChatData: myChatData);
      },
    );
  }

  _updateDatabase() =>
      BlocProvider.of<ContactCubit>(context).addContacts(Global.contacts);
}
