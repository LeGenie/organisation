import 'dart:io';

import 'package:Organiv/config/variable/global.dart';
import 'package:camera/camera.dart';
import 'package:custom_image_picker/custom_image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:torch_compat/torch_compat.dart';
import 'package:flutter/material.dart';

class CameraPage extends StatefulWidget {
  @override
  _CameraPageState createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {
  List<CameraDescription> cameras;
  CameraController _cameraController;
  List<dynamic> _galleryPhotos;
  bool flash = false;
  String path;
  final List<ResolutionPreset> _resolution = [
    ResolutionPreset.low,
    ResolutionPreset.medium,
    ResolutionPreset.high,
    ResolutionPreset.veryHigh,
    ResolutionPreset.ultraHigh
  ];
  int _indexResolution = 2;
  int _camera = 0;

  @override
  void initState() {
    initializeCamera();
    getImagesFromGallery();
    super.initState();
  }

  @override
  void dispose() {
    _cameraController.dispose();
    super.dispose();
  }

  //Camera show
  Future<void> initializeCamera() async {
    cameras = await availableCameras();
    if (cameras.length == 0) return;
    _cameraController =
        CameraController(cameras[_camera], _resolution[_indexResolution]);
    _mounted();
  }

  Future<void> _mounted() async {
    _cameraController.initialize().then((value) {
      if (!mounted) return;
      setState(() {});
    });
  }

  Future<void> _switchCamera() async {
    if (cameras.length > 1) {
      _camera = (_camera + 1) % 2;
      _cameraController =
          CameraController(cameras[_camera], _resolution[_indexResolution]);
    }
    _mounted();
  }

  _flash() {
    flash = !flash;
    if (flash)
      TorchCompat.turnOn();
    else
      TorchCompat.turnOff();
  }

  Future<void> _changeResolution() async {
    if (_indexResolution == _resolution.length - 1)
      _indexResolution = 0;
    else
      _indexResolution++;
    _cameraController =
        CameraController(cameras[_camera], _resolution[_indexResolution]);
    _mounted();
  }

  Future<void> getImagesFromGallery() async {
    CustomImagePicker().getAllImages(callback: (photos) {
      setState(() {
        _galleryPhotos = photos.reversed.toList();
      });
    });
  }

  Future<void> _takePicture() async {
    Directory tempDir = await getTemporaryDirectory();
    path = tempDir.path;

    // Attempt to take a picture and get the file `image`
    // where it was saved.
    await _cameraController.takePicture(path);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    if (_cameraController == null || !_cameraController.value.isInitialized) {
      return Container(
        height: 0.0,
        width: 0.0,
      );
    }
    return Scaffold(
        body: Stack(
      children: [
        Container(
          height: double.infinity,
          width: double.infinity,
          child: path != null
              ? Image.file(File(path))
              : CameraPreview(_cameraController),
        ),
        _cameraButtonWidget(),
        _galleryWidget(),
      ],
    ));
  }

  Widget _cameraButtonWidget() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        margin: EdgeInsets.only(bottom: 10),
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            /*Icon(Icons.flash_on,
                color: Colors.white, size: Global.interParagraphSpace),*/
            RawMaterialButton(
              onPressed: () => _flash(),
              child: Icon(
                Icons.flash_on,
                color: Colors.white,
                size: Global.interParagraphSpace,
              ),
              shape: CircleBorder(),
              fillColor: Colors.transparent,
              padding: const EdgeInsets.all(12.0),
            ),
            GestureDetector(
              onTap: () async => await _takePicture(),
              child: Container(
                height: 80,
                width: 80,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  border: Border.all(color: Global.backgroundTypeMessage),
                ),
              ),
            ),
            RawMaterialButton(
              onPressed: () => _switchCamera(),
              child: Icon(
                Icons.camera_alt,
                color: Colors.white,
                size: Global.interParagraphSpace,
              ),
              shape: CircleBorder(),
              fillColor: Colors.transparent,
              padding: const EdgeInsets.all(12.0),
            ),
            /*RawMaterialButton(
                            onPressed: () => _changeResolution(),
                            child: Icon(
                              Icons.camera,
                              color: Colors.cyan,
                              size: Global.interParagraphSpace / 2,
                            ),
                            shape: CircleBorder(),
                            fillColor: Colors.black.withOpacity(.6),
                            padding: const EdgeInsets.all(12.0),
                          ),*/
          ],
        ),
      ),
    );
  }

  Widget _galleryWidget() {
    if (_galleryPhotos == null || _galleryPhotos.length == 0) {
      return Container(
        height: 0.0,
        width: 0.0,
      );
    }
    return Positioned(
      bottom: 100,
      right: 0,
      left: 0,
      child: Container(
        height: 2 * Global.interParagraphSpace,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount:
              _galleryPhotos.length /*> 15 ? 15 : _galleryPhotos.length*/,
          itemBuilder: (_, index) {
            return Container(
              margin: EdgeInsets.only(right: 8.0),
              height: 3 * Global.interParagraphSpace,
              width: 3 * Global.interParagraphSpace,
              decoration: BoxDecoration(color: Colors.red.withOpacity(.2)),
              child: Image.file(
                File(_galleryPhotos[index]),
                fit: BoxFit.cover,
              ),
            );
          },
        ),
      ),
    );
  }
}
