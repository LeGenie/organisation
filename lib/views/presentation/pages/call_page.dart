import 'package:Organiv/app_const.dart';
import 'package:Organiv/config/variable/global.dart';
import 'package:Organiv/views/domain/entities/call_entity.dart';
import 'package:Organiv/views/presentation/bloc/my_single_call/my_single_call_cubit.dart';
import 'package:Organiv/views/presentation/bloc/user/user_cubit.dart';
import 'package:Organiv/views/presentation/pages/sub_pages/select_contact_page.dart';
import 'package:Organiv/views/presentation/pages/sub_pages/single_item_call_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CallPage extends StatefulWidget {
  const CallPage({Key key}) : super(key: key);

  @override
  _CallPageState createState() => _CallPageState();
}

class _CallPageState extends State<CallPage> {
  get _userEntity => Global.user;

  CallEntity call;
  String callId;

  @override
  void initState() {
    BlocProvider.of<UserCubit>(context).getAllUsers();
    BlocProvider.of<MySingleCallCubit>(context)
        .getMySingleCall(uid: _userEntity.uid);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<MySingleCallCubit, MySingleCallState>(
        builder: (_, mySingleCallState) {
          if (mySingleCallState is MySingleCallLoaded) {
            return _body();
          }
          return _loadingWidget();
        },
      ),
      floatingActionButton: call == null
          ? FloatingActionButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        SelectContactPage(communication: AppConst.call),
                  ),
                );
              },
              backgroundColor: Global.backgroundHeartColor,
              child: Icon(
                Icons.add_call,
                color: Global.backgroundTypeMessage,
              ),
            )
          : null,
    );
  }

  Widget _loadingWidget() {
    return Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  _body() {
    return Column(
      children: [_myCallList()],
    );
  }

  Widget _myCallList() {
    return Expanded(
      child: ListView.builder(
        itemCount: 20,
        itemBuilder: (BuildContext context, int index) {
          return SingleItemCallPage();
        },
      ),
    );
  }
}
