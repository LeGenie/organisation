import 'package:Organiv/views/domain/entities/call_entity.dart';
import 'package:Organiv/views/domain/entities/my_single_call_entity.dart';
import 'package:Organiv/views/domain/entities/my_single_chat_entity.dart';
import 'package:Organiv/views/domain/entities/my_single_status_entity.dart';
import 'package:Organiv/views/domain/entities/status_entity.dart';
import 'package:Organiv/views/domain/entities/text_message_entity.dart';
import 'package:Organiv/views/domain/entities/user_entity.dart';
import 'package:Organiv/views/domain/entities/video_entity.dart';
import 'package:Organiv/views/domain/entities/audio_entity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

abstract class FirebaseRepository {
  //Auth functions
  Future<void> verfiyPhoneNumber(String phoneNumber);
  Future<void> signInWithPhoneNumber(String smspinCode);
  Future<bool> isSignIn();
  Future<void> singOut();
  Future<String> getCurrentUID();
  Future<void> getCreateCurrentUser(UserEntity user);

  //Update User
  Future<void> updateUser(UserEntity user);
  Stream<UserEntity> getUser();

  //Lecture d'entité
  Stream<List<UserEntity>> getAllUsers();
  Stream<List<TextMessageEntity>> getTextMessages(String channelId);
  Stream<List<MySingleChatEntity>> getMySingleChat(String uid);
  Stream<List<MySingleStatusEntity>> getOtherSingleStatus(String uid);
  Stream<List<StatusEntity>> getStatus(String uid, String readUid);
  Stream<List<MySingleCallEntity>> getMySingleCall(String uid);
  /*Stream<List<CallEntity>> getCalls(String channelId);
  Stream<List<AudioEntity>> getAudioCalls(String channelId);
  Stream<List<VideoEntity>> getVideoCalls(String channelId);*/
  Stream<DocumentSnapshot> getAudioCalls(String channelId, String callId);
  Stream<DocumentSnapshot> getVideoCalls(String channelId, String callId);

  //Canal de communication
  Future<void> createOneToOneChatChannel(String uid, String otherUid);
  Future<void> createStatusChannel(String uid);
  Future<void> createOneToOneCallChannel(String uid, String otherUid);
  Future<String> getOneToOneSingleUserChannelId(String uid, String otherUid);
  Future<void> addToMySingleChat(MySingleChatEntity mySingleChatEntity);
  Future<void> addToMySingleStatus(MySingleStatusEntity mySingleStatusEntity);
  Future<void> addToMySingleCall(MySingleCallEntity mySingleCallEntity);

  //Message
  Future<void> sendTextMessage(
      TextMessageEntity textMessageEntity, String channelId,
      {bool update = false});

  //Status
  Future<String> getStatusUserChannelId(String uid, String otherUid);
  Future<void> addStatus(StatusEntity statusEntity, String uid,
      {bool update = false});

  //Call
  Future<String> getOneToOneCallUserChannelId(
      String uid, String otherUid, String id);
  Future<String> callVideo(VideoEntity videoEntity, String channelId);
  Future<String> callAudio(AudioEntity audioEntity, String channelId);
  Future<void> endOneToOneCall(CallEntity call, String channelId);
}
