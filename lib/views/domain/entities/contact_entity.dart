import 'package:cloud_firestore/cloud_firestore.dart';

class ContactEntity {
  final String phoneNumber;
  final String label;
  final String uid;
  final String status;
  final Timestamp lastView;

  ContactEntity({
    this.phoneNumber,
    this.label,
    this.uid,
    this.status,
    this.lastView,
  });
}
