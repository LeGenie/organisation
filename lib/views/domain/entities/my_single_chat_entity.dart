import 'package:Organiv/views/domain/entities/state_page_entity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MySingleChatEntity extends StatePageEntity {
  final String senderName;
  final String senderUID;
  final String recipientName;
  final String recipientUID;
  final String channelId;
  final String profilURL;
  final String recipientPhoneNumber;
  final String senderPhoneNumber;
  final String recentTextMessage;
  final bool isRead;
  final bool isArchived;
  final Timestamp time;
  final Timestamp created_at;
  final int count;
  final Timestamp lastView;
  final bool silence;
  final bool online;
  //Au cas où on fait une mise à jour du message
  //Si on retourne l'exhaustivité ou pas
  bool nullable;

  MySingleChatEntity({
    this.count,
    this.lastView,
    this.silence,
    this.online,
    this.senderName,
    this.senderUID,
    this.recipientName,
    this.recipientUID,
    this.channelId,
    this.profilURL,
    this.recipientPhoneNumber,
    this.senderPhoneNumber,
    this.recentTextMessage,
    this.isRead,
    this.isArchived,
    this.time,
    this.created_at,
    this.nullable,
  }) : super(
          lastView: lastView,
          count: count,
          silence: silence,
          online: online,
          created_at: created_at,
        );

  @override
  // TODO: implement props
  List<Object> get props => [
        count,
        lastView,
        silence,
        online,
        senderName,
        senderUID,
        recipientName,
        recipientUID,
        channelId,
        profilURL,
        recipientPhoneNumber,
        senderPhoneNumber,
        recentTextMessage,
        isRead,
        isArchived,
        created_at,
        updated_at,
        time
      ];

  @override
  Future<void> notification() {
    // TODO: implement notification
    throw UnimplementedError();
  }
}
