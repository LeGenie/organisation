import 'package:Organiv/views/domain/entities/state_page_entity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MySingleStatusEntity extends StatePageEntity {
  final String senderUID;
  final String receiveUID;
  final String channelId;
  final String profilURL;
  final bool isRead;
  final Timestamp time;
  //Number of reading
  final int count;
  final Timestamp lastView;
  final bool silence;
  final bool online;
  //Au cas où on fait une mise à jour du message
  // ignore: non_constant_identifier_names
  final Timestamp created_at;
  final Timestamp updated_at = new Timestamp.now();
  //Si on retourne l'exhaustivité ou pas
  final bool nullable;

  MySingleStatusEntity(
      {this.count,
      this.lastView,
      this.silence,
      this.online,
      this.senderUID,
      this.receiveUID,
      this.channelId,
      this.profilURL,
      this.isRead,
      this.created_at,
      this.time,
      this.nullable})
      : super(
          lastView: lastView,
          count: count,
          silence: silence,
          created_at: created_at,
          online: online,
        );

  @override
  List<Object> get props => [
        count,
        lastView,
        silence,
        online,
        senderUID,
        receiveUID,
        channelId,
        profilURL,
        isRead,
        time,
        created_at,
        updated_at,
        nullable,
      ];

  @override
  Future<void> notification() {
    // TODO: implement notification
    throw UnimplementedError();
  }
}
