import 'package:Organiv/config/variable/language/fr.dart';
import 'package:Organiv/views/domain/entities/state_page_entity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class UserEntity extends StatePageEntity {
  final String name;
  final String phoneNumber;
  final String uid;
  String status;
  final String profilUrl;
  final String isoCode;
  final String internationalizedPhoneNumber;
  final String macAdress;
  final String markAdress;
  final String ipAdress;
  final int count;
  final int countMessage;
  final int countStatus;
  final int countCall;
  final int countTweet;
  final int countDisc;
  final Timestamp lastView;
  final bool silence;
  final bool online;
  final bool hasCall;
  final bool hasDialled;
  final bool stream;
  final String callId;
  // ignore: non_constant_identifier_names
  final Timestamp created_at;
  //Si on retourne l'exhaustivité ou pas
  bool nullable;

  UserEntity({
    this.count,
    this.countMessage,
    this.countStatus,
    this.countCall,
    this.countTweet,
    this.countDisc,
    this.silence,
    this.online,
    this.name,
    this.phoneNumber,
    this.uid,
    this.hasCall,
    this.hasDialled,
    this.callId,
    status,
    this.profilUrl,
    this.lastView,
    this.created_at,
    this.isoCode,
    this.stream,
    this.internationalizedPhoneNumber,
    this.macAdress,
    this.markAdress,
    this.ipAdress,
    this.nullable,
  }) : super(
          lastView: lastView,
          count: count,
          online: online,
          silence: silence,
          created_at: created_at,
        ) {
    this.status = status ?? Variable.statusLabel;
  }

  @override
  // TODO: implement props
  List<Object> get props => [
        count,
        countMessage,
        countStatus,
        countCall,
        countTweet,
        countDisc,
        silence,
        online,
        name,
        phoneNumber,
        uid,
        status,
        profilUrl,
        lastView,
        isoCode,
        hasCall,
        hasDialled,
        stream,
        callId,
        internationalizedPhoneNumber,
        macAdress,
        markAdress,
        ipAdress,
        created_at,
        updated_at,
      ];

  @override
  Future<void> notification() {
    // TODO: implement notification
    throw UnimplementedError();
  }
}
