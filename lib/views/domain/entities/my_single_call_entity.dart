import 'package:Organiv/views/domain/entities/state_page_entity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

// ignore: must_be_immutable
class MySingleCallEntity extends StatePageEntity {
  final String callerName;
  final String callerId;
  final String callerPic;
  final String recipientName;
  final String recipientId;
  final String receiverPic;
  final String channelId;
  final String pipeId;
  final bool isRead;
  final bool isArchived;
  final bool hasDialled;
  final Timestamp time;
  final int count;
  final Timestamp lastView;
  final bool silence;
  final bool online;
  final String type;

  //Au cas où on fait une mise à jour du message
  // ignore: non_constant_identifier_names
  final Timestamp created_at;
  //Si on retourne l'exhaustivité ou pas
  bool nullable;
  MySingleCallEntity({
    this.type,
    this.callerName,
    this.callerId,
    this.recipientName,
    this.recipientId,
    this.channelId,
    this.pipeId,
    this.callerPic,
    this.receiverPic,
    this.hasDialled,
    this.isRead,
    this.isArchived,
    this.time,
    this.count,
    this.lastView,
    this.created_at,
    this.silence,
    this.nullable,
    this.online,
  }) : super(
          lastView: lastView,
          count: count,
          silence: silence,
          online: online,
          created_at: created_at,
        );

  @override
  List<Object> get props => [
        type,
        callerName,
        callerId,
        recipientName,
        recipientId,
        channelId,
        pipeId,
        callerPic,
        receiverPic,
        hasDialled,
        isRead,
        isArchived,
        time,
        count,
        lastView,
        silence,
        online,
        nullable,
        updated_at,
      ];
  @override
  Future<void> notification() {
    throw UnimplementedError();
  }
}
