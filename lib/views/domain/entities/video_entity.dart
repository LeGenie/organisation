import 'package:Organiv/app_const.dart';
import 'package:Organiv/views/domain/entities/call_entity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class VideoEntity extends CallEntity {
  VideoEntity({
    String channelId,
    String callerId,
    String callerName,
    String callerPic,
    String receiverId,
    String receiverName,
    String receiverPic,
    bool hasDialled,
    int count,
    Timestamp lastView,
    bool silence,
    bool online,
    bool nullable,
  }) : super(
          channelId: channelId,
          callerId: callerId,
          callerName: callerName,
          callerPic: callerPic,
          receiverId: receiverId,
          receiverName: receiverName,
          receiverPic: receiverPic,
          hasDialled: hasDialled,
          count: count,
          lastView: lastView,
          online: online,
          silence: silence,
          nullable: nullable,
          type: AppConst.call_video,
        );

  @override
  // TODO: implement props
  List<Object> get props => [
        channelId,
        hasDialled,
        callerId,
        callerName,
        callerPic,
        receiverId,
        receiverName,
        receiverPic,
        count,
        lastView,
        online,
        type,
        silence
      ];
}
