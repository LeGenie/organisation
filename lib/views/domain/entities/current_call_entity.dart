import 'package:equatable/equatable.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class CurrentCallEntity extends Equatable {
  final String callerId;
  final String callerName;
  final Timestamp time;
  final Timestamp created_at;
  final Timestamp updated_at;
  final String profilURL;
  final String channelId;
  final String pipeId;

  CurrentCallEntity({
    this.callerId,
    this.callerName,
    this.time,
    this.updated_at,
    this.created_at,
    this.profilURL,
    this.channelId,
    this.pipeId,
  });

  @override
  // TODO: implement props
  List<Object> get props => [
        callerId,
        callerName,
        time,
        created_at,
        updated_at,
        profilURL,
        channelId,
        pipeId,
      ];
}
