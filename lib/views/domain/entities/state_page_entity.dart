import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

abstract class StatePageEntity extends Equatable {
  final int count;
  final Timestamp lastView;
  final Timestamp created_at;
  final Timestamp updated_at = new Timestamp.now();
  final bool silence;
  final bool online;

  StatePageEntity({
    this.created_at,
    this.count,
    this.lastView,
    this.silence,
    this.online,
  });

  Future<void> notification();
}
