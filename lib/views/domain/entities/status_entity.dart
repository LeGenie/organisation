import 'package:Organiv/views/domain/entities/state_page_entity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class StatusEntity extends StatePageEntity {
  final String statusId;
  final Timestamp time;
  final Timestamp created_at;
  final Timestamp updated_at;
  final String statusType;
  final String legende;
  final String message;
  final String link;
  final String color;
  final int size;
  final List<String> uids;
  final bool nullable;

  StatusEntity({
    this.statusId,
    this.time,
    this.statusType,
    this.legende,
    this.message,
    this.link,
    this.color,
    this.size,
    this.uids,
    this.nullable,
    this.created_at,
    this.updated_at,
  });

  @override
  Future<void> notification() {
    // TODO: implement notification
    throw UnimplementedError();
  }

  @override
  // TODO: implement props
  List<Object> get props => [
        statusId,
        time,
        created_at,
        updated_at,
        statusType,
        legende,
        message,
        link,
        color,
        size,
        uids,
        nullable,
      ];
}
