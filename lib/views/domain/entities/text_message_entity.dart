import 'package:Organiv/views/domain/entities/state_page_entity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class TextMessageEntity extends StatePageEntity {
  final String senderName;
  final String senderUID;
  final String recipientName;
  final String recipientUID;
  final String messageType;
  final String message;
  final String messageId;
  final Timestamp time;
  final bool isReceived;
  final bool isRead;
  final bool isSend;
  final bool isDeleted;
  final bool nullable;
  final int count;
  final Timestamp lastView;
  final Timestamp created_at;
  final bool silence;
  final bool online;

  TextMessageEntity({
    this.count,
    this.lastView,
    this.silence,
    this.online,
    this.senderName,
    this.senderUID,
    this.recipientName,
    this.recipientUID,
    this.messageType,
    this.message,
    this.messageId,
    this.time,
    this.isReceived,
    this.isRead,
    this.isSend,
    this.isDeleted,
    this.nullable,
    this.created_at,
  }) : super(
          lastView: lastView,
          count: count,
          online: online,
          silence: silence,
          created_at: created_at,
        );

  @override
  // TODO: implement props
  List<Object> get props => [
        count,
        lastView,
        silence,
        online,
        senderName,
        senderUID,
        recipientName,
        recipientUID,
        messageType,
        message,
        messageId,
        time,
        isReceived,
        created_at,
        updated_at,
        isRead,
        isSend,
        isDeleted
      ];

  @override
  Future<void> notification() {
    // TODO: implement notification
    throw UnimplementedError();
  }
}
