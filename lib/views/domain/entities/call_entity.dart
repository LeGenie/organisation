import 'package:Organiv/views/domain/entities/state_page_entity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class CallEntity extends StatePageEntity {
  final String channelId;
  final String callerId;
  final String callerName;
  final String callerPic;
  final String receiverId;
  final String receiverName;
  final String receiverPic;
  bool hasDialled;
  final int count;
  final Timestamp lastView;
  final bool silence;
  final bool online;
  final Timestamp time;
  final Timestamp created_at;
  bool nullable;
  final String type;

  CallEntity({
    this.channelId,
    this.hasDialled,
    this.callerId,
    this.callerName,
    this.callerPic,
    this.receiverId,
    this.receiverName,
    this.receiverPic,
    this.time,
    this.count,
    this.lastView,
    this.created_at,
    this.online,
    this.silence,
    this.type,
    this.nullable,
  }) : super(
          count: count,
          lastView: lastView,
          online: online,
          silence: silence,
          created_at: created_at,
        );

  @override
  Future<void> notification() {
    // TODO: implement notification
    throw UnimplementedError();
  }

  @override
  // TODO: implement props
  List<Object> get props => [
        channelId,
        hasDialled,
        callerId,
        callerName,
        callerPic,
        receiverId,
        receiverName,
        receiverPic,
        count,
        lastView,
        created_at,
        updated_at,
        online,
        type,
        silence
      ];
}
