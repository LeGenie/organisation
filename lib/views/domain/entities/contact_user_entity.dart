import 'package:Organiv/views/domain/entities/state_page_entity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ContactUserEntity extends StatePageEntity {
  final int id;
  final String internationalizedPhoneNumber;
  final String phoneNumber;
  final String nameUser;
  final String label;
  final String uid;
  String status;
  final String profilURL;
  Timestamp lastView;
  Timestamp created_at;
  final int count;
  final bool silence;
  bool online;

  bool nullable;

  ContactUserEntity({
    this.id,
    this.phoneNumber,
    this.nameUser,
    this.internationalizedPhoneNumber,
    this.label,
    this.uid,
    this.status,
    this.lastView,
    this.profilURL,
    this.created_at,
    this.count,
    this.silence,
    this.online,
  }) : super(
          lastView: lastView,
          count: count,
          online: online,
          silence: silence,
          created_at: created_at,
        );

  @override
  Future<void> notification() {
    // TODO: implement notification
    throw UnimplementedError();
  }

  @override
  // TODO: implement props
  List<Object> get props => [
        id,
        phoneNumber,
        nameUser,
        internationalizedPhoneNumber,
        label,
        uid,
        profilURL,
        status,
        lastView,
        created_at,
        updated_at,
        count,
        silence,
        online,
      ];
}
