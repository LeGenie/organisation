import 'package:Organiv/views/domain/entities/text_message_entity.dart';
import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class SendTextMessageUseCase {
  final FirebaseRepository repository;

  SendTextMessageUseCase({this.repository});

  Future<void> call(TextMessageEntity textMessageEntity, String channelId,
      {bool update = false}) async {
    return await repository.sendTextMessage(textMessageEntity, channelId,
        update: update);
  }
}
