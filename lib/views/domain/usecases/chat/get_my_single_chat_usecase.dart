import 'package:Organiv/views/domain/entities/my_single_chat_entity.dart';
import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class GetMySingleChatUseCase {
  final FirebaseRepository repository;

  GetMySingleChatUseCase({this.repository});

  Stream<List<MySingleChatEntity>> call({String uid}) {
    return repository.getMySingleChat(uid);
  }
}
