import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class GetOneToOneUserChannelUseCase {
  final FirebaseRepository repository;

  GetOneToOneUserChannelUseCase({this.repository});

  Future<String> call({String uid, String otherUid}) async {
    return await repository.getOneToOneSingleUserChannelId(uid, otherUid);
  }
}
