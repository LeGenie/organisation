import 'package:Organiv/views/domain/entities/my_single_chat_entity.dart';
import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class AddToMySingleChat {
  final FirebaseRepository repository;

  AddToMySingleChat({this.repository});

  Future<void> call(MySingleChatEntity mySingleChatEntity) async {
    return await repository.addToMySingleChat(mySingleChatEntity);
  }
}
