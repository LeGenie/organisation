import 'package:Organiv/views/domain/entities/text_message_entity.dart';
import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class GetTextMessageUseCase {
  final FirebaseRepository repository;

  GetTextMessageUseCase({this.repository});

  Stream<List<TextMessageEntity>> call(String channelId) {
    return repository.getTextMessages(channelId);
  }
}
