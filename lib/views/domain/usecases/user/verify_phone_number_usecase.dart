import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class VerfiyPhoneNumberUserCase {
  final FirebaseRepository repository;

  VerfiyPhoneNumberUserCase({this.repository});

  Future<void> call(String phoneNumber) async {
    return await repository.verfiyPhoneNumber(phoneNumber);
  }
}
