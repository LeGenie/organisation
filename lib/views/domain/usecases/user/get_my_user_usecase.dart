import 'package:Organiv/views/domain/entities/user_entity.dart';
import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class GetMyUserUseCase {
  final FirebaseRepository repository;

  GetMyUserUseCase({this.repository});

  Stream<UserEntity> call() => repository.getUser();
}
