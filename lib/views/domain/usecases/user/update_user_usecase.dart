import 'package:Organiv/views/domain/entities/user_entity.dart';
import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class UpdateUserUseCase {
  final FirebaseRepository repository;

  UpdateUserUseCase({this.repository});

  Future<void> call({UserEntity user}) {
    return repository.updateUser(user);
  }
}
