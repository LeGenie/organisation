import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class GetStatusUserChannelUseCase {
  final FirebaseRepository repository;

  GetStatusUserChannelUseCase({this.repository});

  Future<String> call({String uid, String otherUid}) async {
    return await repository.getStatusUserChannelId(uid, otherUid);
  }
}
