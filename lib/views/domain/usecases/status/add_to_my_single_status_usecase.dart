import 'package:Organiv/views/domain/entities/my_single_status_entity.dart';
import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class AddToMySingleStatus {
  final FirebaseRepository repository;

  AddToMySingleStatus({this.repository});

  Future<void> call(MySingleStatusEntity mySingleChatEntity) async =>
      await repository.addToMySingleStatus(mySingleChatEntity);
}
