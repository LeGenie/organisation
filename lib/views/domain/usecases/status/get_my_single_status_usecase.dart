import 'package:Organiv/views/domain/entities/my_single_status_entity.dart';
import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class GetMySingleStatusUseCase {
  final FirebaseRepository repository;

  GetMySingleStatusUseCase({this.repository});

  Stream<List<MySingleStatusEntity>> call({String uid}) =>
      repository.getOtherSingleStatus(uid);
}
