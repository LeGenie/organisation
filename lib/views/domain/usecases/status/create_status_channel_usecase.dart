import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class CreateStatusChannelUseCase {
  final FirebaseRepository repository;

  CreateStatusChannelUseCase({this.repository});

  Future<void> call({String uid}) async =>
      await repository.createStatusChannel(uid);
}
