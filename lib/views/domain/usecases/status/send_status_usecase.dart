import 'package:Organiv/views/domain/entities/status_entity.dart';
import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class SendStatusUseCase {
  final FirebaseRepository repository;

  SendStatusUseCase({this.repository});

  Future<void> call(StatusEntity statusEntity, String uid,
          {bool update = false}) async =>
      await repository.addStatus(statusEntity, uid, update: update);
}
