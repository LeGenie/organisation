import 'package:Organiv/views/domain/entities/status_entity.dart';
import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class GetStatusUseCase {
  final FirebaseRepository repository;

  GetStatusUseCase({this.repository});

  Stream<List<StatusEntity>> call({String uid, String readUid}) =>
      repository.getStatus(uid, readUid);
}
