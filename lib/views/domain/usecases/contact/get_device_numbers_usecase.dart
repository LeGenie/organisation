import 'package:Organiv/views/domain/repositories/get_device_number_repository.dart';
import 'package:Organiv/views/domain/entities/contact_entity.dart';

class GetDeviceNumbersUseCase {
  final GetDeviceNumberRepository repository;

  GetDeviceNumbersUseCase({this.repository});

  Future<List<ContactEntity>> call() async {
    return await repository.getDeviceNumbers();
  }
}
