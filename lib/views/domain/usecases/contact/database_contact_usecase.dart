import 'package:Organiv/views/data/database/database_local_datasource.dart';
import 'package:Organiv/views/data/model/contact_user_model.dart';

class DatabaseContactUseCase {
  final DatabaseLocalDataSource repository;

  DatabaseContactUseCase({this.repository});

  Future<void> create() async => await repository.createTableContact();

  Future<void> addContact({ContactUserModel contact}) async =>
      await repository.addContact(contact: contact);

  Future<void> deleteContact({String uid}) async =>
      await repository.deleteContact(uid: uid);

  Future<List<ContactUserModel>> getContact() async =>
      await repository.getContact();

  Future<void> truncContact() async => await repository.truncContact();

  Future<void> updateContact({ContactUserModel contact}) async =>
      await repository.updateContact(contact: contact);
}
