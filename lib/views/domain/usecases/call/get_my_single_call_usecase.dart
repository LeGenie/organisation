import 'package:Organiv/views/domain/entities/my_single_call_entity.dart';
import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class GetMySingleCallUseCase {
  final FirebaseRepository repository;

  GetMySingleCallUseCase({this.repository});

  Stream<List<MySingleCallEntity>> call({String uid}) {
    return repository.getMySingleCall(uid);
  }
}
