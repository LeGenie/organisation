import 'package:Organiv/views/domain/repositories/firebase_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class GetAudioUseCase {
  final FirebaseRepository repository;

  GetAudioUseCase({this.repository});

  Stream<DocumentSnapshot> call(String channelId, String callId) {
    return repository.getAudioCalls(channelId, callId);
  }
}
