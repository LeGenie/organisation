import 'package:Organiv/views/domain/entities/call_entity.dart';
import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class EndOneToOneCallUseCase {
  final FirebaseRepository repository;

  EndOneToOneCallUseCase({this.repository});

  Future<void> call({CallEntity call, String callId}) async =>
      await repository.endOneToOneCall(call, callId);
}
