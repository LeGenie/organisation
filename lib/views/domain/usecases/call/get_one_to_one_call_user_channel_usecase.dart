import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class GetOneToOneCallUserChannelUseCase {
  final FirebaseRepository repository;

  GetOneToOneCallUserChannelUseCase({this.repository});

  Future<String> call(
          {String uid, String otherUid, String id = 'ChannelId'}) async =>
      repository.getOneToOneCallUserChannelId(uid, otherUid, id);
}
