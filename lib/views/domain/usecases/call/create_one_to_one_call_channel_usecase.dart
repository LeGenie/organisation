import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class CreateOneToOneCallChannelUseCase {
  final FirebaseRepository repository;

  CreateOneToOneCallChannelUseCase({this.repository});

  Future<void> call({String uid, String otherUid}) async =>
      await repository.createOneToOneCallChannel(uid, otherUid);
}
