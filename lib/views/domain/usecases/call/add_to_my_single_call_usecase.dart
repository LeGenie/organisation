import 'package:Organiv/views/domain/entities/my_single_call_entity.dart';
import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class AddToMySingleCallUseCase {
  final FirebaseRepository repository;

  AddToMySingleCallUseCase({this.repository});

  Future<void> call({MySingleCallEntity mySingleCallEntity}) async =>
      await repository.addToMySingleCall(mySingleCallEntity);
}
