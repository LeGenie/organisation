import 'package:Organiv/views/domain/repositories/firebase_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class GetVideoUseCase {
  final FirebaseRepository repository;

  GetVideoUseCase({this.repository});

  Stream<DocumentSnapshot> call(String channelId, String callId) =>
      repository.getVideoCalls(channelId, callId);
}
