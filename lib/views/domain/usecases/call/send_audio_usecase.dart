import 'package:Organiv/views/domain/entities/audio_entity.dart';
import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class SendAudioUseCase {
  final FirebaseRepository repository;

  SendAudioUseCase({this.repository});

  Future<String> call(AudioEntity audioEntity, String channelId) async =>
      repository.callAudio(audioEntity, channelId);
}
