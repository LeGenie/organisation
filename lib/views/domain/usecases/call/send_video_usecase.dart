import 'package:Organiv/views/domain/entities/video_entity.dart';
import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class SendVideoUseCase {
  final FirebaseRepository repository;

  SendVideoUseCase({this.repository});

  Future<String> call(VideoEntity videoEntity, String channelId) async =>
      repository.callVideo(videoEntity, channelId);
}
