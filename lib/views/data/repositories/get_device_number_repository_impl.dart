import 'package:Organiv/views/data/data_local_datasource/local_datasource.dart';
import 'package:Organiv/views/domain/entities/contact_entity.dart';
import 'package:Organiv/views/domain/repositories/get_device_number_repository.dart';

class GetDeviceNumberRepositoryImpl implements GetDeviceNumberRepository {
  final LocalDataSource localDataSource;

  GetDeviceNumberRepositoryImpl({this.localDataSource});

  @override
  Future<List<ContactEntity>> getDeviceNumbers() =>
      localDataSource.getDeviceNumbers();
}
