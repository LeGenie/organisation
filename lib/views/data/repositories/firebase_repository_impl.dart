import 'package:Organiv/views/data/datasource/firebase_remote_datasource.dart';
import 'package:Organiv/views/domain/entities/audio_entity.dart';
import 'package:Organiv/views/domain/entities/my_single_call_entity.dart';
import 'package:Organiv/views/domain/entities/call_entity.dart';
import 'package:Organiv/views/domain/entities/status_entity.dart';
import 'package:Organiv/views/domain/entities/my_single_status_entity.dart';
import 'package:Organiv/views/domain/entities/text_message_entity.dart';
import 'package:Organiv/views/domain/entities/my_single_chat_entity.dart';
import 'package:Organiv/views/domain/entities/user_entity.dart';
import 'package:Organiv/views/domain/entities/video_entity.dart';
import 'package:Organiv/views/domain/repositories/firebase_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class FirebaseRepositoryImpl implements FirebaseRepository {
  final FirebaseRemoteDataSource remoteDataSource;

  FirebaseRepositoryImpl({this.remoteDataSource});

  @override
  Future<void> singOut() async => remoteDataSource.singOut();

  @override
  Future<void> getCreateCurrentUser(UserEntity user) async =>
      await remoteDataSource.getCreateCurrentUser(user);

  @override
  Future<String> getCurrentUID() async =>
      await remoteDataSource.getCurrentUID();

  @override
  Future<bool> isSignIn() async => remoteDataSource.isSignIn();

  @override
  Future<void> signInWithPhoneNumber(String smspinCode) async =>
      await remoteDataSource.signInWithPhoneNumber(smspinCode);

  @override
  Future<void> verfiyPhoneNumber(String phoneNumber) async =>
      await remoteDataSource.verfiyPhoneNumber(phoneNumber);

  @override
  Future<void> addToMySingleChat(MySingleChatEntity mySingleChatEntity) async =>
      await remoteDataSource.addToMySingleChat(mySingleChatEntity);

  @override
  Future<void> createOneToOneChatChannel(String uid, String otherUid) =>
      remoteDataSource.createOneToOneChatChannel(uid, otherUid);

  @override
  Stream<List<UserEntity>> getAllUsers() => remoteDataSource.getAllUsers();

  @override
  Stream<List<TextMessageEntity>> getTextMessages(String channelId) =>
      remoteDataSource.getTextMessages(channelId);

  @override
  Stream<List<MySingleChatEntity>> getMySingleChat(String uid) =>
      remoteDataSource.getMySingleChat(uid);

  @override
  Future<String> getOneToOneSingleUserChannelId(String uid, String otherUid) =>
      remoteDataSource.getOneToOneSingleUserChannelId(uid, otherUid);

  @override
  Future<void> sendTextMessage(
          TextMessageEntity textMessageEntity, String channelId,
          {bool update = false}) async =>
      !update
          ? remoteDataSource.sendTextMessage(textMessageEntity, channelId)
          : remoteDataSource.updateMessage(textMessageEntity, channelId);

  @override
  Future<void> addStatus(StatusEntity statusEntity, String uid,
          {bool update = false}) =>
      remoteDataSource.addStatus(statusEntity, uid, update);

  @override
  Stream<List<MySingleStatusEntity>> getOtherSingleStatus(String uid) =>
      remoteDataSource.getOtherSingleStatus(uid);

  @override
  Stream<List<StatusEntity>> getStatus(String uid, String readUid) =>
      remoteDataSource.getStatus(uid, readUid);

  @override
  Future<String> getStatusUserChannelId(String uid, String otherUid) async =>
      await remoteDataSource.getStatusUserChannelId(uid, otherUid);

  @override
  Future<void> addToMySingleStatus(
          MySingleStatusEntity mySingleStatusEntity) async =>
      await remoteDataSource.addToMySingleStatus(mySingleStatusEntity);

  @override
  Future<void> createStatusChannel(String uid) async =>
      await remoteDataSource.createStatusChannelUseCase(uid);

  @override
  Future<void> addToMySingleCall(MySingleCallEntity mySingleCallEntity) async =>
      await remoteDataSource.addToMySingleCall(mySingleCallEntity);

  @override
  Future<void> createOneToOneCallChannel(String uid, String otherUid) async =>
      await remoteDataSource.createOneToOneCallChannel(uid, otherUid);

  @override
  Future<String> callAudio(AudioEntity audioEntity, String channelId) async =>
      await remoteDataSource.callAudio(audioEntity, channelId);

  @override
  Future<String> callVideo(VideoEntity videoEntity, String channelId) async =>
      await remoteDataSource.callVideo(videoEntity, channelId);

  @override
  Future<String> getOneToOneCallUserChannelId(
          String uid, String otherUid, String id) async =>
      await remoteDataSource.getOneToOneCallUserChannelId(uid, otherUid, id);

  @override
  Future<void> endOneToOneCall(CallEntity call, String callId) async =>
      await remoteDataSource.endOneToOneCall(call, callId);

  @override
  Stream<List<MySingleCallEntity>> getMySingleCall(String uid) =>
      remoteDataSource.getMySingleCall(uid);

  @override
  Stream<DocumentSnapshot> getAudioCalls(String channelId, String callId) =>
      remoteDataSource.getAudioCalls(channelId, callId);

  @override
  Stream<DocumentSnapshot> getVideoCalls(String channelId, String callId) =>
      remoteDataSource.getVideoCalls(channelId, callId);

  @override
  Future<void> updateUser(UserEntity user) async =>
      await remoteDataSource.updateUser(user);

  @override
  Stream<UserEntity> getUser() => remoteDataSource.getUser();
}
