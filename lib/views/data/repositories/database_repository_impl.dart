import 'package:Organiv/views/data/database/database_local_datasource.dart';
import 'package:Organiv/views/data/model/contact_user_model.dart';
import 'package:Organiv/views/domain/repositories/database_repository.dart';

class DatabaseRepositoryImpl implements DatabaseRepository {
  final DatabaseLocalDataSource localDatabase;

  DatabaseRepositoryImpl({this.localDatabase});

  @override
  Future<void> addContact({ContactUserModel contact}) async =>
      await localDatabase.addContact(contact: contact);

  @override
  Future<void> deleteContact({String uid}) async =>
      await localDatabase.deleteContact(uid: uid);

  @override
  Future<void> updateContact({ContactUserModel contact}) async =>
      await localDatabase.updateContact(contact: contact);

  @override
  Future<void> createTableContact() async =>
      await localDatabase.createTableContact();

  @override
  Future<List<ContactUserModel>> getContact() async =>
      await localDatabase.getContact();

  @override
  Future<void> truncContact() async => await localDatabase.truncContact();

  @override
  Future<void> deleteDatabase() async => await localDatabase.deleteDatabase();

  @override
  Future<void> closeDatabase() async => await localDatabase.closeDatabase();
}
