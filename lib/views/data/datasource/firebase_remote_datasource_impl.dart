import 'dart:async';

import 'package:Organiv/app_const.dart';
import 'package:Organiv/views/data/model/call_model.dart';
import 'package:Organiv/views/data/model/my_single_status_model.dart';
import 'package:Organiv/views/data/model/status_model.dart';
import 'package:Organiv/views/data/model/user_model.dart';
import 'package:Organiv/views/data/datasource/firebase_remote_datasource.dart';
import 'package:Organiv/views/domain/entities/audio_entity.dart';
import 'package:Organiv/views/domain/entities/call_entity.dart';
import 'package:Organiv/views/domain/entities/my_single_call_entity.dart';
import 'package:Organiv/views/domain/entities/status_entity.dart';
import 'package:Organiv/views/domain/entities/my_single_status_entity.dart';
import 'package:Organiv/views/domain/entities/text_message_entity.dart';
import 'package:Organiv/views/domain/entities/my_single_chat_entity.dart';
import 'package:Organiv/views/domain/entities/user_entity.dart';
import 'package:Organiv/views/data/model/text_message_model.dart';
import 'package:Organiv/views/data/model/my_single_chat_model.dart';
import 'package:Organiv/views/data/model/my_single_call_model.dart';
import 'package:Organiv/views/domain/entities/video_entity.dart';
import 'package:Organiv/views/methods/call/call_methods.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';

class FirebaseRemoteDataSourceImpl implements FirebaseRemoteDataSource {
  final FirebaseAuth auth;
  final FirebaseFirestore firestore;
  final FirebaseStorage firebaseStorage;
  final String _singleChannelName = AppConst.SINGLE_CHAT_CHANNEL_COLLECTION;
  final String _statusChannelName = AppConst.STATUS_CHANNEL_COLLECTION;
  final String _callChannelName = AppConst.SINGLE_CALL_CHANNEL_COLLECTION;

  String _verificationId = "";

  FirebaseRemoteDataSourceImpl({
    this.auth,
    this.firestore,
    this.firebaseStorage,
  }) {
    Firebase.initializeApp();
    auth.setLanguageCode("fr");
  }

  //L'authentification par credential
  Future<void> signInWithCredential(var credential) async {
    try {
      await auth.signInWithCredential(credential);
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  Future<void> singOut() async => auth.signOut();

  @override
  Stream<UserEntity> getUser() {
    final _userCollection = firestore.collection(AppConst.USER_COLLECTION);
    final uid = auth.currentUser.uid;

    return _userCollection
        .where("uid", isEqualTo: uid)
        .limit(1)
        .snapshots()
        .map((querySnapshot) => querySnapshot.docs
            .map((doc) => UserModel.fromSnapshot(doc))
            .toList()[0]);
  }

  @override
  @deprecated
  Future<void> getCreateCurrentUser(UserEntity user) async {
    final _userCollection = firestore.collection(AppConst.USER_COLLECTION);
    final uid = await getCurrentUID();
    _userCollection.doc(uid).get().then((userDoc) {
      if (!userDoc.exists) {
        //create new User
        final newUser = UserModel(
          name: user.name,
          phoneNumber: user.phoneNumber,
          status: user.status,
          profilUrl: user.profilUrl,
          isoCode: user.isoCode,
          internationalizedPhoneNumber: user.internationalizedPhoneNumber,
          macAdress: user.macAdress,
          markAdress: user.markAdress,
          ipAdress: user.ipAdress,
          uid: uid,
          lastView: user.lastView,
          callId: null,
          hasCall: false,
          hasDialled: false,
          stream: false,
          nullable: true,
          count: 0,
          countCall: 0,
          countDisc: 0,
          countMessage: 0,
          countStatus: 0,
          countTweet: 0,
          online: true,
          silence: false,
          created_at: user.created_at,
          updated_at: user.updated_at,
        ).toDocument();
        _userCollection.doc(uid).set(newUser);
      } else {
        //Update a user
        final updateUser = UserModel(
          name: user.name,
          macAdress: user.macAdress,
          markAdress: user.markAdress,
          ipAdress: user.ipAdress,
          updated_at: user.updated_at,
          lastView: user.lastView,
          online: true,
          callId: user.callId,
          hasDialled: false,
          hasCall: false,
          stream: false,
          silence: user.silence,
          nullable: false,
        ).toDocument();
        _userCollection.doc(uid).update(updateUser);
      }
    }).catchError((e) {
      print(e.toString());
    });
  }

  @override
  Future<void> updateUser(UserEntity user) async {
    final _userCollection = firestore.collection(AppConst.USER_COLLECTION);
    _userCollection.doc(user.uid).get().then((userDoc) {
      if (userDoc.exists) {
        //Update a user
        final updateUser = UserModel(
          name: user.name,
          status: user.status,
          profilUrl: user.profilUrl,
          macAdress: user.macAdress,
          markAdress: user.markAdress,
          ipAdress: user.ipAdress,
          updated_at: Timestamp.now(),
          lastView: user.lastView,
          count: user.count,
          countMessage: user.countMessage,
          countStatus: user.countStatus,
          countCall: user.countCall,
          countDisc: user.countDisc,
          countTweet: user.countTweet,
          online: user.online,
          callId: user.callId,
          hasDialled: user.hasDialled,
          hasCall: user.hasCall,
          stream: user.stream,
          silence: user.silence,
          nullable: false,
        ).toDocument();
        _userCollection.doc(user.uid).update(updateUser);
      }
    });
  }

  @override
  Future<String> getCurrentUID() async => auth.currentUser.uid;

  @override
  Future<bool> isSignIn() async => auth.currentUser.uid != null;

  @override
  Future<void> signInWithPhoneNumber(String smspinCode) async {
    final AuthCredential authCredential = PhoneAuthProvider.credential(
        verificationId: _verificationId, smsCode: smspinCode);
    await signInWithCredential(authCredential)
        .catchError((e) => print(e.toString()));
  }

  @override
  Future<void> verfiyPhoneNumber(String phoneNumber) async {
    final PhoneVerificationCompleted phoneVerificationCompleted =
        (AuthCredential authCrendential) {
      print("Phone verified : Token ${authCrendential.token}");
    };

    final PhoneVerificationFailed phoneVerificationFailed =
        (FirebaseAuthException firebaseAuthException) {
      print(
        "Phone verified failed : ${firebaseAuthException.message}, ${firebaseAuthException.code}",
      );
    };

    final PhoneCodeAutoRetrievalTimeout phoneCodeAutoRetrievalTimeout =
        (String verificationId) {
      this._verificationId = verificationId;
    };

    final PhoneCodeSent phoneCodeSent =
        (String verificationId, [int forceResendingToken]) {
      this._verificationId = verificationId;
    };

    await auth.verifyPhoneNumber(
      phoneNumber: phoneNumber,
      verificationCompleted: phoneVerificationCompleted,
      verificationFailed: phoneVerificationFailed,
      timeout: const Duration(seconds: 30),
      codeSent: phoneCodeSent,
      codeAutoRetrievalTimeout: phoneCodeAutoRetrievalTimeout,
    );
  }

  @override
  Future<void> addToMySingleChat(MySingleChatEntity mySingleChatEntity) async {
    final myChatRef = firestore
        .collection(AppConst.USER_COLLECTION)
        .doc(mySingleChatEntity.senderUID)
        .collection(_singleChannelName);
    final otherChatRef = firestore
        .collection(AppConst.USER_COLLECTION)
        .doc(mySingleChatEntity.recipientUID)
        .collection(_singleChannelName);

    final mySingleNewChat = new MySingleChatModel(
      time: mySingleChatEntity.time,
      senderName: mySingleChatEntity.senderName,
      senderUID: mySingleChatEntity.senderUID,
      recipientName: mySingleChatEntity.recipientName,
      recipientUID: mySingleChatEntity.recipientUID,
      channelId: mySingleChatEntity.channelId,
      isArchived: mySingleChatEntity.isArchived,
      isRead: mySingleChatEntity.isRead,
      profilURL: mySingleChatEntity.profilURL,
      count: 0,
      lastView: mySingleChatEntity.lastView,
      online: mySingleChatEntity.online,
      silence: mySingleChatEntity.silence,
      recentTextMessage: mySingleChatEntity.recentTextMessage,
      senderPhoneNumber: mySingleChatEntity.senderPhoneNumber,
      recipientPhoneNumber: mySingleChatEntity.recipientPhoneNumber,
      updated_at: Timestamp.now(),
      nullable: mySingleChatEntity.nullable,
    ).toDocument();

    final otherSingleNewChat = new MySingleChatModel(
      time: mySingleChatEntity.time,
      senderName: mySingleChatEntity.recipientName,
      senderUID: mySingleChatEntity.recipientUID,
      recipientName: mySingleChatEntity.senderName,
      recipientUID: mySingleChatEntity.senderUID,
      channelId: mySingleChatEntity.channelId,
      isArchived: mySingleChatEntity.isArchived,
      isRead: mySingleChatEntity.isRead,
      profilURL: mySingleChatEntity.profilURL,
      recentTextMessage: mySingleChatEntity.recentTextMessage,
      senderPhoneNumber: mySingleChatEntity.recipientPhoneNumber,
      recipientPhoneNumber: mySingleChatEntity.senderPhoneNumber,
      updated_at: Timestamp.now(),
      nullable: mySingleChatEntity.nullable,
    ).toDocument();

    myChatRef
        .doc(mySingleChatEntity.recipientUID)
        .get()
        .then((mySingleChatDoc) {
      if (!mySingleChatDoc.exists) {
        //Create
        myChatRef.doc(mySingleChatEntity.recipientUID).set(mySingleNewChat);
        otherChatRef.doc(mySingleChatEntity.senderUID).set(otherSingleNewChat);
      } else {
        //Update
        myChatRef.doc(mySingleChatEntity.recipientUID).update(mySingleNewChat);
        otherChatRef
            .doc(mySingleChatEntity.senderUID)
            .update(otherSingleNewChat);
      }

      return;
    });
    //On met à jour les comptages
    updateUserCountable(uid: mySingleChatEntity.senderUID);
    updateUserCountable(uid: mySingleChatEntity.recipientUID);
    otherChatRef
        .doc(mySingleChatEntity.senderUID)
        .get()
        .then((myOtherChatDoc) async {
      if (myOtherChatDoc.exists) {
        final countSingleNewChat = new MySingleChatModel(
          count: myOtherChatDoc.data()['count'] + 1,
          updated_at: Timestamp.now(),
          nullable: false,
        ).toDocument();
        if (mySingleChatEntity.recentTextMessage != null)
          otherChatRef
              .doc(mySingleChatEntity.senderUID)
              .update(countSingleNewChat);
      }
      return;
    });
  }

  Future<void> updateUserCountable({String uid}) async {
    final userRef = firestore.collection(AppConst.USER_COLLECTION).doc(uid);
    userRef.get().then((user) {
      if (user.exists) {
        //if user exists, we contable all item an give then
        int count = 0;
        int countMessage;
        userRef
            .collection(_singleChannelName)
            .where("count", isGreaterThan: 0)
            .get()
            .then((value) {
          countMessage = value.size;
          count = count + countMessage;
          if (count == user.data()['count'] &&
              countMessage == user.data()['countMessage']) return;
          final userCountableUpdate = UserModel(
            count: count,
            countMessage: countMessage,
            nullable: false,
          ).toDocument();
          userRef.update(userCountableUpdate);
        });
      }
      return;
    });
  }

  @override
  Future<void> createOneToOneChatChannel(String uid, String otherUid) async {
    final _userCollectionRef = firestore.collection(AppConst.USER_COLLECTION);
    final _oneToOneChatChannelRef =
        firestore.collection(AppConst.MY_CHAT_CHANNEL_COLLECTION);
    _userCollectionRef
        .doc(uid)
        .collection(_singleChannelName)
        .doc(otherUid)
        .get()
        .then((singleChatChannelDoc) {
      if (singleChatChannelDoc.exists) {
        return;
      }
      //if not exists
      final _singleChatChannelId = _oneToOneChatChannelRef.doc().id;
      var _singleChannelMap = {
        "channelId": _singleChatChannelId,
        "channelType": AppConst.oneToOne,
        "created_at": Timestamp.now(),
        "count": 0,
      };
      _oneToOneChatChannelRef.doc(_singleChatChannelId).set(_singleChannelMap);

      //currentUser
      _userCollectionRef
          .doc(uid)
          .collection(_singleChannelName)
          .doc(otherUid)
          .set(_singleChannelMap);

      //OtherUser
      _userCollectionRef
          .doc(otherUid)
          .collection(_singleChannelName)
          .doc(uid)
          .set(_singleChannelMap);
      return;
    });
  }

  @override
  Stream<List<UserEntity>> getAllUsers() {
    final _userCollectionRef = firestore.collection(AppConst.USER_COLLECTION);
    return _userCollectionRef.snapshots().map((querySnapshot) {
      return querySnapshot.docs
          .map((docQuerySnapshot) => UserModel.fromSnapshot(docQuerySnapshot))
          .toList();
    });
  }

  @override
  Stream<List<MySingleChatEntity>> getMySingleChat(String uid) {
    final mySingleChatRef = firestore
        .collection(AppConst.USER_COLLECTION)
        .doc(uid)
        .collection(_singleChannelName);
    updateUserCountable(uid: uid);
    return mySingleChatRef.orderBy("time", descending: true).snapshots().map(
        (querySnapshot) => querySnapshot.docs
            .map((doc) => MySingleChatModel.fromSnapShot(doc))
            .toList());
  }

  @override
  Future<String> getOneToOneSingleUserChannelId(
      String uid, String otherUid) async {
    final _userCollectionRef = firestore.collection(AppConst.USER_COLLECTION);
    return _userCollectionRef
        .doc(uid)
        .collection(_singleChannelName)
        .doc(otherUid)
        .get()
        .then((singleChatChannel) {
      if (singleChatChannel.exists) {
        return singleChatChannel.data()['channelId'];
      }
      return Future.value(null);
    });
  }

  @override
  Stream<List<TextMessageEntity>> getTextMessages(String channelId) {
    final messageRef = firestore
        .collection(AppConst.MY_CHAT_CHANNEL_COLLECTION)
        .doc(channelId)
        .collection(AppConst.MESSAGE_COLLECTION);

    return messageRef.orderBy("time").snapshots().map(
          (querySnap) => querySnap.docs
              .map((doc) => TextMessageModel.fromSnapShot(doc))
              .toList(),
        );
  }

  @override
  Future<void> updateMessage(
      TextMessageEntity textMessageEntity, String channelId) async {
    final _messageCollection =
        firestore.collection(AppConst.MY_CHAT_CHANNEL_COLLECTION);
    _messageCollection
        .doc(channelId)
        .collection(AppConst.MESSAGE_COLLECTION)
        .doc(textMessageEntity.messageId)
        .get()
        .then((messageDoc) {
      if (messageDoc.exists) {
        //Update a message
        final updateMessage = TextMessageModel(
          lastView: Timestamp.now(),
          messageId: textMessageEntity.messageId,
          isSend: textMessageEntity.isSend,
          isDeleted: textMessageEntity.isDeleted,
          count: textMessageEntity.count,
          isRead: textMessageEntity.isRead,
          isReceived: textMessageEntity.isReceived,
          message: textMessageEntity.message,
          messageType: textMessageEntity.messageType,
          online: textMessageEntity.online,
          recipientName: textMessageEntity.recipientName,
          recipientUID: textMessageEntity.recipientUID,
          senderName: textMessageEntity.senderName,
          senderUID: textMessageEntity.senderUID,
          silence: textMessageEntity.silence,
          time: textMessageEntity.time,
          nullable: false,
        ).toDocument();
        _messageCollection
            .doc(channelId)
            .collection(AppConst.MESSAGE_COLLECTION)
            .doc(textMessageEntity.messageId)
            .update(updateMessage);
      }
    }).catchError((e) {
      print(e.toString());
    });
  }

  @override
  Future<void> sendTextMessage(
      TextMessageEntity textMessageEntity, String channelId) async {
    final messageRef = firestore
        .collection(AppConst.MY_CHAT_CHANNEL_COLLECTION)
        .doc(channelId)
        .collection(AppConst.MESSAGE_COLLECTION);

    final messageId = messageRef.doc().id;

    final newMessage = new TextMessageModel(
      messageId: messageId,
      message: textMessageEntity.message,
      messageType: textMessageEntity.messageType,
      recipientUID: textMessageEntity.recipientUID,
      recipientName: textMessageEntity.recipientName,
      senderUID: textMessageEntity.senderUID,
      senderName: textMessageEntity.senderName,
      time: textMessageEntity.time,
      isRead: textMessageEntity.isRead,
      isReceived: textMessageEntity.isReceived,
      isDeleted: textMessageEntity.isDeleted,
      isSend: false,
    ).toDocument();
    messageRef.doc(messageId).set(newMessage);
  }

  @override
  Future<void> addStatus(
      StatusEntity statusEntity, String uid, bool update) async {
    final statusRef = firestore
        .collection(AppConst.MY_STATUS_CHANNEL_COLLECTION)
        .doc(uid)
        .collection(AppConst.STATUS_COLLECTION);

    final statusId = statusRef.doc().id;

    final newStatus = new StatusModel(
      color: statusEntity.color,
      legende: statusEntity.legende,
      link: statusEntity.link,
      message: statusEntity.message,
      statusId: statusId,
      size: statusEntity.size,
      statusType: statusEntity.statusType,
      time: statusEntity.time,
      uids: statusEntity.uids,
      nullable: statusEntity.nullable,
    ).toDocument();
    //if exists
    if (update)
      statusRef.doc(statusId).update(newStatus);
    else
      statusRef.doc(statusId).set(newStatus);
  }

  @override
  Stream<List<MySingleStatusEntity>> getOtherSingleStatus(String uid) {
    final myStatusRef =
        firestore.collection(AppConst.MY_STATUS_CHANNEL_COLLECTION);
    updateUserCountable(uid: uid);
    return myStatusRef.orderBy("time", descending: true).snapshots().map(
        (querySnapshot) => querySnapshot.docs
            .map((doc) => MySingleStatusModel.fromSnapshot(doc))
            .toList());
  }

  @override
  Stream<List<StatusEntity>> getStatus(String uid, String readUid) {
    final statusRef = firestore
        .collection(AppConst.MY_STATUS_CHANNEL_COLLECTION)
        .doc(uid)
        .collection(AppConst.STATUS_COLLECTION);
    if (readUid != null)
      return statusRef
          .where("uids", arrayContains: readUid)
          .orderBy("time")
          .snapshots()
          .map((querySnap) => querySnap.docs
              .map((doc) => StatusModel.fromSnapshot(doc))
              .toList());
    else
      return statusRef.orderBy("time").snapshots().map((querySnap) =>
          querySnap.docs.map((doc) => StatusModel.fromSnapshot(doc)).toList());
  }

  @override
  Future<String> getStatusUserChannelId(String uid, String otherUid) {
    final _userCollectionRef = firestore.collection(AppConst.USER_COLLECTION);
    return _userCollectionRef
        .doc(uid)
        .collection(_statusChannelName)
        .doc(otherUid)
        .get()
        .then((statusChannel) {
      if (statusChannel.exists) {
        return statusChannel.data()['channelId'];
      }
      return Future.value(null);
    });
  }

  @override
  Future<void> addToMySingleStatus(
      MySingleStatusEntity mySingleStatusEntity) async {
    final myChatRef = firestore
        .collection(AppConst.MY_STATUS_CHANNEL_COLLECTION)
        .doc(mySingleStatusEntity.channelId);

    final mySingleNewStatus = new MySingleStatusModel(
      time: mySingleStatusEntity.time,
      profilURL: mySingleStatusEntity.profilURL,
      count: mySingleStatusEntity.count,
      online: mySingleStatusEntity.online,
      silence: mySingleStatusEntity.silence,
      nullable: mySingleStatusEntity.nullable,
    ).toDocument();

    myChatRef.get().then((mySingleChatDoc) async {
      if (!mySingleChatDoc.exists) {
        //Create
        await createStatusChannelUseCase(mySingleStatusEntity.channelId);
        return addToMySingleStatus(mySingleStatusEntity);
      } else {
        //Update
        return myChatRef.update(mySingleNewStatus);
      }
    });
  }

  @override
  Future<void> createStatusChannelUseCase(String uid) async {
    final _statusRef =
        firestore.collection(AppConst.MY_STATUS_CHANNEL_COLLECTION);
    _statusRef.doc(uid).get().then((statusChannelDoc) {
      if (statusChannelDoc.exists) {
        return;
      }
      //if not exists
      var _singleChannelMap = {
        "channelId": uid,
        "channelType": AppConst.status,
        "count": 0,
        "time": Timestamp.now(),
      };
      _statusRef.doc(uid).set(_singleChannelMap);
      return;
    });
  }

  @override
  Future<void> addToMySingleCall(MySingleCallEntity mySingleCallEntity) async {
    final myCallRef = firestore
        .collection(AppConst.USER_COLLECTION)
        .doc(mySingleCallEntity.callerId)
        .collection(_callChannelName);
    final otherCallRef = firestore
        .collection(AppConst.USER_COLLECTION)
        .doc(mySingleCallEntity.recipientId)
        .collection(_callChannelName);

    final mySingleNewCall = new MySingleCallModel(
      time: mySingleCallEntity.time,
      channelId: mySingleCallEntity.channelId,
      pipeId: mySingleCallEntity.pipeId,
      isArchived: mySingleCallEntity.isArchived,
      isRead: mySingleCallEntity.isRead,
      callerPic: mySingleCallEntity.callerPic,
      receiverPic: mySingleCallEntity.receiverPic,
      count: mySingleCallEntity.count,
      lastView: mySingleCallEntity.lastView,
      online: mySingleCallEntity.online,
      silence: mySingleCallEntity.silence,
      callerId: mySingleCallEntity.callerId,
      callerName: mySingleCallEntity.callerName,
      hasDialled: true,
      recipientId: mySingleCallEntity.recipientId,
      recipientName: mySingleCallEntity.recipientName,
      type: mySingleCallEntity.type,
      updated_at: Timestamp.now(),
      nullable: mySingleCallEntity.nullable,
    ).toDocument();

    final otherSingleNewCall = new MySingleCallModel(
      time: mySingleCallEntity.time,
      channelId: mySingleCallEntity.channelId,
      pipeId: mySingleCallEntity.pipeId,
      isArchived: mySingleCallEntity.isArchived,
      isRead: mySingleCallEntity.isRead,
      callerPic: mySingleCallEntity.receiverPic,
      receiverPic: mySingleCallEntity.callerPic,
      lastView: mySingleCallEntity.lastView,
      online: mySingleCallEntity.online,
      silence: mySingleCallEntity.silence,
      callerId: mySingleCallEntity.recipientId,
      callerName: mySingleCallEntity.recipientName,
      hasDialled: false,
      recipientId: mySingleCallEntity.callerId,
      recipientName: mySingleCallEntity.callerName,
      type: mySingleCallEntity.type,
      updated_at: Timestamp.now(),
      nullable: mySingleCallEntity.nullable,
    ).toDocument();

    myCallRef.doc(mySingleCallEntity.recipientId).get().then((mySingleCallDoc) {
      if (!mySingleCallDoc.exists) {
        //Create
        myCallRef.doc(mySingleCallEntity.recipientId).set(mySingleNewCall);
        otherCallRef.doc(mySingleCallEntity.callerId).set(otherSingleNewCall);
      } else {
        //Update
        myCallRef.doc(mySingleCallEntity.recipientId).update(mySingleNewCall);
        otherCallRef
            .doc(mySingleCallEntity.callerId)
            .update(otherSingleNewCall);
      }
      return;
    });
  }

  @override
  Future<void> createOneToOneCallChannel(String uid, String otherUid) async {
    final _userCollectionRef = firestore.collection(AppConst.USER_COLLECTION);
    final _oneToOneCallChannelRef =
        firestore.collection(AppConst.MY_CALL_CHANNEL_COLLECTION);
    _userCollectionRef
        .doc(uid)
        .collection(_callChannelName)
        .doc(otherUid)
        .get()
        .then((singleCallChannelDoc) {
      if (singleCallChannelDoc.exists) {
        return;
      }
      //if not exists
      final _singleCallChannelId = _oneToOneCallChannelRef.doc().id;
      var _singleChannelMap = {
        "channelId": _singleCallChannelId,
        "channelType": AppConst.oneToOne,
      };
      _oneToOneCallChannelRef.doc(_singleCallChannelId).set(_singleChannelMap);

      //currentUser
      _userCollectionRef
          .doc(uid)
          .collection(_callChannelName)
          .doc(otherUid)
          .set(_singleChannelMap);

      //OtherUser
      _userCollectionRef
          .doc(otherUid)
          .collection(_callChannelName)
          .doc(uid)
          .set(_singleChannelMap);
      return;
    });
  }

  @override
  Future<String> callAudio(AudioEntity audioEntity, String channelId) async {
    final _callAudioRef = firestore
        .collection(AppConst.MY_CALL_CHANNEL_COLLECTION)
        .doc(channelId);
    //On met à jour les informations générales sur l'appel
    _callAudioRef.get().then((myCallChannel) {
      Map<String, dynamic> map = {
        'channelId': channelId,
        'channelType': AppConst.oneToOne,
        'channelExchange': AppConst.call_audio,
      };
      if (myCallChannel.exists)
        return _callAudioRef.update(map);
      else
        return _callAudioRef.set(map);
    });

    String callId = await call(audioEntity, channelId, AppConst.call_audio);

    MySingleCallEntity mySingleCallEntity = MySingleCallEntity(
      pipeId: callId,
      channelId: channelId,
      callerId: audioEntity.callerId,
      callerName: audioEntity.callerName,
      recipientId: audioEntity.receiverId,
      recipientName: audioEntity.receiverName,
      callerPic: audioEntity.callerPic,
      receiverPic: audioEntity.receiverPic,
      online: true,
      type: AppConst.call_audio,
      time: Timestamp.now(),
      nullable: true,
    );

    await addToMySingleCall(mySingleCallEntity);
    return callId;
  }

  @override
  Future<String> callVideo(VideoEntity videoEntity, String channelId) async {
    final _callVideoRef = firestore
        .collection(AppConst.MY_CALL_CHANNEL_COLLECTION)
        .doc(channelId);
    //On met à jour les informations générales sur l'appel
    _callVideoRef.get().then((myCallChannel) {
      Map<String, dynamic> map = {
        'channelId': channelId,
        'channelType': AppConst.oneToOne,
        'channelExchange': AppConst.call_video,
      };
      if (myCallChannel.exists)
        return _callVideoRef.update(map);
      else
        return _callVideoRef.set(map);
    });

    String callId = await call(videoEntity, channelId, AppConst.call_video);
    MySingleCallEntity mySingleCallEntity = MySingleCallEntity(
      pipeId: callId,
      channelId: channelId,
      callerId: videoEntity.callerId,
      callerName: videoEntity.callerName,
      recipientId: videoEntity.receiverId,
      recipientName: videoEntity.receiverName,
      callerPic: videoEntity.callerPic,
      receiverPic: videoEntity.receiverPic,
      online: true,
      type: AppConst.call_video,
      time: Timestamp.now(),
      nullable: true,
    );

    await addToMySingleCall(mySingleCallEntity);
    return callId;
  }

  @override
  Future<String> getOneToOneCallUserChannelId(
      String uid, String otherUid, String id) async {
    final _userCollectionRef = firestore.collection(AppConst.USER_COLLECTION);
    return _userCollectionRef
        .doc(uid)
        .collection(_callChannelName)
        .doc(otherUid)
        .get()
        .then((statusChannel) {
      if (statusChannel.exists) {
        if (id == 'ChannelId')
          return statusChannel.data()['channelId'];
        else
          return statusChannel.data()['pipeId'];
      } else
        return Future.value(null);
    });
  }

  @override
  Future<void> endOneToOneCall(CallEntity call, String callId) async {
    final _callRef = firestore
        .collection(AppConst.MY_CALL_CHANNEL_COLLECTION)
        .doc(call.channelId)
        .collection(AppConst.CALL_COLLECTION)
        .doc(callId);
    final _myCallRef = firestore
        .collection(AppConst.USER_COLLECTION)
        .doc(call.callerId)
        .collection(_callChannelName);
    final _otherCallRef = firestore
        .collection(AppConst.USER_COLLECTION)
        .doc(call.receiverId)
        .collection(_callChannelName);
    final _userRef =
        firestore.collection(AppConst.USER_COLLECTION).doc(call.callerId);
    final _otherRef =
        firestore.collection(AppConst.USER_COLLECTION).doc(call.receiverId);

    final mySingleNewCall = new MySingleCallModel(
      channelId: call.channelId,
      pipeId: callId,
      nullable: false,
      online: false,
      updated_at: Timestamp.now(),
    ).toDocument();

    final otherSingleNewCall = new MySingleCallModel(
      channelId: call.channelId,
      pipeId: callId,
      nullable: false,
      online: false,
      updated_at: Timestamp.now(),
    ).toDocument();

    final callEnd = new CallModel(
      hasDialled: false,
      count: 2,
      online: false,
      lastView: new Timestamp.now(),
      nullable: false,
    ).toDocument();

    await _myCallRef.doc(call.receiverId).update(mySingleNewCall);
    await _otherCallRef.doc(call.callerId).update(otherSingleNewCall);
    await _callRef.update(callEnd);

    //Update a user
    final updateUser = UserModel(
      callId: "",
      hasCall: false,
      hasDialled: false,
      nullable: false,
      stream: false,
    ).toDocument();

    _userRef.update(updateUser);
    _otherRef.update(updateUser);
  }

  @override
  Stream<List<MySingleCallEntity>> getMySingleCall(String uid) {
    final mySingleCallRef = firestore
        .collection(AppConst.USER_COLLECTION)
        .doc(uid)
        .collection(_callChannelName);

    updateUserCountable(uid: uid);
    return mySingleCallRef.orderBy("time", descending: true).snapshots().map(
        (querySnapshot) => querySnapshot.docs
            .map((doc) => MySingleCallModel.fromSnapShot(doc))
            .toList());
  }

  @override
  Stream<DocumentSnapshot> getAudioCalls(String channelId, String callId) {
    return getCalls(channelId, callId);
  }

  @override
  Stream<DocumentSnapshot> getVideoCalls(String channelId, String callId) {
    return getCalls(channelId, callId);
  }

  @override
  Stream<DocumentSnapshot> getCalls(String channelId, String callId) {
    final _myCallRef = firestore
        .collection(AppConst.MY_CALL_CHANNEL_COLLECTION)
        .doc(channelId)
        .collection(AppConst.CALL_COLLECTION)
        .doc(callId)
        .snapshots();

    return _myCallRef;
  }

  @override
  Future<String> call(
      CallEntity callEntity, String channelId, String type) async {
    final oneTooneCallChannel = firestore
        .collection(AppConst.MY_CALL_CHANNEL_COLLECTION)
        .doc(channelId);
    final _userRef =
        firestore.collection(AppConst.USER_COLLECTION).doc(callEntity.callerId);
    final _otherRef = firestore
        .collection(AppConst.USER_COLLECTION)
        .doc(callEntity.receiverId);
    //L'ID de la vidéo
    String _callId =
        oneTooneCallChannel.collection(AppConst.CALL_COLLECTION).doc().id;

    final CallModel callModel = CallModel(
      callerId: callEntity.callerId,
      callerName: callEntity.callerName,
      callerPic: callEntity.callerPic,
      channelId: _callId,
      receiverId: callEntity.receiverId,
      receiverName: callEntity.receiverName,
      receiverPic: callEntity.receiverPic,
      type: type,
      nullable: true,
      time: Timestamp.now(),
    );
    final CallMethods callMethods = new CallMethods(call: callModel);

    //On lance l'appel
    callMethods.makeCall();

    await oneTooneCallChannel
        .collection(AppConst.CALL_COLLECTION)
        .doc(_callId)
        .set(callMethods.hasDialledMap);

    //Update a user
    final otherUser = UserModel(
      callId: callEntity.callerId,
      hasCall: true,
      hasDialled: false,
      nullable: false,
    ).toDocument();

    final myUser = UserModel(
      callId: callEntity.receiverId,
      hasDialled: true,
      hasCall: true,
      nullable: false,
    ).toDocument();

    _userRef.update(myUser);
    _otherRef.update(otherUser);

    return _callId;
  }
}
