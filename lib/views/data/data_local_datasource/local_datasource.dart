import 'package:Organiv/views/domain/entities/contact_entity.dart';
import 'package:contacts_service/contacts_service.dart';

abstract class LocalDataSource {
  Future<List<ContactEntity>> getDeviceNumbers();
}

class LocalDataSourceImpl implements LocalDataSource {
  @override
  Future<List<ContactEntity>> getDeviceNumbers() async {
    List<ContactEntity> _contacts = [];
    final _getContactsData = await ContactsService.getContacts();

    _getContactsData.forEach((myContact) {
      myContact.phones.forEach((phoneData) {
        _contacts.add(ContactEntity(
          phoneNumber: phoneData.value,
          label: myContact.displayName,
        ));
      });
    });
    return _contacts;
  }
}
