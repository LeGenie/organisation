import 'package:Organiv/views/data/model/contact_user_model.dart';

abstract class DatabaseLocalDataSource {
  Future<void> deleteDatabase();
  Future<void> closeDatabase();

  //Create, insert, update and delete contact user in database
  Future<void> createTableContact();
  Future<List<ContactUserModel>> getContact();
  Future<void> addContact({ContactUserModel contact});
  Future<void> updateContact({ContactUserModel contact});
  Future<void> deleteContact({String uid});
  Future<void> truncContact();
}
