import 'package:Organiv/views/data/database/database_local_datasource.dart';
import 'package:Organiv/views/data/model/contact_user_model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_common/sqlite_api.dart';

class DatabaseLocalDataSourceImpl implements DatabaseLocalDataSource {
  final Database database;
  final String contactTable = 'contacts';
  final String contactTableExpr =
      "CREATE TABLE IF NOT EXISTS contacts(id INTEGER PRIMARY KEY, internationalizedPhoneNumber VARCHAR(191) NULL DEFAULT NULL," +
          " label VARCHAR(191) NULL DEFAULT NULL, phoneNumber VARCHAR(191) NULL DEFAULT NULL, nameUser VARCHAR(191) NULL DEFAULT NULL," +
          " status VARCHAR(191) NULL DEFAULT NULL, count INTEGER  NULL DEFAULT 0, silence INTEGER  NULL DEFAULT 0, online INTEGER  NULL DEFAULT 0, " +
          " milliseconds INTEGER, uid VARCHAR(191)  NULL DEFAULT NULL, profilURL VARCHAR(191)  NULL DEFAULT NULL)";

  DatabaseLocalDataSourceImpl({this.database});

  @override
  Future<void> addContact({ContactUserModel contact}) async {
    final Database db = database;
    Map<String, dynamic> map = {
      "internationalizedPhoneNumber": contact.internationalizedPhoneNumber,
      "label": contact.label,
      "phoneNumber": contact.phoneNumber,
      "nameUser": contact.nameUser,
      "status": contact.status,
      "count": contact.count,
      "profilURL": contact.profilURL,
      "silence": contact.silence ? 1 : 0,
      "online": contact.silence ? 1 : 0,
      "uid": contact.uid,
      "milliseconds": contact.lastView.millisecondsSinceEpoch,
    };
    await db.insert(contactTable, map,
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  @override
  Future<void> createTableContact() async {
    final Database db = database;
    await db.execute(contactTableExpr);
  }

  @override
  Future<void> deleteContact({String uid}) async {
    final Database db = database;
    db.delete(contactTable, where: 'uid = ?', whereArgs: [uid]);
  }

  @override
  Future<List<ContactUserModel>> getContact() async {
    final Database db = database;
    final List<Map<String, dynamic>> contacts = await db.query(contactTable);
    //Convert map to object
    return List.generate(contacts.length, (index) {
      return ContactUserModel.fromMap(contacts[index]);
    });
  }

  @override
  Future<void> truncContact() async {
    final Database db = database;
    db.delete(contactTable);
  }

  @override
  Future<void> updateContact({ContactUserModel contact}) async {
    final Database db = database;
    await db.update(contactTable, contact.toDocument(),
        where: 'uid = ?', whereArgs: [contact.uid]);
  }

  @override
  Future<void> deleteDatabase() {}

  @override
  Future<void> closeDatabase() async {
    final Database db = database;
    db.close();
  }
}
