import 'package:Organiv/views/domain/entities/current_call_entity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class CurrentCallModel extends CurrentCallEntity {
  CurrentCallModel({
    String callerId,
    String callerName,
    Timestamp time,
    Timestamp created_at,
    Timestamp updated_at,
    String profilURL,
    String channelId,
    String pipeId,
  }) : super(
          callerId: callerId,
          callerName: callerName,
          time: time,
          channelId: channelId,
          pipeId: pipeId,
          profilURL: profilURL,
          updated_at: updated_at,
          created_at: created_at,
        );

  factory CurrentCallModel.fromSnapShot(DocumentSnapshot snapshot) {
    return CurrentCallModel(
      callerId: snapshot.data()['callerId'],
      callerName: snapshot.data()['callerName'],
      profilURL: snapshot.data()['profilURL'],
      channelId: snapshot.data()['channelId'],
      pipeId: snapshot.data()['pipeId'],
      time: snapshot.data()['time'],
      created_at: snapshot.data()['created_at'],
      updated_at: snapshot.data()['updated_at'],
    );
  }

  Map<String, dynamic> toDocument() {
    Map<String, dynamic> json = {
      "callerId": callerId,
      "callerName": callerName,
      "profilURL": profilURL,
      "channelId": channelId,
      "pipeId": pipeId,
      "time": time,
      'created_at': created_at,
      'updated_at': Timestamp.now(),
    };

    return json;
  }
}
