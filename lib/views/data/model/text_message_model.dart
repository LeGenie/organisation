import 'package:Organiv/views/domain/entities/text_message_entity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class TextMessageModel extends TextMessageEntity {
  TextMessageModel({
    String senderName,
    String senderUID,
    String recipientName,
    String recipientUID,
    String messageType,
    String message,
    String messageId,
    bool isReceived,
    bool isRead,
    bool isSend,
    bool isDeleted = false,
    Timestamp time,
    int count,
    Timestamp lastView,
    Timestamp created_at,
    Timestamp updated_at,
    bool silence,
    bool online,
    bool nullable = true,
  }) : super(
          senderName: senderName,
          senderUID: senderUID,
          recipientName: recipientName,
          recipientUID: recipientUID,
          messageType: messageType,
          message: message,
          messageId: messageId,
          time: time,
          isRead: isRead,
          isReceived: isReceived,
          isSend: isSend,
          isDeleted: isDeleted,
          nullable: nullable,
          count: count,
          silence: silence,
          online: online,
          lastView: lastView,
          created_at: created_at,
        );

  factory TextMessageModel.fromSnapShot(DocumentSnapshot snapshot) {
    return TextMessageModel(
      senderName: snapshot.data()['senderName'],
      senderUID: snapshot.data()['senderUID'],
      recipientName: snapshot.data()['recipientName'],
      recipientUID: snapshot.data()['recipientUID'],
      messageType: snapshot.data()['messageType'],
      message: snapshot.data()['message'],
      time: snapshot.data()['time'],
      isRead: snapshot.data()['isRead'],
      lastView: snapshot.data()['lastView'],
      online: snapshot.data()['online'],
      silence: snapshot.data()['silence'],
      count: snapshot.data()['count'],
      messageId: snapshot.data()['messageId'],
      isReceived: snapshot.data()['isReceived'],
      isDeleted: snapshot.data()['isDeleted'],
      isSend: snapshot.data()['isSend'],
      created_at: snapshot.data()['created_at'],
      updated_at: snapshot.data()['updated_at'],
    );
  }

  Map<String, dynamic> toDocument() {
    Map<String, dynamic> json = {
      'senderName': senderName,
      'senderUID': senderUID,
      'recipientName': recipientName,
      'recipientUID': recipientUID,
      'messageType': messageType,
      'message': message,
      'time': time,
      'isRead': isRead,
      'isSend': isSend,
      'lastView': lastView,
      'online': online,
      'silence': silence,
      'count': count,
      'messageId': messageId,
      'isReceived': isReceived,
      'created_at': created_at,
      'updated_at': Timestamp.now(),
      'isDeleted': isDeleted
    };

    //Au cas où on veut faire une mise à jour du message
    if (!nullable) json.removeWhere((key, value) => value == null);
    return json;
  }
}
