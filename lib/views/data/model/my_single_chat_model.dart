import 'package:Organiv/views/domain/entities/my_single_chat_entity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MySingleChatModel extends MySingleChatEntity {
  MySingleChatModel({
    String senderName,
    String senderUID,
    String recipientName,
    String recipientUID,
    String channelId,
    String profilURL,
    String recipientPhoneNumber,
    String senderPhoneNumber,
    String recentTextMessage,
    bool isRead,
    bool isArchived,
    Timestamp time,
    Timestamp created_at,
    Timestamp updated_at,
    int count,
    Timestamp lastView,
    bool silence,
    bool online,
    bool nullable = true,
  }) : super(
          senderName: senderName,
          senderUID: senderUID,
          recipientName: recipientName,
          recipientUID: recipientUID,
          channelId: channelId,
          profilURL: profilURL,
          recipientPhoneNumber: recipientPhoneNumber,
          senderPhoneNumber: senderPhoneNumber,
          recentTextMessage: recentTextMessage,
          isRead: isRead,
          isArchived: isArchived,
          time: time,
          created_at: created_at,
          lastView: lastView,
          count: count,
          online: online,
          silence: silence,
          nullable: nullable,
        );

  factory MySingleChatModel.fromSnapShot(DocumentSnapshot snapshot) {
    return MySingleChatModel(
      senderName: snapshot.data()['senderName'],
      senderUID: snapshot.data()['senderUID'],
      recipientName: snapshot.data()['recipientName'],
      recipientUID: snapshot.data()['recipientUID'],
      channelId: snapshot.data()['channelId'],
      profilURL: snapshot.data()['profilURL'],
      recipientPhoneNumber: snapshot.data()['recipientPhoneNumber'],
      senderPhoneNumber: snapshot.data()['senderPhoneNumber'],
      recentTextMessage: snapshot.data()['recentTextMessage'],
      isRead: snapshot.data()['isRead'],
      isArchived: snapshot.data()['isArchived'],
      lastView: snapshot.data()['lastView'],
      count: snapshot.data()['count'],
      online: snapshot.data()['online'],
      silence: snapshot.data()['silence'],
      time: snapshot.data()['time'],
      created_at: snapshot.data()['created_at'],
      updated_at: snapshot.data()['updated_at'],
    );
  }

  Map<String, dynamic> toDocument() {
    Map<String, dynamic> json = {
      'senderName': senderName,
      'senderUID': senderUID,
      'recipientName': recipientName,
      'recipientUID': recipientUID,
      'channelId': channelId,
      'profilURL': profilURL,
      'recipientPhoneNumber': recipientPhoneNumber,
      'senderPhoneNumber': senderPhoneNumber,
      'recentTextMessage': recentTextMessage,
      'isRead': isRead,
      'lastView': lastView,
      'count': count,
      'online': online,
      'silence': silence,
      'isArchived': isArchived,
      'time': time,
      'created_at': created_at,
      'updated_at': Timestamp.now(),
    };

    //Au cas où on veut faire une mise à jour du chat
    if (!nullable) json.removeWhere((key, value) => value == null);
    return json;
  }
}
