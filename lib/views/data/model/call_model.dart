import 'package:Organiv/views/domain/entities/call_entity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class CallModel extends CallEntity {
  CallModel({
    String channelId,
    String callerId,
    String callerName,
    String callerPic,
    String receiverId,
    String receiverName,
    String receiverPic,
    bool hasDialled,
    int count,
    String type,
    Timestamp lastView,
    Timestamp created_at,
    Timestamp updated_at,
    bool silence,
    bool online,
    bool nullable,
    Timestamp time,
  }) : super(
          channelId: channelId,
          callerId: callerId,
          callerName: callerName,
          callerPic: callerPic,
          receiverId: receiverId,
          receiverName: receiverName,
          receiverPic: receiverPic,
          hasDialled: hasDialled,
          count: count,
          created_at: created_at,
          type: type,
          time: time,
          lastView: lastView,
          online: online,
          silence: silence,
          nullable: nullable,
        );

  factory CallModel.fromSnapShot(DocumentSnapshot snapshot) {
    return CallModel(
      channelId: snapshot.data()['channelId'],
      callerId: snapshot.data()['callerId'],
      callerName: snapshot.data()['callerName'],
      callerPic: snapshot.data()['callerPic'],
      receiverId: snapshot.data()['receiverId'],
      receiverName: snapshot.data()['receiverName'],
      receiverPic: snapshot.data()['receiverPic'],
      hasDialled: snapshot.data()['hasDialled'],
      count: snapshot.data()['count'],
      type: snapshot.data()['type'],
      lastView: snapshot.data()['lastView'],
      time: snapshot.data()['time'],
      online: snapshot.data()['online'],
      silence: snapshot.data()['silence'],
      created_at: snapshot.data()['created_at'],
      updated_at: snapshot.data()['updated_at'],
    );
  }

  Map<String, dynamic> toDocument() {
    Map<String, dynamic> json = {
      "channelId": channelId,
      "callerId": callerId,
      "callerName": callerName,
      "callerPic": callerPic,
      "receiverId": receiverId,
      "receiverName": receiverName,
      "receiverPic": receiverPic,
      "hasDialled": hasDialled,
      "count": count,
      "time": time,
      "type": type,
      "lastView": lastView,
      "online": online,
      "silence": silence,
      "updated_at": Timestamp.now(),
      "created_at": created_at,
    };

    //Au cas où on veut faire une mise à jour de l'appel
    if (!nullable) json.removeWhere((key, value) => value == null);
    return json;
  }
}
