import 'package:Organiv/views/domain/entities/contact_user_entity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

// ignore: must_be_immutable
class ContactUserModel extends ContactUserEntity {
  int milliseconds;
  ContactUserModel({
    int id,
    String internationalizedPhoneNumber,
    String label,
    Timestamp lastView,
    String phoneNumber,
    String nameUser,
    String status,
    int count,
    bool silence,
    bool online,
    String uid,
    String profilURL,
    this.milliseconds,
    // ignore: non_constant_identifier_names
    Timestamp created_at,
    // ignore: non_constant_identifier_names
    Timestamp updated_at,
    bool nullable = true,
  }) : super(
          id: id,
          internationalizedPhoneNumber: internationalizedPhoneNumber,
          label: label,
          lastView: lastView,
          phoneNumber: phoneNumber,
          status: status,
          uid: uid,
          count: count,
          online: online,
          nameUser: nameUser,
          silence: silence,
          created_at: created_at,
        );

  // convenience constructor to create a Word object
  factory ContactUserModel.fromMap(Map<String, dynamic> map) {
    return ContactUserModel(
      id: map["id"],
      internationalizedPhoneNumber: map["internationalizedPhoneNumber"],
      label: map["label"],
      phoneNumber: map["phoneNumber"],
      milliseconds: map["milliseconds"],
      status: map["status"],
      uid: map["uid"],
      count: map["count"],
      online: map["online"] == 1 ? true : false,
      nameUser: map["nameUser"],
      silence: map["silence"] == 1 ? true : false,
      profilURL: map["profilURL"],
    );
  }

  Map<String, dynamic> toDocument() {
    Map<String, dynamic> json = {
      "internationalizedPhoneNumber": internationalizedPhoneNumber,
      "label": label,
      "phoneNumber": phoneNumber,
      "nameUser": nameUser,
      "status": status,
      "count": count,
      "silence": silence ? 1 : 0,
      "online": online ? 1 : 0,
      "uid": uid,
      "milliseconds": milliseconds,
      "profilURL": profilURL,
    };
    if (!nullable) json.removeWhere((key, value) => value == null);
    return json;
  }
}
