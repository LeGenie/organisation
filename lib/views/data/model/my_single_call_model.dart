import 'package:Organiv/views/domain/entities/my_single_call_entity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

// ignore: must_be_immutable
class MySingleCallModel extends MySingleCallEntity {
  MySingleCallModel({
    String callerName,
    String callerId,
    String recipientName,
    String recipientId,
    String channelId,
    String pipeId,
    String callerPic,
    String receiverPic,
    bool isRead,
    bool isArchived,
    bool hasDialled,
    Timestamp time,
    // ignore: non_constant_identifier_names
    Timestamp created_at,
    int count,
    Timestamp lastView,
    bool silence,
    bool online,
    String type,

    //Au cas où on fait une mise à jour du message
    // ignore: non_constant_identifier_names
    Timestamp updated_at,
    //Si on retourne l'exhaustivité ou pas
    bool nullable = true,
  }) : super(
          recipientName: recipientName,
          channelId: channelId,
          pipeId: pipeId,
          callerPic: callerPic,
          receiverPic: receiverPic,
          callerId: callerId,
          callerName: callerName,
          recipientId: recipientId,
          type: type,
          isRead: isRead,
          isArchived: isArchived,
          time: time,
          created_at: created_at,
          lastView: lastView,
          count: count,
          online: online,
          silence: silence,
          hasDialled: hasDialled,
          nullable: nullable,
        );

  factory MySingleCallModel.fromSnapShot(DocumentSnapshot snapshot) {
    return MySingleCallModel(
      recipientName: snapshot.data()['recipientName'],
      channelId: snapshot.data()['channelId'],
      pipeId: snapshot.data()['pipeId'],
      callerPic: snapshot.data()['callerPic'],
      receiverPic: snapshot.data()['receiverPic'],
      callerId: snapshot.data()['callerId'],
      callerName: snapshot.data()['callerName'],
      recipientId: snapshot.data()['recipientId'],
      type: snapshot.data()['type'],
      isRead: snapshot.data()['isRead'],
      isArchived: snapshot.data()['isArchived'],
      time: snapshot.data()['time'],
      lastView: snapshot.data()['lastView'],
      count: snapshot.data()['count'],
      online: snapshot.data()['online'],
      silence: snapshot.data()['silence'],
      hasDialled: snapshot.data()['hasDialled'],
      created_at: snapshot.data()['created_at'],
      updated_at: snapshot.data()['updated_at'],
    );
  }

  Map<String, dynamic> toDocument() {
    Map<String, dynamic> json = {
      "recipientName": recipientName,
      "channelId": channelId,
      "pipeId": pipeId,
      "callerPic": callerPic,
      "receiverPic": receiverPic,
      "callerId": callerId,
      "callerName": callerName,
      "recipientId": recipientId,
      "type": type,
      "isRead": isRead,
      "isArchived": isArchived,
      "time": time,
      'created_at': created_at,
      'updated_at': Timestamp.now(),
      "lastView": lastView,
      "count": count,
      "online": online,
      "silence": silence,
      "hasDialled": hasDialled,
    };

    //Au cas où on veut faire une mise à jour du chat
    if (!nullable) json.removeWhere((key, value) => value == null);
    return json;
  }
}
