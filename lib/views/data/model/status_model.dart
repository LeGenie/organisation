import 'package:Organiv/views/domain/entities/status_entity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class StatusModel extends StatusEntity {
  StatusModel({
    String statusId,
    Timestamp time,
    Timestamp created_at,
    Timestamp updated_at,
    String statusType,
    String legende,
    String message,
    String link,
    String color,
    int size,
    List<String> uids,
    bool nullable = true,
  }) : super(
          statusId: statusId,
          time: time,
          statusType: statusType,
          legende: legende,
          link: link,
          color: color,
          size: size,
          message: message,
          uids: uids,
          created_at: created_at,
          updated_at: updated_at,
          nullable: nullable,
        );

  factory StatusModel.fromSnapshot(DocumentSnapshot snapshot) {
    return StatusModel(
      statusId: snapshot.data()['statusId'],
      time: snapshot.data()['time'],
      statusType: snapshot.data()['statusType'],
      legende: snapshot.data()['legende'],
      link: snapshot.data()['link'],
      color: snapshot.data()['color'],
      size: snapshot.data()['size'],
      message: snapshot.data()['message'],
      //uids: _listUser (snapshot.data()['uids']),
      created_at: snapshot.data()['created_at'],
      updated_at: snapshot.data()['updated_at'],
    );
  }

  List<String> _listUser(List<dynamic> uids) {
    List<String> array = [];
    uids.forEach((uid) {
      array.add(uid.toString());
    });
    return array;
  }

  Map<String, dynamic> toDocument() {
    Map<String, dynamic> json = {
      "statusId": statusId,
      "time": time,
      "statusType": statusType,
      "legende": legende,
      "link": link,
      "color": color,
      "size": size,
      "message": message,
      "created_at": created_at,
      "updated_at": Timestamp.now(),
      "uids": uids,
    };
    if (!nullable) json.removeWhere((key, value) => value == null);
    return json;
  }
}
