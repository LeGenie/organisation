import 'package:Organiv/views/domain/entities/my_single_status_entity.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MySingleStatusModel extends MySingleStatusEntity {
  MySingleStatusModel({
    String channelId,
    String profilURL,
    Timestamp time,
    Timestamp created_at,
    Timestamp updated_at,
    int count,
    bool silence,
    bool online,
    bool nullable = true,
  }) : super(
          channelId: channelId,
          count: count,
          online: online,
          profilURL: profilURL,
          silence: silence,
          time: time,
          created_at: created_at,
          nullable: nullable,
        );

  factory MySingleStatusModel.fromSnapshot(DocumentSnapshot snapshot) {
    return MySingleStatusModel(
      channelId: snapshot.data()['channelId'],
      count: snapshot.data()['count'],
      online: snapshot.data()['online'],
      profilURL: snapshot.data()['profilURL'],
      silence: snapshot.data()['silence'],
      time: snapshot.data()['time'],
      created_at: snapshot.data()['created_at'],
      updated_at: snapshot.data()['updated_at'],
    );
  }

  Map<String, dynamic> toDocument() {
    Map<String, dynamic> json = {
      "channelId": channelId,
      "count": count,
      "isRead": isRead,
      "lastView": lastView,
      "online": online,
      "profilURL": profilURL,
      "silence": silence,
      "time": time,
      "created_at": created_at,
      "updated_at": Timestamp.now(),
    };
    if (!nullable) json.removeWhere((key, value) => value == null);
    return json;
  }
}
