import 'package:flutter/material.dart';
import 'package:Organiv/views/methods/support/Choice.dart';
import 'package:Organiv/config/variable/language/fr.dart';
import 'package:Organiv/config/variable/global.dart';

class PopUpMenu extends StatefulWidget {
  @override
  PopUpMenuState createState() => PopUpMenuState();
}

class PopUpMenuState extends State<PopUpMenu> {
  @override
  Widget build(BuildContext context) {
    final TextStyle bodyStyle = Theme.of(context).textTheme.body1;

    //Les valeurs de l'actualisation
    List<Choice> choices = <Choice>[
      new Choice(
        title: Variable.newGroup,
        function: null,
      ),
      new Choice(
        title: Variable.newDiffusion,
        function: null,
      ),
      new Choice(
        title: Variable.importantMessage,
        function: null,
      ),
      new Choice(
        title: Variable.setting,
        function: null,
      ),
    ];

    return PopupMenuButton(
      itemBuilder: (BuildContext context) {
        return choices.map((Choice choice) {
          return PopupMenuItem<Choice>(
            value: choice,
            enabled: true,
            child: new FlatButton(
              child: new Text(
                choice.title,
                style: bodyStyle,
              ),
              onPressed: () => choice.execute(),
            ),
          );
        }).toList();
      },
    );
  }
}
