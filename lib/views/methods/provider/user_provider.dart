import 'package:Organiv/config/variable/global.dart';
import 'package:flutter/widgets.dart';
import 'package:Organiv/views/data/model/user_model.dart';
import 'package:Organiv/views/domain/entities/user_entity.dart';
import 'package:Organiv/views/domain/repositories/firebase_repository.dart';

class UserProvider with ChangeNotifier {
  UserEntity _user;
  final FirebaseRepository repository;

  UserProvider({this.repository});

  UserModel get getUser => _user;

  void refreshUser() async {
    String uid = await repository.getCurrentUID();
    Stream<List<UserEntity>> streamUser = await repository.getAllUsers();
    streamUser.listen((users) {
      _user = users.firstWhere((user) => user.uid == uid,
          orElse: () => UserModel());
    });
    Global.user = getUser;
    print("Nouvelle mise à jour");
    notifyListeners();
  }
}
