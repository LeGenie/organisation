import 'package:shared_preferences/shared_preferences.dart';

class HelperFunction {
  static String sharedPreferenceUserLoggedInKey = "ISLOGGEDIN";
  static String sharedPreferenceUIDKey = "UIDKEY";
  static String sharedPreferenceIndexPageKey = "INDEXPAGEKEY";

  //Saving data to SharedPreference
  static Future<bool> saveUserLoggedInSharePreference(
      bool isUserLoggedIn) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setBool(sharedPreferenceUserLoggedInKey, isUserLoggedIn);
  }

  static Future<bool> saveUIDSharePreference(String uid) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setString(sharedPreferenceUIDKey, uid);
  }

  static Future<bool> saveIndexPageSharePreference(int index) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setInt(sharedPreferenceIndexPageKey, index);
  }

  //getting data from SharedPreference
  static Future<bool> getUserLoggedInSharePreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.getBool(sharedPreferenceUserLoggedInKey);
  }

  static Future<String> getUIDSharePreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.getString(sharedPreferenceUIDKey);
  }

  static Future<int> getIndexPageSharePreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.getInt(sharedPreferenceIndexPageKey);
  }
}
