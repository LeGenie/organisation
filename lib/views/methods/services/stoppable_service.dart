abstract class StoppableService {
  bool _serviceStopped = false;
  bool get serviceStopped => _serviceStopped;

  void stop() {
    _serviceStopped = true;
  }

  void start() {
    _serviceStopped = false;
  }
}
