import 'package:international_phone_input/international_phone_input.dart';

class ValidatePhoneNumber {
  static void validatePhoneNumber(
      void Function(String phoneNumber, String internationalizedPhoneNumber,
              String isoCode)
          onPhoneNumberChange,
      String phoneText,
      String code) {
    if (phoneText != null && phoneText.isNotEmpty) {
      PhoneService.parsePhoneNumber(phoneText, code).then((isValid) {
        if (onPhoneNumberChange != null) {
          if (isValid) {
            PhoneService.getNormalizedPhoneNumber(phoneText, code)
                .then((number) {
              onPhoneNumberChange(phoneText, number, code);
            });
          } else {
            onPhoneNumberChange('', '', code);
          }
        }
      });
    }
  }
}
