import 'package:Organiv/views/data/model/call_model.dart';
import 'package:Organiv/views/domain/entities/user_entity.dart';
import 'package:Organiv/views/methods/call/call_methods.dart';
import 'package:path/path.dart';

class CallUtils {
  static final CallMethods callMethods = CallMethods();


  static dial({UserEntity from, UserEntity to, context}) async {
    CallModel call = CallModel(
      callerId: from.uid,
      callerName: from.name,
      callerPic: from.profilUrl,
      receiverId: to.uid,
      receiverName: to.name,
      receiverPic: to.profilUrl,
    );
  }
}
