import 'package:Organiv/views/data/model/call_model.dart';

class CallMethods {
  Map<String, dynamic> _hasDialledMap;
  Map<String, dynamic> _hasNotDialledMap;
  Map<String, dynamic> _endCall;
  final CallModel call;

  CallMethods({this.call});

  //Make a new Call
  void makeCall() {
    //Caller
    call.hasDialled = true;
    _hasDialledMap = call.toDocument();
    //Receiver
    call.hasDialled = false;
    _hasNotDialledMap = call.toDocument();
  }

  void endCall() {
    //Receiver
    call.hasDialled = false;
    call.nullable = false;
    _hasNotDialledMap = call.toDocument();
  }

  get hasDialledMap => _hasDialledMap;

  get hasNotDialledMap => _hasNotDialledMap;
}
