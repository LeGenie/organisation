import 'package:flutter/material.dart';

class Choice{

  final String title;
  final IconData icon;
  final VoidCallback function;

  Choice({this.title, this.icon, this.function});

  void execute(){
    if(this.function != null) function();
  }
}