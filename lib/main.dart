import 'package:Organiv/views/methods/util/connexion_utils.dart';
import 'package:Organiv/views/presentation/bloc/auth/auth_cubit.dart';
import 'package:Organiv/views/presentation/bloc/communication/communication_cubit.dart';
import 'package:Organiv/views/presentation/bloc/contact/contact_cubit.dart';
import 'package:Organiv/views/presentation/bloc/my_single_chat/my_single_chat_cubit.dart';
import 'package:Organiv/views/presentation/bloc/phone_auth/phone_auth_cubit.dart';
import 'package:Organiv/views/presentation/bloc/user/user_cubit.dart';
import 'package:Organiv/views/presentation/bloc/my_single_status/my_single_status_cubit.dart';
import 'package:Organiv/views/presentation/bloc/my_single_call/my_single_call_cubit.dart';
import 'package:Organiv/views/presentation/bloc/status/status_cubit.dart';
import 'package:Organiv/views/presentation/bloc/call/call_cubit.dart';
import 'package:Organiv/views/presentation/bloc/get_device_number/get_device_numbers_cubit.dart';
import 'package:Organiv/views/presentation/screens/home_screen.dart';
import 'package:Organiv/views/presentation/screens/splash_screen.dart';
import 'package:Organiv/views/presentation/screens/welcome_screen.dart';
import 'package:Organiv/views/presentation/screens/pickup/ringing.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:Organiv/config/variable/global.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_core/firebase_core.dart' as firebase_core;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission/permission.dart';
import 'injection_container.dart' as di;

//void main() => runApp(MyApp());
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await firebase_core.Firebase.initializeApp();
  await di.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return HomePage();
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with WidgetsBindingObserver {
  //bool _hasNetworkConnection;
  //bool _fallbackViewOn;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state != AppLifecycleState.resumed) print("Toto");
  }

  @override
  initState() {
    super.initState();
    /*_hasNetworkConnection = false;
    _fallbackViewOn = false;*/
    ConnectionStatusSingleton connectionStatus =
        ConnectionStatusSingleton.getInstance();
    connectionStatus.initialize();
    connectionStatus.connectionChange.listen(_updateConnectivity);
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: _listProviders(),
      child: MaterialApp(
        title: Global.title,
        theme: defaultTargetPlatform == TargetPlatform.iOS
            ? Global.kIOSTheme
            : Global.kDefaultTheme,
        debugShowCheckedModeBanner: false,
        routes: {
          "/": (context) {
            return BlocBuilder<AuthCubit, AuthState>(
              builder: (context, authState) {
                permission();
                if (authState is Authenticated) {
                  return BlocBuilder<UserCubit, UserState>(
                    builder: (context, userState) {
                      if (userState is UserCall) {
                        return Ringing();
                      }
                      if (userState is UserLoaded) {
                        BlocProvider.of<UserCubit>(context).getMyUser();
                        return SplashScreen(
                          time: 1,
                          home: HomeScreen(),
                        );
                      }
                      return Container();
                    },
                  );
                }

                if (authState is UnAuthenticated) {
                  return SplashScreen(
                    time: Global.durationLoad,
                    home: WelcomScreen(),
                  );
                }

                return Container();
              },
            );
          }
        },
      ),
    );
  }

  _listProviders() {
    return [
      BlocProvider(
        create: (_) => di.sl<AuthCubit>()..appStarted(),
      ),
      BlocProvider(
        create: (_) => di.sl<PhoneAuthCubit>(),
      ),
      BlocProvider<UserCubit>(
        create: (_) => di.sl<UserCubit>()..getAllUsers(),
      ),
      BlocProvider<GetDeviceNumbersCubit>(
        create: (_) => di.sl<GetDeviceNumbersCubit>(),
      ),
      BlocProvider<CommunicationCubit>(
        create: (_) => di.sl<CommunicationCubit>(),
      ),
      BlocProvider<MySingleChatCubit>(
        create: (_) => di.sl<MySingleChatCubit>(),
      ),
      BlocProvider<ContactCubit>(
        create: (_) => di.sl<ContactCubit>()..create(),
      ),
      BlocProvider<MySingleStatusCubit>(
        create: (_) => di.sl<MySingleStatusCubit>(),
      ),
      BlocProvider<StatusCubit>(
        create: (_) => di.sl<StatusCubit>(),
      ),
      BlocProvider<MySingleCallCubit>(
        create: (_) => di.sl<MySingleCallCubit>(),
      ),
      BlocProvider<CallCubit>(
        create: (_) => di.sl<CallCubit>(),
      ),
    ];
  }

  //Permission
  void permission() async {
    await Permission.getPermissionsStatus([
      PermissionName.Contacts,
      PermissionName.Camera,
      PermissionName.Microphone,
      PermissionName.Storage,
      PermissionName.Internet,
    ]);

    await Permission.requestPermissions([
      PermissionName.Contacts,
      PermissionName.Camera,
      PermissionName.Microphone,
      PermissionName.Storage,
      PermissionName.Internet,
    ]);
  }

  void _updateConnectivity(dynamic hasConnection) {
    /*if (!_hasNetworkConnection) {
      if (!_fallbackViewOn) {
        setState(() {
          _fallbackViewOn = true;
          _hasNetworkConnection = hasConnection;
        });
      }
    } else {
      if (_fallbackViewOn) {
        setState(() {
          _fallbackViewOn = false;
          _hasNetworkConnection = hasConnection;
        });
      }
    }*/
    Global.connected = hasConnection;
    print("Le status de la connexion est 3: ${Global.connected}");
  }
}
