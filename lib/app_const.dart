import 'package:Organiv/config/variable/global.dart';
import 'package:flutter/material.dart';

//APP Version
const APP_VERSION = "1.0.0";

class AppConst {
  //Database
  static final String database = Global.title;
  static final String contactTable = 'contacts';
  static final String urlFirebase = "organisation-7c98a.firebaseio.com";

  //Exchange type
  static final String text = "TEXT";
  static final String image = "IMAGE";
  static final String audio = "AUDIO";
  static final String video = "VIDEO";
  static final String map = "MAP";
  static final String doc = "DOCUMENT";
  static final String sticker = "STICKER";
  static final String call = "CALL";
  static final String call_video = "CALL VIDEO";
  static final String call_audio = "CALL AUDIO";
  static final String entreprise = "ENTREPRISE";

  //Communication Type
  static final String oneToOne = "oneToOne";
  static final String group = "group";
  static final String manyToMany = "manyToMany";
  static final String oneToMany = "oneToMany";
  static final String manyToOne = "manyToOne";
  static final String public = "PUBLICATION";
  static final String direct = "DIRECT";
  static final String status = "STATUS";
  static final String forum = "FORUM";

  //Groupement Type
  static final String USER_COLLECTION = "users";
  static final String MESSAGE_COLLECTION = "messages";
  static final String STATUS_COLLECTION = "status";
  static final String CALL_COLLECTION = "calls";

  static final String SINGLE_CHAT_CHANNEL_COLLECTION = "singleChatChannel";
  static final String SINGLE_CALL_CHANNEL_COLLECTION = "singleCallChannel";
  static final String STATUS_CHANNEL_COLLECTION = "statusChannel";

  static final String MY_STATUS_CHANNEL_COLLECTION = "myStatusChannel";
  static final String MY_CHAT_CHANNEL_COLLECTION = "myChatChannel";
  static final String MY_CALL_CHANNEL_COLLECTION = "myCallChannel";
  static final String MY_LIVE_CHANNEL_COLLECTION = "myLiveChannel";
}

const kBackgoundColor = Color(0xFF091C40);
const kSecondaryColor = Color(0xFF606060);
const kRedColor = Color(0xFFFF1E46);

/// Define App ID and Token
const APP_ID = "959cf47e11794494985e9a88dfade9a2";
const APP_CERTIFICATE = "2252e4a4b13a4cb585d06be56868d2e6";
const Token =
    "006959cf47e11794494985e9a88dfade9a2IAAHZp1ORZJvPQA1hzhUbN1ukVFYApiSR5FTWB+KBJSU9Ufd3HkAAAAAEAAEa9nrCAk5YAEAAQAICTlg";
