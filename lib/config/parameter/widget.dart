import 'package:flutter/material.dart';
import 'package:Organiv/config/variable/global.dart';

//Le style simple de l'application
TextStyle simpleTextStyle(
  BuildContext context, {
  Color color,
  double size,
  FontWeight fontWeight,
  TextDecoration decoration = TextDecoration.none,
  FontStyle fontStyle,
}) {
  Global _global = new Global(context);

  return TextStyle(
    color: color ?? _global.bodyStyle.color,
    fontSize: size ?? _global.bodyStyle.fontSize,
    fontStyle: fontStyle ?? _global.bodyStyle.fontStyle,
    fontWeight: fontWeight ?? _global.bodyStyle.fontWeight,
    decoration: decoration ?? _global.bodyStyle.decoration,
  );
}

//La décoration des champs de saisies
InputDecoration textInputDecoration(BuildContext context, String hintText,
    {Color color, Widget prefixIcon}) {
  Global _global = new Global(context);
  return InputDecoration(
    hintText: hintText,
    hintStyle: TextStyle(color: color ?? _global.bodyStyle.color),
    prefixIcon: prefixIcon,
  );
}

//La décoration des champs de conversation
InputDecoration conversationDecoration(BuildContext context, String hintText) {
  return InputDecoration(
      hintText: hintText,
      hintStyle: TextStyle(color: Global.colorTypeMessage),
      border: InputBorder.none);
}
