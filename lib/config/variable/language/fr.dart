import 'package:Organiv/config/variable/global.dart';
import 'package:flutter/material.dart';

//Les variables globale de l'application
class Variable {
  Variable({BuildContext context});

  //App Bar
  static String searchWord = "Recherche ...";

  //Sign In, Sign Up
  static String nameWord = "Entrer votre nom";
  static String nameInvalidWord = "Nom vide";
  static String nameTooLongWord = "Nom trop long";
  static String phoneWord = "Numéro de téléphone";
  static String phoneEmptyWord = "Numéro vide";
  static String phoneInvalidWord = "Numéro invalide";
  static String codeInvalidWord = "Code invalide";
  static String emailWord = "Email";
  static String emailInvalidWord = "Email invalide";
  static String emailExprRegr =
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
  static String passwordWord = "Mot de passe";
  static String passwordForgetWord = "Mot de passe oublié ?";
  static String passwordInvalidWord = "Mot de passe trop court <8";
  static String signInWord = "Commencer";
  static String signInGoogleWord = "Connexion avec Google";
  static String signInBack = "Retour";
  static String signInWrongWord = "Email inexistant ou mot de passe incorrect!";
  static String withoutAccountWord = "Vous n'avez pas de compte?";
  static String signUpWord = "Inscription";
  static String signUpGoogleWord = "Inscription avec Google";
  static String withAccountWord = "Vous avez déjà un compte";

  //Error
  static String logginError = "Something is wrong";

  //SignIn with phone
  //1
  static String signInPhone = "Vérifier votre numéro de téléphone";
  static String selectPhoneCountry = "Selectionner votre pays";
  static String commentDescSignPhone =
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.";
  static String commentDescWelcome =
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.";
  static String commentDescSmsCode =
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.";
  static String codePhoneVerification = "Code de vérification";
  static String next = "Suivant";
  static String agree = "ACCEPTER ET CONTINUER";
  static String digits = "Entrer les 6 chiffres du code";
  //2
  static String welcome = "Bienvenu sur ${Global.title}";

  //3 - Profil
  static String titleProfil = "Info. du profil";
  static String commentDescProfil =
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.";

  //Home
  static List<String> headerHome = ['DISC.', 'STATUT', 'APPELS', 'TEST'];

  //Chat page
  static String welcomeChat =
      "Discuter avec vos amis et votre famille,\n sur ${Global.title}";
  static String message = "Message";
  static String remind = "Remind Me";

  //statut page
  static String welcomeStatus =
      "Partager vos moments les plus précieux avec vos amis et votre famille,\n sur ${Global.title}";
  static String myStatus = "Mon statut";
  static String myStatusSubtitle = "Mettre à jour votre statut";
  static String recentStatus = "Récentes mises à jour";
  static String viewStatus = "Mises à jour vues";
  static String ortherStatus = "Autres mises à jour";
  static String silenceStatus = "Mises à jour en silencieux";
  static String conjonctionA = "à";

  //Call page
  static String incoming = "Incoming...";
  static String callHasMade(String type) => "Appel $type en cours...";
  static String acceptcall = "Accepter";
  static String declinecall = "Renvoyer";

  //User
  static String statusLabel = "Salut, j'utilise ${Global.title}.";

  //Conversation page
  static String lastView = "Dernière vu ";
  static String online = "En ligne";
  static String today = "Aujourd'hui";
  static String yesterday = "Hier";

  //Contact page
  static String selectContact = "Sélectionner un contact";
  static String newGroup = "Nouv. groupe";
  static String newCallGroup = "Nouvel appel de groupe";
  static String newContact = "Nouveau contact";

  //Status
  static String labelStatus = "Ecrire un statut";

  //Les labels de texte
  static String newConversation = "Nouvelle conversation";
  static String typeMessage = "Taper un message";

  //Les labels du popupMenu
  static String deconnect = "Déconnexion";
  static String newDiffusion = "Nouvelle diffusion";
  static String importantMessage = "Messages importants";
  static String setting = "Paramètres";
}
