import 'package:Organiv/views/data/model/contact_user_model.dart';
import 'package:Organiv/views/domain/entities/user_entity.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:Organiv/config/variable/language/fr.dart';
import 'package:unique_identifier/unique_identifier.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';

class Global extends Variable {
  BuildContext context;
  Global(this.context);

  //User
  static UserEntity user;
  static bool connected;

  //Le Nom/Titre de l'application
  //Les variables globales de l'application
  static String title = 'Networking';
  //Les liens de photos
  static String linkLogo = "assets/images/logo.png";
  static String linkWelcome = "assets/images/brand.png";
  static String linkBackground = "assets/images/background.png";
  static String linkBackgroundMessage =
      "assets/images/background_wallpaper.png";
  static String linkProfil = "assets/images/profile_default.png";
  //loading
  static int durationLoad = 5;
  static double circleSize = 150;
  //Les couleurs de l'application
  //La couleur d'entête
  static MaterialColor heartColor = Colors.lightBlue;
  static Color backgroundHeartColor = Colors.lightBlue;
  //La couleur de fond d'écran
  static Color backgroundColor = Colors.white;
  //La couleur des liens
  static Color linkColor = Colors.blueAccent;
  //La couleur du titre
  static Color foregroundTitle = Colors.black54.withOpacity(0.7);
  //La couleur du text dans la conversation
  static Color colorTypeMessage = Colors.black;
  //La couleur de fond de la zone de saisie
  static Color backgroundTypeMessage = Colors.white;
  //La couleur de fond d'un message envoyé par moi
  static Color backgroundSendMessage = Colors.lightBlue[400];
  //La couleur de fond en cas de refus ou de danger
  static Color backgroundRefus = Colors.redAccent.withOpacity(0.5);
  //Les couleurs de notification
  static Color notificationColor = backgroundHeartColor;
//  [backgroundHeartColor.withOpacity(0.4),
//  backgroundHeartColor.withOpacity(0.5),backgroundHeartColor.withOpacity(0.4),];
  //la couleur de fond d'un message reçu
  static Color backgroundReceiveMessage = Colors.grey[50];
  //Les variables du nom
  static int maxLenghtName = 15;
  //Agrandir listing
  static double ratio = 1;
  //Conversation
  static int minLineConversation = 1;
  static int maxLineConversation = 6;
  //Status
  static int maxWordsStatus = 700;
  static int maxLineStatus = 10;
  static int minLineStatus = 1;
  static int durationVideo = 30; //In seconde

  //Le style
  static double titleSize = 18;
  static double commentSize = 16;
  static final double interParagraphSpace = 30;
  static int indexPage;
  static int stringLenght = 20;
  static String hourFormat = 'H:mm';
  static String dayFormat = 'd MMMM yyyy';
  static String dateFormat = 'dd/MM/yyyy';
  static String weekFormat = 'EEEE';

  static String date(
      {Timestamp timestamp, String format, bool transformation = false}) {
    if (format == hourFormat)
      return DateFormat(format).format(timestamp.toDate());
    Duration duration = Timestamp.now().toDate().difference(timestamp.toDate());

    if (duration.inDays < 1 &&
        int.parse(DateFormat('d').format(timestamp.toDate()).toString()) ==
            int.parse(
                DateFormat('d').format(Timestamp.now().toDate()).toString()))
      return Variable.today;
    if (duration.inDays < 2 &&
        int.parse(DateFormat('d').format(timestamp.toDate()).toString()) !=
            int.parse(
                DateFormat('d').format(Timestamp.now().toDate()).toString()))
      return Variable.yesterday;
    if (duration.inDays < 7 &&
        DateFormat('EE').format(timestamp.toDate()).toString() ==
            DateFormat('EE').format(Timestamp.now().toDate()).toString()) {
      return DateFormat(weekFormat).format(timestamp.toDate());
    }
    return DateFormat(format).format(timestamp.toDate());
  }

  //Mes conctats
  static List<ContactUserModel> contacts = [];
  //Le style par défaut du téléphone
  //Le style 1
  TextStyle get bodyStyle {
    return Theme.of(this.context).textTheme.body1;
  }

  //Le style 2
  TextStyle get bodyStyle2 {
    return Theme.of(this.context).textTheme.body2;
  }

  //Les distances de normes
  //La distances avec le fond
  double fondDistance() {
    return MediaQuery.of(context).size.height - 50;
  }

  //La distance avec les bordures à droite
  double borderDistanceSign() {
    return MediaQuery.of(context).size.width / 20;
  }

  //La distance avec les bordures à droite
  double borderDistanceConversation() {
    return MediaQuery.of(context).size.width / 60;
  }

  //La distance avec les messages
  double interDistanceConversation() {
    return MediaQuery.of(context).size.height / 60;
  }

  //La distance avec le dessus pour la page de sign Up
  double centerSignUpTop() {
    return MediaQuery.of(context).size.height / 5;
  }

  //La distance entre interligne espacée
  double interLineDistanceExpand() {
    return MediaQuery.of(context).size.height / 300;
  }

  //Parametrage pour les iOS
  static final ThemeData kIOSTheme = new ThemeData(
      primarySwatch: backgroundHeartColor,
      accentColor: heartColor,
      scaffoldBackgroundColor: backgroundColor,
      visualDensity: VisualDensity.adaptivePlatformDensity);
  //Parametrage pour les Android
  static final ThemeData kDefaultTheme = new ThemeData(
      primarySwatch: backgroundHeartColor,
      accentColor: heartColor,
      scaffoldBackgroundColor: backgroundColor,
      visualDensity: VisualDensity.adaptivePlatformDensity);

  //Mon nom
  static String myName;

  //Mon email
  static String myEmail;

  //Mon numéro de téléphone
  static String myPhone;

  //Le code de mon pays
  static String myIsoCode;

  //Permet de resortir l'id associé à l'utilisateur lors de sa création, celui-ci sera unique
  static String myIdDocument;

  //Mon adresse Mac
  static String macAdress;

  //Ma marque de téléphone
  static String markAdress;

  //Les fonctions annexes
  //retourner l'heure dans une date
  static String getTime(DateTime date) {
    return "${date.hour}:${date.minute}";
  }

  //Truncate string
  static String truncString(String name) {
    return name.length <= stringLenght
        ? name
        : "${name.substring(0, stringLenght)}...";
  }

  //Snackbar
  static void showSnackbar(String message, GlobalKey<ScaffoldState> scaffoldKey,
      {Color color = Colors.black}) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: color,
    ));
  }

  //Retourner un identifiant unique du device
  static get macAdressPhone async {
    if (defaultTargetPlatform == TargetPlatform.iOS) {
      return (await (new DeviceInfoPlugin()).iosInfo).identifierForVendor;
    } else if (defaultTargetPlatform == TargetPlatform.android) {
      return await UniqueIdentifier.serial;
    } else
      return 'UNKNOW';
  }

  //Retourner la marque du telephone
  static get markPhone async {
    DeviceInfoPlugin deviceInfo = new DeviceInfoPlugin();
    if (defaultTargetPlatform == TargetPlatform.iOS) {
      return (await deviceInfo.iosInfo).utsname.machine;
    } else if (defaultTargetPlatform == TargetPlatform.android) {
      return (await deviceInfo.androidInfo).model;
    } else
      return 'UNKNOW';
  }

  //La fonction d'ordonnancement
  static String getChatId(String a, String b) {
    return a.substring(0, 1).codeUnitAt(0) > b.substring(0, 1).codeUnitAt(0)
        ? "$b\_$a"
        : "$a\_$b";
  }
}
